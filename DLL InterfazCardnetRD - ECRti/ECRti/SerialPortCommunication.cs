﻿using ECRti.Dtos;
using System;
using System.Collections.Generic;
using System.IO.Ports;
using System.Text;

namespace ECRti
{
    /// <summary>
    /// All available actions to communicate with the Point Of Sales through Serial Port.
    /// </summary>
    public class SerialPortCommunication : Communication, ICommunication
    {
        /// <summary>
        /// Initializes a new instance of the ECRti.SerialPortCommunication class using the
        /// specified SerialPort Settings to communicate with the Point Of Sales (POS).
        /// </summary>
        /// <param name="serialPortSettings">SerialPort Settings to be used.</param>
        public SerialPortCommunication(Settings.SerialPort serialPortSettings)
        {
            _currentSetting = serialPortSettings;
        }

        private readonly Settings.SerialPort _currentSetting;

        /// <summary>
        /// Get card holder information
        /// </summary>
        /// <returns>Information about card holder</returns>
        public CardDto RequestCardInformation()
        {

            // Write and Read to POS.
            var data = WriteReadPOS(Frames.CardConsultation, false);

            // Validate
            if (data.Length != 6) return null;

            if (!Enum.TryParse(data[0], true, out CardDto.Hosts host)) throw new FormatException();
            if (!Enum.TryParse(data[1], true, out CardDto.CardProducts cardProduct)) throw new FormatException();
            if (!short.TryParse(data[4], out short serviceCode)) throw new FormatException();

            return new CardDto
            {
                Host = host,
                CardProduct = cardProduct,
                TransactionMode = data[2],
                AccountNumber = data[3],
                ServiceCode = serviceCode,
                CardHolderName = data[5]
            };

        }

        public CardDto RequestCardInformation_KeepListenerAlive()
        {
            return RequestCardInformation();
        }

        protected override string[] WriteReadPOS(string frame, bool readOnly = false, 
        bool KeepListenerAlive = false, bool ListenerIsAlive = false)
        {
            return WriteReadPOS(frame, readOnly);
        }

        protected override string[] WriteReadPOS(string frame, bool readOnly = false)
        {
            // Declare Local Variables
            var tries = 0;

            //Suscribe to observe the comunication with the POS a log information
            LogMonitor observer = new LogMonitor();
            observer.Subscribe(this);

            // Start writing to the POS and set Communication Parameters.
            using (var serialPort = new SerialPort { PortName = _currentSetting.PortName, BaudRate = (int)_currentSetting.BoudRate, DataBits = (int)_currentSetting.DataBits, Parity = _currentSetting.Parity, StopBits = _currentSetting.StopBits, ReadTimeout = _currentSetting.ReadTimeout })
            {
                // Open Serial Port
                serialPort.Open();

                // Try to send the Frame to the POS.
                while (tries < Settings.SerialPort.MaximumNumberOfRetransmissions)
                {
                    // Char Variable for reading each character from POS.
                    char value;
                    byte[] buffer;

                    // Increment tries in one.
                    tries++;

                    // Are we reading only
                    if (!readOnly)
                    {
                        // Convert Frame to Bytes.
                        buffer = Frames.FrameToBytes(frame);

                        try
                        {
                            // Write to POS
                            serialPort.Write(buffer, 0, buffer.Length);
                        }
                        catch (TimeoutException)
                        {
                            throw new TimeoutException(Messages.Warnings.TimeoutPOS(_currentSetting.WriteTimeout));
                        }

                        // Loggin Information
                        LogNewTransaction(_currentSetting.PortName, LogDto.Requests.Sent, Messages.Information.Frame(), frame);

                        // Read the Acknowledgment
                        try
                        {
                            value = Convert.ToChar(serialPort.ReadByte());
                        }
                        catch (TimeoutException)
                        {
                            throw new TimeoutException(Messages.Warnings.TimeoutPOS(_currentSetting.WriteTimeout));
                        }

                        // Loggin Information
                        LogNewTransaction(_currentSetting.PortName, LogDto.Requests.Received, GetRequestType(value), value.ToString());

                        // Validate if the answer was an Acknowledgment.
                        if (value != ControlCharacters.ACK) continue;
                    }

                    // Row input received from the POS.
                    var input = new StringBuilder();

                    try
                    {
                        do
                        {
                            // Read POS answer Byte by Byte.
                            value = Convert.ToChar(serialPort.ReadByte());

                            // Append the Read Character to the String.
                            input.Append(value);
                        } while (!value.Equals(ControlCharacters.ETX) && !value.Equals(ControlCharacters.EOT));
                    }
                    catch (TimeoutException)
                    {
                        throw new TimeoutException(Messages.Warnings.TimeoutPOS(_currentSetting.ReadTimeout));
                    }

                    // Loggin Information
                    LogNewTransaction(_currentSetting.PortName, LogDto.Requests.Received, GetRequestType(value), input.ToString());

                    // Answer to the POS with an Acknowledgment.
                    buffer = Frames.FrameToBytes(ControlCharacters.ACK.ToString());

                    try
                    {
                        // Write to POS
                        serialPort.Write(buffer, 0, buffer.Length);
                    }
                    catch (TimeoutException)
                    {
                        throw new TimeoutException(Messages.Warnings.TimeoutPOS(_currentSetting.WriteTimeout));
                    }

                    // Loggin Information
                    LogNewTransaction(_currentSetting.PortName, LogDto.Requests.Sent, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                    // Clean up the response; remove the Start of TransmissTransmission.
                    var cleanedResponse = input.Length.Equals(1) ? input.ToString() : input.Remove(0, 1).Remove(input.Length - 1, 1).ToString();

                    // Split Separator Character.
                    return cleanedResponse.Split(ControlCharacters.FS);
                }

                // Negative Acknowledgment
                throw new Exception(Messages.Warnings.NegativeAcknowledgement());
            }
        }

        /// <summary>
        /// Authorize transaction of types defined in TransactionDto.TransactionTypes. Example: TransactionTypes.NormalSale, TransactionTypes.CheckIn
        /// </summary>
        /// <param name="transactionType">Type of transaction to authorize</param>
        /// <param name="transactionDto">Transaction to authorize</param>
        /// <returns>Trasaction with information about the autorization</returns>
        public TransactionDto AuthorizeTransaction(TransactionDto.TransactionTypes transactionType, TransactionDto transactionDto)
        {
            var frame = Frames.BuildFrameToAutorizeTransaction(transactionType, transactionDto);
            return GetTransaction(WriteReadPOS(frame, false), transactionDto);
        }
        public TransactionDto AuthorizeTransaction(TransactionDto.TransactionTypes transactionType, 
        TransactionDto transactionDto, bool KeepListenerAlive = false)
        {
            return AuthorizeTransaction(transactionType, transactionDto);
        }

        /// <summary>
        /// Cancel a transaction of a determined type
        /// </summary>
        /// <param name="transactionType">Type of transaction to cancel. Example: CancellationNormalSale or CheckInCancellation</param>
        /// <param name="transactionDto">Transaction to cancel.</param>
        /// <returns>String equivalent to a Communication.ControlCharacters. If successful Returns ControlCharacters.SuccessfulCancellation</returns>
        public string CancelTransaction(TransactionDto.TransactionTypes transactionType, TransactionDto transactionDto)
        {
            var frame = Frames.BuildFrameToCancelTransaction(transactionType, transactionDto);
            return WriteReadPOS(frame, false)[0];
        }

        /// <summary>
        /// Execute the close transactions in the POS..
        /// </summary>
        /// <param name="isMultiMerchantEnabled">Indicate if there are multi merchant transactions</param>
        /// <param name="multiMerchantID">If Multi Merchant is enabled, provide the Multi Merchant ID to execute the his close</param>
        /// <returns>Detail about the close by card host</returns>
        public List<ClosureDto> ExecuteClosure(bool isMultiMerchantEnabled, byte? multiMerchantID = null)
        {
            var frame = Frames.BuildFrameToExecuteClosure(isMultiMerchantEnabled, multiMerchantID);
            var answer = WriteReadPOS(frame, false);

            // Validate if the Batch is empty.
            if (answer == null || answer[0] == ControlCharacters.EmptyBatch) return null;

            var closureList = new List<ClosureDto>();
            do
            {
                var closureDto = new ClosureDto();
                GetClosure(answer, closureDto);

                closureList.Add(closureDto);

                // Read next host, if any.
                answer = WriteReadPOS(frame, true);

                // If the fields read is End of Transmission, exit loop
            } while (answer[0] != ControlCharacters.EOT.ToString());

            return closureList;
        }

        /// <summary>
        /// Update the tip amount of a transaction
        /// </summary>
        /// <param name="transactionDto">Transaction to update tip amount</param>
        /// <returns>String equivalent to a Communication.ControlCharacters. If successful Returns ControlCharacters.SuccessfulTip</returns>
        public string AdjustTipAmount(TransactionDto transactionDto)
        {
            var frame = Frames.BuildFrameToUpdateTipAmount(transactionDto);
            return WriteReadPOS(frame, false)[0];
        }

        /// <summary>
        /// Authorize a service for a specific acquirer
        /// </summary>
        /// <param name="MultiAcquirerID">Acquire ID to authorize the service</param>
        public void AuthorizeService(byte MultiAcquirerID)
        {
            var frame = Frames.Services(MultiAcquirerID);
            WriteReadPOS(frame, false);
        }
    }
}