﻿namespace ECRti
{
    internal partial struct Messages
    {
        public struct Warnings
        {
            public static string TimeoutPOS(int timeout)
            {
                return $"Ha caducado el tiempo de espera (timeout) de {timeout / 1000:N0} segundos al POS. Revise la comunicación a éste o incremente el tiempo de espera en Configuraciones";
            }

            public static string NegativeAcknowledgement()
            {
                return "Revise la comunicación con el POS";
            }

            public static string InvalidFunction => "Función es inválida";

            public static string TransactionDidNotProgress => "Transacción no progresó, no es válida ó fue cancelada por el usuario";

            public static string InvalidCard => "Tarjeta no permitida";

            public static string InvalidIPAddressV4 => "La dirección IP debe de ser versión 4. Ejemplo \"143.24.20.36\"";
        }
    }
}