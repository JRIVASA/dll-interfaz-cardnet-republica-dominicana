﻿namespace ECRti
{
    public static partial class Settings
    {
        public partial class SerialPort
        {
            /// <summary>
            /// The serial baud rate.
            /// </summary>
            public enum Baud
            {
                Rate75 = 75,
                Rate150 = 150,
                Rate300 = 300,
                Rate600 = 600,
                Rate1200 = 1200,
                Rate2400 = 2400,
                Rate4800 = 4800,
                Rate9600 = 9600,
                Rate19200 = 19200,
                Rate38400 = 38400,
                Rate57600 = 57600,
                Rate115200 = 115200,
                Rate230400 = 230400,
            }
        }
    }
}