﻿namespace ECRti
{
    public abstract partial class Communication
    {
        /// <summary>
        /// Control Characters Used for the transmission of the information between the POS and ECR.
        /// </summary>
        public struct ControlCharacters
        {
            /// <summary>
            /// End Of Transmission (HEX 4)
            /// </summary>
            public const char EOT = '';

            /// <summary>
            /// End of Text (HEX 3)
            /// </summary>
            public const char ETX = '';

            /// <summary>
            /// Field Separator (HEX 1C)
            /// </summary>
            public const char FS = '';

            /// <summary>
            /// Acknowledgment (HEX 6)
            /// </summary>
            public const char ACK = '';

            /// <summary>
            /// Enquiere (HEX 5)
            /// </summary>
            public const char ENQ = '';

            /// <summary>
            /// Synchronous Idle (HEX 16)
            /// </summary>
            public const char SYN = '';

            /// <summary>
            /// End of Medium (HEX 19)
            /// </summary>
            public const char EOM = '';

            /// <summary>
            /// Start of Text (HEX 2)
            /// </summary>
            public const char STX = '';

            /// <summary>
            /// Successful Tip Applied
            /// </summary>
            public const string SuccessfulTip = "05";

            /// <summary>
            /// Successful Cancellation
            /// </summary>
            public const string SuccessfulCancellation = "00";

            /// <summary>
            /// Invalid Operation requested by the ECR to the POS
            /// </summary>
            public const string InvalidOperation = "40";

            /// <summary>
            /// Duplicate Batch
            /// </summary>
            public const string DuplicateBatch = "02";

            /// <summary>
            /// Fail closing Batch
            /// </summary>
            public const string FailBatch = "01";

            /// <summary>
            /// Empty Batch
            /// </summary>
            public const string EmptyBatch = "10";

            /// <summary>
            /// Successful Closing Batch
            /// </summary>
            public const string SuccessfulBatch = "00";

            /// <summary>
            /// Transaction Did Not Progress
            /// </summary>
            public const string TransactionNotProgress = "99";

            /// <summary>
            /// Instruction to close all the Merchant
            /// </summary>
            public const string ClousureMerchantID = "99";

            /// <summary>
            /// Transaction Did Not Progress
            /// </summary>
            public const string CommunicationError = "97";

            /// <summary>
            /// Invalid Card
            /// </summary>
            public const string InvalidCard = "40";
        }
    }
}