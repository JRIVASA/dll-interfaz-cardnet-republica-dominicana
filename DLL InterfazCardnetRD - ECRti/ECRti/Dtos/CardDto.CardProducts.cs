﻿namespace ECRti.Dtos
{
    public partial class CardDto
    {
        /// <summary>
        /// Card Product Name
        /// </summary>
        public enum CardProducts
        {
            /// <summary>
            /// Visa Card
            /// </summary>
            Visa,

            /// <summary>
            /// MasterCard Card
            /// </summary>
            MC,

            /// <summary>
            /// A Toda Hora Card
            /// </summary>
            ATH,

            /// <summary>
            /// American Express Card
            /// </summary>
            AMEX
        }
    }
}