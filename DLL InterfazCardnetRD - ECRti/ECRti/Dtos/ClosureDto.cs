﻿using System;

namespace ECRti.Dtos
{
    /// <summary>
    /// Data Transfer Object for the Closure
    /// </summary>
    public class ClosureDto
    {
        /// <summary>
        /// Internal Closure ID
        /// </summary>
        public int ID { get; set; }

        /// <summary>
        /// Closure result:
        /// <para>00 = Successful closing</para>
        /// <para>01 = Close fail, try again</para>
        /// <para>02 = Duplicate Lot</para>
        /// </summary>
        public string Result { get; set; }

        /// <summary>
        /// Host, in other words type of supported products
        /// </summary>
        public string Host { get; set; }

        /// <summary>
        /// Identifier of the merchant configured at the point of sale.
        /// </summary>
        public int MerchantID { get; set; }

        /// <summary>
        /// ID terminal number used in the transaction
        /// </summary>
        public int TerminalID { get; set; }

        /// <summary>
        /// Batch number closed in the Terminal
        /// </summary>
        public short BatchNumber { get; set; } = -1;

        /// <summary>
        /// Closing Date
        /// </summary>
        public DateTime BatchDateTime { get; set; } = DateTime.MinValue;

        /// <summary>
        /// Quantity of Return Transactions
        /// </summary>
        public int ReturnsQuantity { get; set; } = -1;

        /// <summary>
        /// Total amount of the Returns
        /// </summary>
        public decimal ReturnsAmount { get; set; } = -1;

        /// <summary>
        /// Total amount of Tax the Returns
        /// </summary>
        public decimal ReturnsTax { get; set; } = -1;

        /// <summary>
        /// Quantity Purchases in the lote
        /// </summary>
        public int PurchasesQuantity { get; set; } = -1;

        /// <summary>
        /// Total Purchases amount
        /// </summary>
        public decimal PurchasesAmount { get; set; } = -1;

        /// <summary>
        /// Total Purchases amount Taxes
        /// </summary>
        public decimal PurchasesTax { get; set; } = -1;

        /// <summary>
        /// Total Other amount taxes
        /// </summary>
        public decimal OtherTax { get; set; } = -1;
    }
}