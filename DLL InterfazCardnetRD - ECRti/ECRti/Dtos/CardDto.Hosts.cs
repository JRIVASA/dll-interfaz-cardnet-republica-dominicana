﻿namespace ECRti.Dtos
{
    public partial class CardDto
    {
        /// <summary>
        /// Host, in other words type of supported products
        /// </summary>
        public enum Hosts
        {
            /// <summary>
            /// Undefined host
            /// </summary>
            Undefined = 0,

            /// <summary>
            /// Credit host
            /// </summary>
            Credit = 6,

            /// <summary>
            /// Debit host
            /// </summary>
            Debit = 7,

            /// <summary>
            /// Amex host
            /// </summary>
            Amex = 8,

            /// <summary>
            /// Privada host
            /// </summary>
            Privada = 9,

            /// <summary>
            /// Peaje host
            /// </summary>
            Peaje = 10,

            /// <summary>
            /// Tpago host
            /// </summary>
            Tpago = 11,

            /// <summary>
            /// Other host
            /// </summary>
            Other = 12
        }
    }
}