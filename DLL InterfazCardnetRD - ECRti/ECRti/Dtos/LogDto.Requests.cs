﻿namespace ECRti.Dtos
{
    /// <summary>
    /// Data Transfer Object for Log
    /// </summary>
    public partial class LogDto
    {
        /// <summary>
        /// Type of request for register a log
        /// </summary>
        public enum Requests
        {
            /// <summary>
            /// Request sent
            /// </summary>
            Sent,

            /// <summary>
            /// Request received
            /// </summary>
            Received
        }
    }
}