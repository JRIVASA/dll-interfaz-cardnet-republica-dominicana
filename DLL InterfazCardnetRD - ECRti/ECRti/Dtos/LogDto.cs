﻿namespace ECRti.Dtos
{
    public partial class LogDto
    {
        /// <summary>
        /// Default contructor for a new object
        /// </summary>
        public LogDto()
        {
        }

        /// <summary>
        /// Initializes a new instance of the LogDto class to
        /// specify the information to be logged
        /// </summary>
        /// <param name="dateTime">Date time for the operation</param>
        /// <param name="serialPortName">Information for communication flow. Example: Serial Port name or Socket</param>
        /// <param name="request">Flow of communication defined in ECRti.Dtos.LogDto.Requests</param>
        /// <param name="description">Information about the type of log</param>
        /// <param name="frame">Frame sended or received</param>
        public LogDto(string dateTime, string serialPortName, Requests request, string description, string frame)
        {
            DateTime = dateTime;
            ConnectionInformation = serialPortName;
            Request = request;
            Description = description;
            Frame = frame;
        }

        /// <summary>
        /// Date time for the operation
        /// </summary>
        public string DateTime { get; set; }

        /// <summary>
        /// Information for communication flow. Example: Serial Port name or Socket
        /// </summary>
        public string ConnectionInformation { get; }

        /// <summary>
        /// Flow of communication defined in ECRti.Dtos.LogDto.Requests
        /// </summary>
        public Requests Request { get; }

        /// <summary>
        /// Information about the type of log
        /// </summary>
        public string Description { get; }

        /// <summary>
        /// Frame sended or received
        /// </summary>
        public string Frame { get; }
    }
}