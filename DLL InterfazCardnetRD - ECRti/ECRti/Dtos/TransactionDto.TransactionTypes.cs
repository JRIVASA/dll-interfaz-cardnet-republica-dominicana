﻿namespace ECRti.Dtos
{
    /// <summary>
    /// Data Transfer Object for a transaction
    /// </summary>
    public partial class TransactionDto
    {
        /// <summary>
        /// Types of transactions with which we can perform operations
        /// </summary>
        public enum TransactionTypes
        {
            /// <summary>
            /// Empty
            /// </summary>
            Empty = 0,

            /// <summary>
            /// Normal Sale
            /// </summary>
            NormalSale = 1,

            /// <summary>
            /// Sale in Quotas
            /// </summary>
            SaleInQuotas = 2,

            /// <summary>
            /// Loyalty
            /// </summary>
            Loyalty = 3,

            /// <summary>
            /// TPago Sale
            /// </summary>
            TPagoSale = 4,

            /// <summary>
            /// Toll Recharge Sale
            /// </summary>
            TollRechargeSale = 5,

            /// <summary>
            /// BanReservas Más Sale
            /// </summary>
            BanReservasMásSale = 6,

            /// <summary>
            /// Cancellation Normal Sale
            /// </summary>
            CancellationNormalSale = 7,

            /// <summary>
            /// Normal Sale Return
            /// </summary>
            NormalSaleReturn = 8,

            /// <summary>
            /// Check In Transaction, apply for Hotels / Rent a Car
            /// </summary>
            CheckIn = 9,

            /// <summary>
            ///Re-Check In
            /// </summary>
            ReCheckIn = 10,

            /// <summary>
            /// Check Out Credit
            /// </summary>
            CheckOutCredit = 11,

            /// <summary>
            /// Check Out AMEX
            /// </summary>
            CheckOutAMEX = 12,

            /// <summary>
            /// Ajust tip for a specific transaction
            /// </summary>
            Tip = 13,

            /// <summary>
            /// Close all transactions in the POS
            /// </summary>
            Closure = 14,

            /// <summary>
            /// Quasi-Cash
            /// </summary>
            QuasiCash = 15,

            /// <summary>
            ///NotShow
            /// </summary>
            NotShow = 16,

            /// <summary>
            /// Room Deposit for Hotel transactions
            /// </summary>
            RoomDeposit = 17,

            /// <summary>
            /// Check In Cancellation for Hotel transactions
            /// </summary>
            CheckInCancellation = 18,

            /// <summary>
            /// Check Out Cancellation for Hotel transactions
            /// </summary>
            CheckOutCancellation = 19
        }
    }
}