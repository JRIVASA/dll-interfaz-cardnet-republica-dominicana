﻿namespace ECRti.Dtos
{
    /// <summary>
    /// Data Transfer Object for Card information
    /// </summary>
    public partial class CardDto
    {
        /// <summary>
        /// Host, in other words type of supported products
        /// </summary>
        public Hosts Host { get; set; }

        /// <summary>
        /// Card Product Name
        /// </summary>
        public CardProducts CardProduct { get; set; }

        /// <summary>
        /// <remarks>
        /// <para>Transaction Mode:</para>
        /// <para>D@5 = Venta con presencia de Banda Magnética</para>
        /// <para> D@6 = Anulación Venta con presencia de Banda Magnética </para>
        /// <para> C@5 = Venta con CHIP (EMV) </para>
        /// <para> C@5 = Venta con CHIP (EMV) </para>
        /// <para> C@6 = Anulación Venta con CHIP (EMV) </para>
        /// <para> F@5 = Venta Fallback </para>
        /// <para> F@6 = Anulación Fallback </para>
        /// <para> A@5 = Venta con Proximidad (Contactless) </para>
        /// <para> A@6 = Anulación Venta con Proximidad (Contactless) </para>
        /// <para> B@5 = Venta con Proximidad (Fallback) </para>
        /// <para> B@6 = Anulación con Proximidad (Fallback) </para>
        /// <para> T@5 = Entrada manual sin presencia del plástico </para>
        /// <para> T@6 = Anulación Venta sin presencia del plástico </para>
        /// </remarks>
        /// </summary>
        public string TransactionMode { get; set; }

        /// <summary>
        /// Account Number Identifier
        /// </summary>
        public string AccountNumber { get; set; }

        /// <summary>
        /// Card Service Code
        /// </summary>
        public short ServiceCode { get; set; }

        /// <summary>
        /// Card Holder Name
        /// </summary>
        public string CardHolderName { get; set; }
    }
}