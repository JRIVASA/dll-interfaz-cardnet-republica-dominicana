﻿using System;
using System.Net;
using System.Net.Sockets;

namespace ECRti
{
    public static partial class Settings
    {
        /// <summary>
        /// Settings for Ethernet communication
        /// </summary>
        public class Ethernet
        {
            /// <summary>
            /// Initializes a new instance of the ECRit.Settings.Ethernet class to
            /// specify the Ethernet Settings to be used to communicate with the POS.
            /// </summary>
            /// <param name="ipLocalEndPointV4">The IPv4 and Port Number of local computer, represented in a System.Net.IPEndPoint Object.</param>
            /// <param name="ipRemoteEndPointV4">The IPv4 and Port Number of the POS, represented in a System.Net.IPEndPoint Object.</param>
            /// <param name="timeOut">The number of milliseconds before a time-out occurs when a send or receive operation does not finish.</param>
            public Ethernet(IPEndPoint ipLocalEndPointV4, IPEndPoint ipRemoteEndPointV4, int timeOut)
            {
                IPLocalEndPointV4 = ipLocalEndPointV4;
                IPRemoteEndPointV4 = ipRemoteEndPointV4;
                TimeOut = timeOut;
            }

            private IPEndPoint ipLocalEndPointV4;

            /// <summary>
            /// The IPv4 and Port Number of local computer, represented in a System.Net.IPEndPoint Object.
            /// </summary>
            public IPEndPoint IPLocalEndPointV4
            {
                get { return ipLocalEndPointV4; }
                set
                {
                    if (!IsIPv4AdressValid(value.Address))
                        throw new ArgumentException(Messages.Warnings.InvalidIPAddressV4, nameof(IPLocalEndPointV4));

                    ipLocalEndPointV4 = value;
                }
            }

            private IPEndPoint ipRemoteEndPointV4;

            /// <summary>
            /// The IPv4 and Port Number of the POS, represented in a System.Net.IPEndPoint Object.
            /// </summary>
            public IPEndPoint IPRemoteEndPointV4
            {
                get { return ipRemoteEndPointV4; }
                set
                {
                    if (!IsIPv4AdressValid(value.Address))
                        throw new ArgumentException(Messages.Warnings.InvalidIPAddressV4, nameof(IPRemoteEndPointV4));

                    ipRemoteEndPointV4 = value;
                }
            }

            /// <summary>
            /// The number of milliseconds before a time-out occurs when a send or receive operation does not finish.
            /// </summary>
            public int TimeOut { get; }

            /// <summary>
            /// Validate if an System.Net.IPAdress is version 4.
            /// </summary>
            /// <param name="ipAddress">The IP Address to validate.</param>
            /// <returns>true: if the IP Address is version 4, otherwise false.</returns>
            internal static bool IsIPv4AdressValid(IPAddress ipAddress)
            {
                return ipAddress.AddressFamily == AddressFamily.InterNetwork;
            }
        }
    }
}