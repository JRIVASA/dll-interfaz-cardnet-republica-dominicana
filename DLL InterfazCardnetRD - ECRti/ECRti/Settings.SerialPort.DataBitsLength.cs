﻿namespace ECRti
{
    public static partial class Settings
    {
        public partial class SerialPort
        {
            /// <summary>
            /// The standard length of data bits per byte
            /// </summary>
            public enum DataBitsLength
            {
                Bits5 = 5,
                Bits6 = 6,
                Bits7 = 7,
                Bits8 = 8
            }
        }
    }
}