﻿using ECRti.Dtos;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Globalization;
using System.Text;
using System.Text.RegularExpressions;

namespace ECRti
{
    /// <summary>
    /// Comunication class that support Ethenet and Serial Port
    /// </summary>
    public abstract partial class Communication : IObservable<LogDto>
    {
        public Communication()
        {
            _observersList = new List<IObserver<LogDto>>();
            _logs = new List<LogDto>();
        }

        private List<IObserver<LogDto>> _observersList { get; }

        private List<LogDto> _logs { get; }

        #region Methods for Observable implementation

        public IDisposable Subscribe(IObserver<LogDto> observer)
        {
            // Check whether observer is already registered. If not, add it
            if (!_observersList.Contains(observer))
            {
                _observersList.Add(observer);

                // Provide observer with existing data.
                foreach (var item in _logs)
                    observer.OnNext(item);
            }
            return new Unsubscriber<LogDto>(_observersList, observer);
        }

        protected void LogNewTransaction(string serialPortName, LogDto.Requests request, string description, string frame)
        {
            var transacction = new LogDto(DateTime.Now.ToString("dd/MM/yyyy hh:mm:ss:fff tt", null), serialPortName, request, description, frame);
            _logs.Add(transacction);
            _observersList.ForEach(obs => obs.OnNext(transacction));
        }

        /// <summary>
        /// Gets log with information about the communication between ECRti and the POS
        /// </summary>
        /// <returns>List of all the logs made</returns>
        public List<LogDto> GetLogs() => _logs;

        protected void Completed()
        {
            _observersList.ForEach(obs => obs.OnCompleted());
            _observersList.Clear();
        }

        #endregion Methods for Observable implementation

        #region Abstract Methods

        protected abstract string[] WriteReadPOS(string frame, bool readOnly = false);

        protected abstract string[] WriteReadPOS(string frame, bool readOnly = false, 
        bool KeepListenerAlive = false, bool ListenerIsAlive = false);

        #endregion Abstract Methods

        protected static string GetRequestType(char value)
        {
            // Declare Local Variables
            switch (value)
            {
                case ControlCharacters.ACK:
                    return Messages.Information.MessageReceivedCorrectly();

                case ControlCharacters.ETX:
                    return Messages.Information.Answer();

                case ControlCharacters.EOT:
                    return Messages.Information.EndOfTransmission();

                case ControlCharacters.ENQ:
                    return Messages.Information.Enquirer();

                case ControlCharacters.SYN:
                    return Messages.Information.SYN();

                default:
                    return string.Empty;
            }
        }

        protected static TransactionDto GetTransaction(
        IReadOnlyCollection<string> fields, TransactionDto Transaction, 
        Boolean DebugMode = false)
        {

            Int64 ReservedPositions = -1;
            Int64 SignaturePositions = -1;

            Int64 CurrentReadField = 0;

            // Loop throw all the fields.

            if (DebugMode)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Transaction Data Received. Field Count: " + fields.Count);
            }

            foreach (var field in fields)
            {

                CurrentReadField++;

                if (DebugMode)
                {
                    Microsoft.VisualBasic.Interaction.MsgBox("Current Field: " + CurrentReadField.ToString() + "\n\n" +
                    "Field Data:" + field);
                }

                // Validate if there is an invalid operation.
                if (field == ControlCharacters.InvalidOperation && fields.Count == 1) throw new WarningException(Messages.Warnings.InvalidFunction);

                // Validate if the Transaction progress.
                if (field == ControlCharacters.TransactionNotProgress && fields.Count == 1) throw new WarningException(Messages.Warnings.TransactionDidNotProgress);

                // Validate if the card is invalid.
                if (fields.Count == 1) throw new WarningException(Messages.Warnings.InvalidCard);

                // Get the Host
                if (Transaction.Host == 0 && Regex.IsMatch(field, @"^\d{2}$") && Enum.TryParse(field, true, out CardDto.Hosts host))
                {
                    Transaction.Host = host;
                    continue;
                }

                // Get the Card Product.
                if (Transaction.CardProduct == 0 && Regex.IsMatch(field, @"^([A-Za-z]|\s){2,8}$") && Enum.TryParse(field, true, out CardDto.CardProducts cardProduct))
                {
                    Transaction.CardProduct = cardProduct;
                    continue;
                }

                // Get the Transaction Mode.
                if (String.IsNullOrWhiteSpace(Transaction.TransactionMode) && Regex.IsMatch(field, "^(D|C|F|A|B|T)@(5|6)"))
                {
                    Transaction.TransactionMode = field;
                    continue;
                }

                // Get the Account Number (Credit Card Number)
                if (Transaction.Host == CardDto.Hosts.Tpago)
                {
                    if (Regex.IsMatch(field, @"^(\w|\s){16}$"))
                    {
                        Transaction.AccountNumber = field;
                        continue;
                    }
                }
                else
                {
                    if (Regex.IsMatch(field.Trim(), @"^\d{6}(\*|X|x){5,6}\d{4}$"))
                    {
                        Transaction.AccountNumber = field;
                        continue;
                    }
                }

                // Get the Batch Number.
                if (Transaction.BatchNumber == 0 && Regex.IsMatch(field, @"^\d{3}$") && short.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out short shortNumber))
                {
                    Transaction.BatchNumber = shortNumber;
                    continue;
                }

                // Get the Transaction Date.
                if (Transaction.TransactionDateTime == DateTime.MinValue && Regex.IsMatch(field, @"^\d{6}$") && DateTime.TryParseExact(field, "ddMMyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime transactionDate))
                {
                    Transaction.TransactionDateTime = transactionDate;
                    continue;
                }

                // Get the Transaction Time.
                if (Transaction.TransactionDateTime.TimeOfDay.Ticks == 0 && Regex.IsMatch(field, @"^\d{6}$") && TimeSpan.TryParseExact(field, "hhmmss", CultureInfo.InvariantCulture, TimeSpanStyles.None, out TimeSpan transactionTime))
                {
                    Transaction.TransactionDateTime = Transaction.TransactionDateTime.AddTicks(transactionTime.Ticks);
                    continue;
                }

                // Get the Card Holder Name
                if (String.IsNullOrWhiteSpace(Transaction.CardHolderName) && Regex.IsMatch(field, "^.{26}$"))
                {
                    Transaction.CardHolderName = field.TrimEnd();
                    continue;
                }

                // Get the Authorization Number.
                if (String.IsNullOrWhiteSpace(Transaction.AuthorizationNumber) && Regex.IsMatch(field, @"^\w{6}$"))
                {
                    Transaction.AuthorizationNumber = field;
                    continue;
                }

                if (Transaction.Host == CardDto.Hosts.Tpago)
                {

                    if (Transaction.TransactionTipAmount == 0 && Regex.IsMatch(field, @"^\d{12}$") && Decimal.TryParse(field.Substring(0,10), NumberStyles.None, CultureInfo.InvariantCulture, out Decimal DecNum))
                    {

                        Transaction.TransactionTipAmount = DecNum;

                        if (Decimal.TryParse(field.Substring(10, 2), NumberStyles.None, CultureInfo.InvariantCulture, out DecNum))
                            Transaction.TransactionTipAmount += (DecNum / 100);

                        continue;
                    }

                    if (Transaction.TransactionTotal == 0 && Regex.IsMatch(field, @"^\d{12}$") && Decimal.TryParse(field.Substring(0, 10), NumberStyles.None, CultureInfo.InvariantCulture, out DecNum))
                    {

                        Transaction.TransactionTotal = DecNum;

                        if (Decimal.TryParse(field.Substring(10, 2), NumberStyles.None, CultureInfo.InvariantCulture, out DecNum))
                            Transaction.TransactionTotal += (DecNum / 100);

                        continue;
                    }

                }

                // Get the Terminal ID.
                if (Transaction.TerminalID == 0 && Regex.IsMatch(field, @"^\d{8}$") && int.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out int terminalID))
                {
                    Transaction.TerminalID = terminalID;
                    continue;
                }

                // Get the Reference Number.
                if (Transaction.ReferenceNumber == 0 && Regex.IsMatch(field, @"^\d{3}$") && short.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out shortNumber))
                {
                    Transaction.ReferenceNumber = shortNumber;
                    continue;
                }

                // Get the Retrieval Reference Number.
                if (Transaction.RetrievalReferenceNumber == 0 && Regex.IsMatch(field, @"^\d{12}$") && long.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out long retrievalReferenceNumber))
                {
                    Transaction.RetrievalReferenceNumber = retrievalReferenceNumber;
                    continue;
                }

                // Get the Merchant ID
                if ((Regex.IsMatch(field, @"^\d{3}3(46|49|50)\d{9}$")) 
                && int.TryParse(field.Substring(3, 9), NumberStyles.None, CultureInfo.InvariantCulture, out int merchantID))
                {
                    Transaction.MerchantID = merchantID;
                    continue;
                } else if ((Regex.IsMatch(field, @"^\d{4}3(46|49|50)\d{8}$"))
                && int.TryParse(field.Substring(4, 9), NumberStyles.None, CultureInfo.InvariantCulture, out int merchantID2))
                {
                    Transaction.MerchantID = merchantID2;
                    continue;
                }

                // Get the Loyalty Deferred Number.
                if (String.IsNullOrWhiteSpace(Transaction.LoyaltyDeferredNumber) && Regex.IsMatch(field, @"^(Q|PU|CU)\d{2}$"))
                {
                    Transaction.LoyaltyDeferredNumber = field.TrimEnd();
                    continue;
                }

                // Get the Transaction ID given by the Brand Name of the Credit Card.
                //if (String.IsNullOrWhiteSpace(Transaction.TransactionID) && Regex.IsMatch(field, @"^\d{15}$"))
                if (String.IsNullOrWhiteSpace(Transaction.TransactionID) && Regex.IsMatch(field, @"^(\w|\s){15}$"))
                {
                    Transaction.TransactionID = field.TrimEnd();
                    continue;
                }

                // Get the Application Identifier.
                if (String.IsNullOrWhiteSpace(Transaction.ApplicationIdentifier) && Regex.IsMatch(field, @"^(\w|\s){16}$"))
                {
                    Transaction.ApplicationIdentifier = field.TrimEnd();
                    continue;
                }

                // Get the Sale Type Indicator (00|BE|PR|PM) |ETC
                if (String.IsNullOrWhiteSpace(Transaction.SaleResultTypeIndicator) && Regex.IsMatch(field, @"^(\w|\s){2}$"))
                {
                    Transaction.SaleResultTypeIndicator = field.TrimEnd().ToUpper();
                    continue;
                }

                if (String.Equals(Transaction.SaleResultTypeIndicator, "00", StringComparison.OrdinalIgnoreCase)
                || String.Equals(Transaction.SaleResultTypeIndicator, "BE", StringComparison.OrdinalIgnoreCase))
                {
                    ReservedPositions = 61;
                    SignaturePositions = 41;
                }
                else if (String.Equals(Transaction.SaleResultTypeIndicator, "DC", StringComparison.OrdinalIgnoreCase))
                {
                    ReservedPositions = 18;
                    SignaturePositions = 41;
                }
                else
                {
                    ReservedPositions = 64;
                    SignaturePositions = 41;
                }

                if (CurrentReadField > 16)
                {
                    // Fallbacks in case SaleResultTypeIndicator is not returned...

                    if (ReservedPositions == (-1))
                        ReservedPositions = 64;

                    if (SignaturePositions == (-1))
                        SignaturePositions = 41;
                }

                if (String.Equals(Transaction.SaleResultTypeIndicator, "DC", StringComparison.OrdinalIgnoreCase))
                {

                    // Attempt to Read DCC Related Fields

                    // Get the DCC Accepted Indicator (Y|N|D) |ETC
                    if (String.IsNullOrWhiteSpace(Transaction.DCCAccepted) && Regex.IsMatch(field, @"^(\w|\s){1}$"))
                    {
                        Transaction.DCCAccepted = field.TrimEnd();
                        continue;
                    }

                    // Get the DCCMarginPercentage (4,2)
                    if (Transaction.DCCMarginPercentage == 0 && Regex.IsMatch(field, @"^\d{4}$") && Decimal.TryParse(field.Substring(0,2), NumberStyles.None, CultureInfo.InvariantCulture, out Decimal DecNum))
                    {

                        Transaction.DCCMarginPercentage = DecNum;

                        if (Decimal.TryParse(field.Substring(2, 2), NumberStyles.None, CultureInfo.InvariantCulture, out DecNum))
                            Transaction.DCCMarginPercentage += (DecNum / 100);

                        continue;
                    }

                    // Get the DCC Transaction Amount (13,2)
                    if (Transaction.DCCTransactionAmount == 0 && Regex.IsMatch(field, @"^\d{15}$") && Decimal.TryParse(field.Substring(0, 13), NumberStyles.None, CultureInfo.InvariantCulture, out DecNum))
                    {

                        Transaction.DCCTransactionAmount = DecNum;

                        if (Decimal.TryParse(field.Substring(13, 2), NumberStyles.None, CultureInfo.InvariantCulture, out DecNum))
                            Transaction.DCCTransactionAmount += (DecNum / 100);

                        continue;
                    }

                    // Get the DCC Display Rate (6,9)
                    if (Transaction.DCCDisplayRate == 0 && Regex.IsMatch(field, @"^\d{15}$") && Decimal.TryParse(field.Substring(0, 6), NumberStyles.None, CultureInfo.InvariantCulture, out DecNum))
                    {

                        Transaction.DCCDisplayRate = DecNum;

                        if (Decimal.TryParse(field.Substring(6, 9), NumberStyles.None, CultureInfo.InvariantCulture, out DecNum))
                            Transaction.DCCDisplayRate += (DecNum / 1000000000);

                        continue;
                    }

                    // Get the DCC Currency Symbol
                    if (String.IsNullOrWhiteSpace(Transaction.DCCCurrencyIdentifier) && Regex.IsMatch(field, @"^(\w|\s){3}$"))
                    {
                        Transaction.DCCCurrencyIdentifier = field.TrimEnd();
                        continue;
                    }

                }
                else if (String.Equals(Transaction.SaleResultTypeIndicator, "PR", StringComparison.OrdinalIgnoreCase)
                || String.Equals(Transaction.SaleResultTypeIndicator, "PM", StringComparison.OrdinalIgnoreCase))
                {

                    // Attempt to Read PROMONET Related Fields

                    // Get the Program ID
                    if (String.IsNullOrWhiteSpace(Transaction.PromonetProgramID) && Regex.IsMatch(field, @"^(\w|\s){4}$"))
                    {
                        Transaction.PromonetProgramID = field.TrimEnd();
                        continue;
                    }

                    // Get the Program Name
                    if (String.IsNullOrWhiteSpace(Transaction.PromonetProgramName) && Regex.IsMatch(field, @"^.{15}$"))
                    {
                        Transaction.PromonetProgramName = field.TrimEnd();
                        continue;
                    }

                }

                if (ReservedPositions != (-1))
                    // Get all the Reserved Information.
                    if (String.IsNullOrWhiteSpace(Transaction.Reserved) && Regex.IsMatch(field, @"^.{" + ReservedPositions.ToString() + "}$"))
                    {
                        Transaction.Reserved = field.TrimEnd();
                        continue;
                    }

                if (SignaturePositions != (-1))
                {

                    // Get the Signature of the Transaction.
                    /*if (Transaction.Signature == null && Regex.IsMatch(field.TrimEnd(), @"^(\w|\s){41}$"))
                    {
                        Transaction.Signature = Encoding.ASCII.GetBytes(field);
                        continue;
                    }*/

                    if (String.IsNullOrWhiteSpace(Transaction.DigitalSignatureFileName) && Regex.IsMatch(field, @"^.{" + SignaturePositions.ToString() + "}$"))
                    {
                        //Transaction.Signature = Encoding.ASCII.GetBytes(field);
                        Transaction.DigitalSignatureFileName = field.TrimEnd();
                        continue;
                    }

                }

                if (String.Equals(Transaction.SaleResultTypeIndicator, "PR", StringComparison.OrdinalIgnoreCase)
                || String.Equals(Transaction.SaleResultTypeIndicator, "PM", StringComparison.OrdinalIgnoreCase))
                {

                    // Attempt to Read PROMONET Related Fields

                    // Get the Program Message
                    if (String.IsNullOrWhiteSpace(Transaction.PromonetMessage) && Regex.IsMatch(field, @"^.{398}$"))
                    {
                        Transaction.PromonetMessage = field.TrimEnd();
                        continue;
                    }

                    // Get the Program QR Code
                    if (String.IsNullOrWhiteSpace(Transaction.PromonetQRCode) && Regex.IsMatch(field, @"^.{500}$"))
                    {
                        Transaction.PromonetQRCode = field.TrimEnd();
                        continue;
                    }

                }

                if (Transaction.Reserved != null)
                {

                    // Additional Fields

                    if (String.IsNullOrWhiteSpace(Transaction.DigitalSignaturePerformed) && Regex.IsMatch(field, @"^(S|N)"))
                    {
                        Transaction.DigitalSignaturePerformed = field.TrimEnd();
                        continue;
                    }

                    if (String.IsNullOrWhiteSpace(Transaction.ResponseMessage) && Regex.IsMatch(field, @"^.{25}$"))
                    {
                        Transaction.ResponseMessage = field.TrimEnd();
                        continue;
                    }

                    if (String.IsNullOrWhiteSpace(Transaction.SignatureModeIndicator) && Regex.IsMatch(field, @"^(0|1|2)$"))
                    {
                        Transaction.SignatureModeIndicator = field.TrimEnd();
                        continue;
                    }

                    if (String.IsNullOrWhiteSpace(Transaction.FormattedExpirationDate) && Regex.IsMatch(field, @"^\d{2}\/\d{2}$"))
                    {
                        Transaction.FormattedExpirationDate = field.TrimEnd();
                        continue;
                    }

                    if (String.IsNullOrWhiteSpace(Transaction.VoucherMode) && Regex.IsMatch(field, @"^(1|2|3|4|5|6)$"))
                    {
                        Transaction.VoucherMode = field.TrimEnd();
                        continue;
                    }

                    if (String.IsNullOrWhiteSpace(Transaction.EMVApplicationLevel) && Regex.IsMatch(field, @"^(\w|\s){12}$"))
                    {
                        Transaction.EMVApplicationLevel = field.TrimEnd();
                        continue;
                    }

                    if (String.IsNullOrWhiteSpace(Transaction.QuickPaymentIndicator) && Regex.IsMatch(field, @"^(0|1)$"))
                    {
                        Transaction.QuickPaymentIndicator = field.TrimEnd();
                        continue;
                    }

                    if (String.IsNullOrWhiteSpace(Transaction.PINModeIndicator) && Regex.IsMatch(field, @"^(0|1|2)$"))
                    {
                        Transaction.PINModeIndicator = field.TrimEnd();
                        continue;
                    }

                }

                /*
                // Get the Application Version used.
                if (!Regex.IsMatch(field, @"^.{35}$")) continue;
                Transaction.ApplicationVersion = field.TrimEnd();
                */
            }

            return Transaction;

        }

        protected static ClosureDto GetClosure(IReadOnlyCollection<string> fields, ClosureDto closure)
        {
            // Validate First
            if (fields == null) throw new ArgumentNullException(nameof(fields));

            // Loop throw all the fields on the collection.
            foreach (var field in fields)
            {
                // Get Result
                if (string.IsNullOrWhiteSpace(closure.Result) && Regex.IsMatch(field, @"^\d{2}$"))
                {
                    closure.Result = field;
                    continue;
                }

                // Get the Host
                if (string.IsNullOrWhiteSpace(closure.Host) && Regex.IsMatch(field, @"^.{8}$"))
                {
                    closure.Host = field;
                    continue;
                }

                // Get the Merchant ID
                if (closure.MerchantID == 0 && Regex.IsMatch(field.TrimEnd(), @"^\d{3}3(46|49|50)\d{9}$") && int.TryParse(field.Substring(3, 9), NumberStyles.None, CultureInfo.InvariantCulture, out int integerValue))
                {
                    closure.MerchantID = integerValue;
                    continue;
                }

                // Get the Terminal ID.
                if (closure.TerminalID == 0 && Regex.IsMatch(field, @"^\d{8}$") && int.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out integerValue))
                {
                    closure.TerminalID = integerValue;
                    continue;
                }

                // Get the Batch Number.
                if (closure.BatchNumber == -1 && Regex.IsMatch(field, @"^\d{3}$") && short.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out short shortValue))
                {
                    closure.BatchNumber = shortValue;
                    continue;
                }

                // Get the Transaction Date.
                if (closure.BatchDateTime == DateTime.MinValue && Regex.IsMatch(field, @"^\d{6}$") && DateTime.TryParseExact(field, "ddMMyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out DateTime dateTimeValue))
                {
                    closure.BatchDateTime = dateTimeValue;
                    continue;
                }

                // Get the Transaction Time.
                if (closure.BatchDateTime.TimeOfDay.Ticks == 0 && Regex.IsMatch(field, @"^\d{6}$") && TimeSpan.TryParseExact(field, "hhmmss", CultureInfo.InvariantCulture, TimeSpanStyles.None, out TimeSpan timeSpanValue))
                {
                    closure.BatchDateTime = closure.BatchDateTime.AddTicks(timeSpanValue.Ticks);
                    continue;
                }

                // Get the Total Quantity Return.
                if (closure.ReturnsQuantity == -1 && Regex.IsMatch(field, @"^\d{3}$") && short.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out shortValue))
                {
                    closure.ReturnsQuantity = shortValue;
                    continue;
                }

                // Get the Total Amount Return.
                if (closure.ReturnsAmount == -1 && Regex.IsMatch(field, @"^\d{12}$") && decimal.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out decimal decimalValue))
                {
                    closure.ReturnsAmount = decimalValue / 100;
                    continue;
                }

                // Get the Total Tax Return.
                if (closure.ReturnsTax == -1 && Regex.IsMatch(field, @"^\d{12}$") && decimal.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out decimalValue))
                {
                    closure.ReturnsTax = decimalValue / 100;
                    continue;
                }

                // Get the Total Quantity Purchases.
                if (closure.PurchasesQuantity == -1 && Regex.IsMatch(field, @"^\d{3}$") && short.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out shortValue))
                {
                    closure.PurchasesQuantity = shortValue;
                    continue;
                }

                // Get the Total Amount Purchases.
                if (closure.PurchasesAmount == -1 && Regex.IsMatch(field, @"^\d{12}$") && decimal.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out decimalValue))
                {
                    closure.PurchasesAmount = decimalValue / 100;
                    continue;
                }

                // Get the Total Amount Purchases.
                if (closure.PurchasesTax == -1 && Regex.IsMatch(field, @"^\d{12}$") && decimal.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out decimalValue))
                {
                    closure.PurchasesTax = decimalValue / 100;
                    continue;
                }

                // Get the Total Amount Other Taxes.
                if (closure.OtherTax != -1 || !Regex.IsMatch(field, @"^\d{12}$") || !decimal.TryParse(field, NumberStyles.None, CultureInfo.InvariantCulture, out decimalValue)) continue;

                closure.OtherTax = decimalValue / 100;
            }

            return closure;
        }
    }
}