﻿namespace ECRti
{
    public static partial class Settings
    {
        /// <summary>
        /// Format used to represent date values
        /// </summary>
        internal const string DateFormat = "ddMMyy";
    }
}