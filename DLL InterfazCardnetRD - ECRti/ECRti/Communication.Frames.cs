﻿using ECRti.Dtos;
using System;
using System.Collections.Generic;
using System.Text;

namespace ECRti
{
    public abstract partial class Communication
    {
        /// <summary>
        /// Frames to communicate to the POS
        /// </summary>
        public struct Frames
        {
            /// <summary>
            /// Multiple Acquirer ID Messages
            /// </summary>
            /// <param name="acquirerID">ID of the acquirer</param>
            /// <returns>Acquirer Identification frame</returns>
            public static string MultiAcquirer(int? acquirerID = null)
            {
                if (acquirerID == null)
                    return "{0:00}";

                return $"{acquirerID:00}";
            }

            /// <summary>
            /// Multi Merchant Partial Frame
            /// </summary>
            public static string MultiMerchant => "{0:00}";

            /// <summary>
            /// Normal Retail's Frame
            /// </summary>
            public static string Retail => "CN00{0}{1:000000000000}{2:000000000000}{3:000000000000}{4:000000}{5}";

            /// <summary>
            /// Return Retail's Frame
            /// </summary>
            public static string Return => "CN03{0}{1:000000000000}{2:000000000000}{3:000000000000}{4:000000}{5}";

            /// <summary>
            /// Points Retail's Frame, this type of sale is made with points.
            /// </summary>
            public static string RetailLoyalty => "PU00{0}{1:000000000000}{2:000000000000}{3:000000000000}{4:000000}{5}";

            /// <summary>
            /// Deferred Retail's Frame, this type of sale is splitted.
            /// </summary>
            public static string RetailDeferred => "CU00{0}{1:000000000000}{2:000000000000}{3:000000000000}{4:000000}{5:00}{6}";

            /// <summary>
            /// TPago Retail's Frame, this type of sale with a phone number.
            /// </summary>
            public static string RetailTPago => "TP00{0}{1:000000000000}{2:000000000000}{3:000000000000}{4:000000}{5}";

            /// <summary>
            /// Peaje Retail's Frame, this type of sale is a credit on the card-holder.
            /// </summary>
            public static string Peaje => "PA00{0}{1:000000000000}{2:000000}{3}";

            /// <summary>
            /// BR Más Retail's Frame, this type of sale is a credit on the card-holder.
            /// </summary>
            public static string BRMas => "BR00{0}{1:000000000000}{2:000000}{3}";

            /// <summary>
            /// Check In's Frame, this type of transaction occurs when the user rent a car or checks in on a hotel.
            /// </summary>
            public static string CheckIn => "CH00{0}{1:00000000}{2:000000}{3:000000}{4:000000000000}{5:000000000000}{6:000000000000}{7:000000}{8}";

            /// <summary>
            /// Check Out's Frame, this type of transaction occurs when the user rent a car or checks out on a hotel.
            /// </summary>
            public static string CheckOut => "CH01{0}{1:00000000}{2:000000000000}{3}";

            /// <summary>
            /// Check Out AMEX's Frame, this type of transaction occurs when the user rent a car or checks out on a hotel.
            /// </summary>
            public static string CheckOutAMEX => "CH05{0}{1:000000}{2:000000000000}{3}";

            /// <summary>
            /// Increment Check In's Frame, this type of transaction occurs when the user rent a car or  re-checks in on a hotel.
            /// </summary>
            public static string CheckReCheckIn => "IC00{0}{1:00000000}{2:000000000000}{3:000000000000}{4:000000000000}{5}";

            /// <summary>
            /// This type of transactions belongs to Casinos.
            /// </summary>
            public static string QuasiCash => "QC00{0}{1:000000000000}{2:000000000000}{3:000000000000}{4:000000}{5}";

            /// <summary>
            /// This type of transactions belongs to Hotels.
            /// </summary>
            public static string NotShown => "NS00{0}{1:000000000000}{2:000000000000}{3:000000000000}{4:000000}{5}";

            /// <summary>
            /// This type of transactions belongs to Hotels.
            /// </summary>
            public static string RoomDeposit => "RD00{0}{1:000000000000}{2:000000000000}{3:000000000000}{4:000000}{5}";

            /// <summary>
            /// Abort a retail transaction.
            /// </summary>
            public static string RetailAbort => "CN02{0}{1:00}{2:000}{3}";

            /// <summary>
            /// Abort a Check In transaction.
            /// </summary>
            public static string CheckInAbort => "CH02{0}{1:00}{2:00000000}{3:000}{4}";

            /// <summary>
            /// Tip or Readjust Transaction
            /// </summary>
            public static string Tip => "AV00{0}{1:00}{2:000}{3:000000000000}{4}";

            /// <summary>
            /// Closes the Batch that belongs to the Merchant ID.
            /// </summary>
            public static string ClosingBatch => "CN01{0}";

            /// <summary>
            /// Exclusive transaction for payments and services for communication providers
            /// </summary>
            /// <param name="acquirerID">ID of the acquirer</param>
            /// <returns>Service frame for the acquirer</returns>
            public static string Services(int acquirerID)
            {
                return $"SV{acquirerID:00}";
            }

            /// <summary>
            /// Card Consultation
            /// </summary>
            public static string CardConsultation => "CS00";

            /// <summary>
            /// Build the frame to process one transaction or transactions massively
            /// </summary>
            /// <param name="TransactionTypes"></param>
            /// <param name="transaction"></param>
            /// <returns></returns>
            internal static string BuildFrameToAutorizeTransaction(TransactionDto.TransactionTypes TransactionTypes, TransactionDto transaction)
            {
                switch (TransactionTypes)
                {
                    case TransactionDto.TransactionTypes.CheckIn:
                        return string.Format(CheckIn, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.Folio, // 1 - Folio Number
                            transaction.CheckInDate.ToString(Settings.DateFormat), // 2 - Entry Date
                            transaction.CheckOutDate.ToString(Settings.DateFormat), // 3 - Exit Date
                            transaction.TransactionAmount * 100, // 4 - Transaction Amount
                            transaction.TransactionITBIS * 100, // 5 - ITBIS Amount
                            transaction.TransactionOtherTaxes * 100, // 6 - Other Taxes Amount
                            transaction.InvoiceNumber, // 7 - Invoice Ticket Number
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 8 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.NormalSale:
                        return string.Format(Retail, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.TransactionAmount * 100, // 1 - Transaction Amount
                            transaction.TransactionITBIS * 100, // 2 - ITBIS Amount
                            transaction.TransactionOtherTaxes * 100, // 3 - Other Taxes Amount
                            transaction.InvoiceNumber, // 4 - Invoice Ticket Number
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 5 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.SaleInQuotas:

                        return string.Format(RetailDeferred, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.TransactionAmount * 100, // 1 - Transaction Amount
                            transaction.TransactionITBIS * 100, // 2 - ITBIS Amount
                            transaction.TransactionOtherTaxes * 100, // 3 - Other Taxes Amount
                            transaction.InvoiceNumber, // 4 - Invoice Ticket Number
                            transaction.NumberOfShares.Value, // 5 - Number of Shares
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 6 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.Loyalty:
                        return string.Format(RetailLoyalty, //  Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.TransactionAmount * 100, // 1 - Transaction Amount
                            transaction.TransactionITBIS * 100, // 2 - ITBIS Amount
                            transaction.TransactionOtherTaxes * 100, // 3 - Other Taxes Amount
                            transaction.InvoiceNumber, // 4 - Invoice Ticket Number
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 5 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.TPagoSale:
                        return string.Format(RetailTPago, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.TransactionAmount * 100, // 1 - Transaction Amount
                            transaction.TransactionITBIS * 100, // 2 - ITBIS Amount
                            transaction.TransactionOtherTaxes * 100, // 3 - Other Taxes Amount
                            transaction.InvoiceNumber, // 4 - Invoice Ticket Number
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 5 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.TollRechargeSale:

                        return string.Format(Peaje, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.TotalAmount.Value * 100, // 1 - Transaction Amount
                            transaction.InvoiceNumber, // 2 - Invoice Ticket Number
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 3 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.BanReservasMásSale:

                        return string.Format(BRMas, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.TransactionAmount * 100, // 1 - Transaction Amount
                            transaction.InvoiceNumber, // 2 - Invoice Ticket Number
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 3 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.NormalSaleReturn:
                        return string.Format(Return, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.TransactionAmount * 100, // 1 - Transaction Amount
                            transaction.TransactionITBIS * 100, // 2 - ITBIS Amount
                            transaction.TransactionOtherTaxes * 100, // 3 - Other Taxes Amount
                            transaction.InvoiceNumber, // 4 - Invoice Ticket Number
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 5 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.QuasiCash:

                        return string.Format(QuasiCash, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.TransactionAmount * 100, // 1 - Transaction Amount
                            transaction.TransactionITBIS * 100, // 2 - ITBIS Amount
                            transaction.TransactionOtherTaxes * 100, // 3 - Other Taxes Amount
                            transaction.InvoiceNumber, // 4 - Invoice Ticket Number
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 5 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.RoomDeposit:

                        return string.Format(RoomDeposit, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.TransactionAmount * 100, // 1 - Transaction Amount
                            transaction.TransactionITBIS * 100, // 2 - ITBIS Amount
                            transaction.TransactionOtherTaxes * 100, // 3 - Other Taxes Amount
                            transaction.InvoiceNumber, // 4 - Invoice Ticket Number
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 5 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.NotShow:

                        return string.Format(NotShown, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.TransactionAmount * 100, // 1 - Transaction Amount
                            transaction.TransactionITBIS * 100, // 2 - ITBIS Amount
                            transaction.TransactionOtherTaxes * 100, // 3 - Other Taxes Amount
                            transaction.InvoiceNumber, // 4 - Invoice Ticket Number
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 5 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.ReCheckIn:
                        return string.Format(CheckReCheckIn, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.Folio, // 1 - Folio Number
                            transaction.TransactionAmount * 100, // 2 - Transaction Amount
                            transaction.TransactionITBIS * 100, // 3 - ITBIS Amount
                            transaction.TransactionOtherTaxes * 100, // 4 - Other Taxes Amount
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 5 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.CheckOutCredit:
                        return string.Format(CheckOut, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.Folio, // 1 - Folio Number
                            transaction.TransactionAmount * 100, // 2 - Transaction Amount
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 3 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.CheckOutAMEX:
                        return string.Format(CheckOutAMEX, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            transaction.AuthorizationNumber, // 1 - Authorization Number
                            transaction.TransactionAmount * 100, // 2 - Transaction Amount
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 3 - Multi Acquirer ID, if apply
                            );

                    default:
                        return string.Empty;
                }
            }

            internal static string BuildFrameToCancelTransaction(TransactionDto.TransactionTypes TransactionTypes, TransactionDto transaction)
            {
                // Select the Transaction Type
                switch (TransactionTypes)
                {
                    case TransactionDto.TransactionTypes.CancellationNormalSale:
                        return string.Format(RetailAbort, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            (int)transaction.Host, // 1 - Host
                            transaction.ReferenceNumber, // 2 - Reference Number
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 3 - Multi Acquirer ID, if apply
                            );

                    case TransactionDto.TransactionTypes.CheckInCancellation:
                        return string.Format(CheckInAbort, // Format
                            transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                            (int)transaction.Host, // 1 - Host
                            transaction.Folio, // 2 - Folio Number
                            transaction.ReferenceNumber, // 3 - Reference Number
                            transaction.MultiAcquirerID.ToString().Replace("0", "") // 4 - Multi Acquirer ID, if apply
                            );

                    default:
                        return string.Empty;
                }
            }

            internal static string BuildFrameToUpdateTipAmount(TransactionDto transaction)
            {
                return string.Format(Tip, // Format
                    transaction.MultiMerchantID.ToString().Replace("0", ""), // 0 - Multi Merchant ID, if apply
                    (int)transaction.Host, // 1 - Host
                    transaction.ReferenceNumber, // 2 - Reference Number
                    transaction.TransactionTipAmount, // 3 - Tip Amount
                    transaction.MultiAcquirerID.ToString().Replace("0", "") // 4 - Multi Acquirer ID, if apply
                    );
            }

            internal static string BuildFrameToExecuteClosure(bool isMultiMerchantEnabled, byte? multiMerchantID = null)
            {
                if (isMultiMerchantEnabled && multiMerchantID == null)
                    throw new ArgumentNullException("If the parameter isMultiMerchantEnabled is true you have to supply a Multi Merchant ID in the second parameter");

                if (isMultiMerchantEnabled)
                {
                    if (multiMerchantID <= 0)
                        throw new ArgumentOutOfRangeException(nameof(multiMerchantID), "Should be greater than zero(0)");

                    return string.Format(isMultiMerchantEnabled
                        ? string.Format(MultiMerchant, ControlCharacters.ClousureMerchantID)
                        : string.Empty);
                }
                else
                {
                    return string.Format(ClosingBatch, isMultiMerchantEnabled
                        ? string.Format(MultiMerchant, multiMerchantID)
                        : string.Empty);
                }
            }

            internal static byte[] FrameToBytes(string frame)
            {
                // Convert String to Bytes
                var bytes = (new ASCIIEncoding()).GetBytes(frame);

                // if the length of the frame is equal to one then return the byte array.
                if (frame.Length == 1) return bytes;

                // Add a byte at the end to insert the LRC
                var buffer = new byte[bytes.Length + 1];
                bytes.CopyTo(buffer, 0);

                // Calculate the LCR and add to the end of the array.
                buffer[buffer.Length - 1] = GetLRC(bytes);

                // Return the Converted Array
                return buffer;
            }

            private static byte GetLRC(IReadOnlyList<byte> data)
            {
                var checksum = data[1];
                for (var i = 2; i <= data.Count - 1; i++) checksum ^= data[i];
                return checksum;
            }

            /// <summary>
            /// Clear a frame
            /// </summary>
            /// <param name="frame"></param>
            /// <returns>Frame without the following ControlCharacters: STX, ETX</returns>
            internal static string ClearFrame(string frame)
            {
                return frame.Replace(ControlCharacters.STX.ToString(), "")
                    .Replace(ControlCharacters.ETX.ToString(), "");
            }

            /// <summary>
            /// Clear the response of the POS
            /// </summary>
            /// <param name="data"></param>
            /// <returns>Data without the following ControlCharacters: ENQ, EOT</returns>
            internal static string CleanedDataResponse(string data)
            {
                return data.Replace(ControlCharacters.ENQ.ToString(), "")
                    .Replace(ControlCharacters.EOT.ToString(), "")
                    .Replace("\0", "");
            }
        }
    }
}