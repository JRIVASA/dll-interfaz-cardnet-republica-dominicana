﻿namespace ECRti
{
    internal partial struct Messages
    {
        public struct Information
        {
            public static string MessageReceivedCorrectly()
            {
                return "Acknowledgement";
            }

            public static string Answer()
            {
                return "Respuesta";
            }

            public static string EndOfTransmission()
            {
                return "Fin de la Transmisión";
            }

            public static string Frame()
            {
                return "Trama";
            }

            public static string Enquirer()
            {
                return "Enquire";
            }

            public static string SYN()
            {
                return "Synchronous Idle";
            }

            public static string SentToPOS(string localEndPoint, string remoteEndPoint)
            {
                return $"{localEndPoint} --> {remoteEndPoint}";
            }

            public static string ReceivedFromPOS(string localEndPoint, string remoteEndPoint)
            {
                return $"{localEndPoint} <-- {remoteEndPoint}";
            }
        }
    }
}