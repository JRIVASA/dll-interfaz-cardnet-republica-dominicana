﻿using ECRti.Dtos;
using System;
using System.Collections.Generic;

namespace ECRti
{
    internal class LogMonitor : IObserver<LogDto>
    {
        private readonly List<LogDto> _logs = new List<LogDto>();
        private IDisposable _cancellation;

        public List<LogDto> GetLogs()
        {
            return _logs;
        }

        public virtual void Subscribe(Communication provider)
        {
            _cancellation = provider.Subscribe(this);
        }

        public virtual void Unsubscribe()
        {
            _cancellation.Dispose();
            _logs.Clear();
        }

        public void OnCompleted()
        {
            _logs.Clear();
        }

        public void OnError(Exception error)
        {
            // No implementation.
        }

        public void OnNext(LogDto value)
        {
            _logs.Add(value);
        }
    }
}