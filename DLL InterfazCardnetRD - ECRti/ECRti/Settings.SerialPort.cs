﻿using System.IO.Ports;

namespace ECRti
{
    public static partial class Settings
    {
        /// <summary>
        /// Settings for Serial Port communication
        /// </summary>
        public partial class SerialPort
        {
            /// <summary>
            /// Initializes a new instance of the ECRit.Settings.SerialPort class to
            /// specify the SerialPort Settings to be used.
            /// </summary>
            /// <param name="portName">The port to use (for example, COM1).</param>
            /// <param name="boudRate">The baud rate.</param>
            /// <param name="dataBits">The data bits value.</param>
            /// <param name="parity">One of the System.IO.Ports.SerialPort.Parity values.</param>
            /// <param name="stopBits">One of the System.IO.Ports.SerialPort.StopBits values.</param>
            /// <param name="readTimeout">The number of milliseconds before a time-out occurs when a read operation does not finish.</param>
            /// <param name="writeTimeout">The number of milliseconds before a time-out occurs when a write operation does not finish.</param>
            public SerialPort(string portName, Baud boudRate, DataBitsLength dataBits, Parity parity, StopBits stopBits, int readTimeout, int writeTimeout)
            {
                PortName = portName;
                BoudRate = boudRate;
                DataBits = dataBits;
                Parity = parity;
                StopBits = stopBits;
                ReadTimeout = readTimeout;
                WriteTimeout = writeTimeout;
            }

            internal const int MaximumNumberOfRetransmissions = 2;

            /// <summary>
            /// The port to use (for example, COM1).
            /// </summary>
            public string PortName { get; }

            /// <summary>
            /// The baud rate to use in the communication
            /// </summary>
            public Baud BoudRate { get; }

            /// <summary>
            /// The standard length of data bits per byte to be use in the communication
            /// </summary>
            public DataBitsLength DataBits { get; }

            /// <summary>
            /// The Parity to use in the communication
            /// </summary>
            public Parity Parity { get; }

            /// <summary>
            /// The StopBits to use in the communication
            /// </summary>
            public StopBits StopBits { get; }

            /// <summary>
            /// The number of milliseconds before a time-out occurs when a read operation does not finish
            /// </summary>
            public int ReadTimeout { get; }

            /// <summary>
            /// The number of milliseconds before a time-out occurs when a write operation does not finish
            /// </summary>
            public int WriteTimeout { get; }
        }
    }
}