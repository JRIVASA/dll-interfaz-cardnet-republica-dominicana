﻿using ECRti.Dtos;
using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace ECRti
{
    /// <summary>
    /// All available actions to communicate with the Point Of Sales through Ethernet.
    /// </summary>
    public class EthernetCommunication : Communication, ICommunication
    {
        public Socket listener;
        public Socket socket;
        private string ecrToPos = "";
        private string posToEcr = "";

        private Boolean DebugMode;
        public Boolean Prop_DebugMode   // property
        {
            get { return DebugMode; }
            set { DebugMode = value; }
        }

        /// <summary>
        /// Initializes a new instance of the ECRti.EthernetCommunication class using the
        /// specified Ethernet Settings to communicate with the Point Of Sales (POS).
        /// </summary>
        /// <param name="ethernetSetting">Ethernet Settings to be used.</param>
        public EthernetCommunication(Settings.Ethernet ethernetSetting)
        {

            _currentSetting = ethernetSetting;
            listener = new Socket(_currentSetting.IPLocalEndPointV4.AddressFamily, SocketType.Stream, ProtocolType.Tcp);
            String AppPath = System.Reflection.Assembly.GetExecutingAssembly().Location;

            if (System.IO.File.Exists("ElecDebugMode.Active") || System.IO.Directory.Exists("ElecDebugMode.Active"))
            {
                DebugMode = true;
                Microsoft.VisualBasic.Interaction.MsgBox("DebugMode Enabled" + "\n" + AppPath);
            }

            System.Net.ServicePointManager.SecurityProtocol =
            System.Net.SecurityProtocolType.Ssl3
            | System.Net.SecurityProtocolType.Tls12
            | SecurityProtocolType.Tls11
            | SecurityProtocolType.Tls;

            ServicePointManager.Expect100Continue = true;

            System.Net.ServicePointManager.ServerCertificateValidationCallback = (sender, certificate, chain, errors) => {
                return true;
            };

        }

        private Settings.Ethernet _currentSetting;

        /// <summary>
        /// Update the tip amount of a transaction
        /// </summary>
        /// <param name="transactionDto">Transaction to update tip amount</param>
        /// <returns>String equivalent to a Communication.ControlCharacters. If successful Returns ControlCharacters.SuccessfulTip</returns>
        /// 
        public string AdjustTipAmount(TransactionDto transactionDto)
        {
            var frame = Frames.BuildFrameToUpdateTipAmount(transactionDto);
            return WriteReadPOS(frame, false)[0];
        }

        /// <summary>
        /// Authorize a service for a specific acquirer
        /// </summary>
        /// <param name="MultiAcquirerID">Acquire ID to authorize the service</param>
        public void AuthorizeService(byte MultiAcquirerID)
        {
            var frame = Frames.Services(MultiAcquirerID);
            WriteReadPOS(frame, false);
        }

        public TransactionDto AuthorizeTransaction(TransactionDto.TransactionTypes transactionType,
        TransactionDto transactionDto)
        {
            return AuthorizeTransaction(transactionType, transactionDto, false);
        }

        /// <summary>
        /// Authorize transaction of types defined in TransactionDto.TransactionTypes. Example: TransactionTypes.NormalSale, TransactionTypes.CheckIn
        /// </summary>
        /// <param name="transactionType">Type of transaction to authorize</param>
        /// <param name="transactionDto">Transaction to authorize</param>
        /// <param name="ListenerIsAlive">ListenerIsAlive</param> 
        /// <returns>Trasaction with information about the autorization</returns>
        public TransactionDto AuthorizeTransaction(TransactionDto.TransactionTypes transactionType,
        TransactionDto transactionDto, bool ListenerIsAlive = false)
        {

            /*if (DebugMode)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Starting Authorize Transaction. Building Frame");
            }*/

            var frame = Frames.BuildFrameToAutorizeTransaction(transactionType, transactionDto);

            if (DebugMode)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("frame to send: " + frame);
            }

            /*
            
            // TESTING RESPONSE HANDLING

            String[] Resp = ("06|VISA    |C@5|542298xxxxxx6290|151|160323|140526|LUIS ARRAGA               |" +
            "158260|20202020|307|123456789321|000349111110000|102030405060708|EMVAPPLEVEL12345|DC|Y|0400|" +
            "000000000001832|000061236899563|USD|                  |02000234090211.bmp                       |" +
            "S|APROBADA                 |1|00/00|1|CARDNET     |0|0").Split('|');

            return GetTransaction(Resp, transactionDto, DebugMode);
            
            //*/

            String[] ResponseFields = WriteReadPOS(frame, transactionDto.HasDiscount,
            false, ListenerIsAlive);

            return GetTransaction(ResponseFields, transactionDto, DebugMode);

        }

        /// <summary>
        /// Cancel a transaction of a determined type
        /// </summary>
        /// <param name="transactionType">Type of transaction to cancel. Example: CancellationNormalSale or CheckInCancellation</param>
        /// <param name="transactionDto">Transaction to cancel.</param>
        /// <returns>String equivalent to a Communication.ControlCharacters. If successful Returns ControlCharacters.SuccessfulCancellation</returns>
        public string CancelTransaction(TransactionDto.TransactionTypes transactionType, TransactionDto transactionDto)
        {

            var frame = Frames.BuildFrameToCancelTransaction(transactionType, transactionDto);

            if (DebugMode)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("frame to send: " + frame);
            }

            return WriteReadPOS(frame, false)[0];

        }

        /// <summary>
        /// Execute the close transactions in the POS..
        /// </summary>
        /// <param name="isMultiMerchantEnabled">Indicate if there are multi merchant transactions</param>
        /// <param name="multiMerchantID">If Multi Merchant is enabled, provide the Multi Merchant ID to execute the his close</param>
        /// <returns>Detail about the close by card host</returns>
        public List<ClosureDto> ExecuteClosure(bool isMultiMerchantEnabled, byte? multiMerchantID = null)
        {
            var frame = Frames.BuildFrameToExecuteClosure(isMultiMerchantEnabled, multiMerchantID);
            var answer = WriteReadPOS(frame, false);

            if (answer == null || answer[0] == ControlCharacters.EmptyBatch) return null;

            var closures = new List<ClosureDto>();

            foreach (var closure in answer)
            {
                var closureDto = new ClosureDto();
                GetClosure(closure.Split(ControlCharacters.FS), closureDto);

                closures.Add(closureDto);
            }

            return closures;
        }

        /// <summary>
        /// Get card holder information
        /// </summary>
        /// <returns>Information about card holder</returns>
        public CardDto RequestCardInformation()
        {

            var data = WriteReadPOS(Frames.CardConsultation, false, false, false);

            if (data.Length != 6) return null;

            if (!Enum.TryParse(data[0], true, out CardDto.Hosts host)) throw new FormatException();
            if (!Enum.TryParse(data[1], true, out CardDto.CardProducts cardProduct)) throw new FormatException();
            if (!short.TryParse(data[4], out short serviceCode)) throw new FormatException();

            return new CardDto
            {
                Host = host,
                CardProduct = cardProduct,
                TransactionMode = data[2],
                AccountNumber = data[3],
                ServiceCode = serviceCode,
                CardHolderName = data[5]
            };

        }

        /// <summary>
        /// Get card holder information. Keeps Listener Alive for a follow up transaction.
        /// </summary>
        /// <returns>Information about card holder</returns>
        
        public CardDto RequestCardInformation_KeepListenerAlive()
        {

            var data = WriteReadPOS(Frames.CardConsultation, false, true, false);

            if (data.Length != 6) return null;

            if (!Enum.TryParse(data[0], true, out CardDto.Hosts host)) throw new FormatException();
            if (!Enum.TryParse(data[1], true, out CardDto.CardProducts cardProduct)) throw new FormatException();
            if (!short.TryParse(data[4], out short serviceCode)) throw new FormatException();

            return new CardDto
            {
                Host = host,
                CardProduct = cardProduct,
                TransactionMode = data[2],
                AccountNumber = data[3],
                ServiceCode = serviceCode,
                CardHolderName = data[5]
            };

        }

        protected override String[] WriteReadPOS(string frame, bool readOnly = false)
        {
            return WriteReadPOS(frame, false, false, false);
        }

        protected override String[] WriteReadPOS(string frame, bool readOnly = false,
        bool KeepListenerAlive = false, bool ListenerIsAlive = false)
        {

            if (DebugMode)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("frame: " + frame + "\n" +
                "KeepListenerAlive: " + KeepListenerAlive.ToString() + "\n" +
                "ListenerIsAlive: " + ListenerIsAlive.ToString() + "\n");
            }

            //if (!ListenerIsAlive)
            //{
                //Suscribe to observe the comunication with the POS and log information
                LogMonitor observer = new LogMonitor();
                observer.Subscribe(this);
            //}

            //Variables for read and  write data from POS
            byte[] SentAndReceivedBytes = new byte[1024];
            char controlCharacter = '\0';
            string recibedData = null;
            string clearFrame = Frames.ClearFrame(frame);
            int retransmissionTries = 0;
            List<string> closures = new List<string>();

            if (!readOnly)
            {

                if (!ListenerIsAlive)
                {
                    ValidateConnection();
                    ConnectToPOS(out SentAndReceivedBytes);
                }

                try
                {
                    if (!ListenerIsAlive)
                    {
                        // Program is suspended while waiting for an incoming connection.
                        listener.Bind(_currentSetting.IPLocalEndPointV4);
                        listener.Listen(5);
                        listener.ReceiveTimeout = _currentSetting.TimeOut;
                        listener.SendTimeout = _currentSetting.TimeOut;
                        socket = listener.Accept();

                    }

                    ecrToPos = Messages.Information.SentToPOS(_currentSetting.IPLocalEndPointV4.ToString(), socket.RemoteEndPoint.ToString());
                    posToEcr = Messages.Information.ReceivedFromPOS(_currentSetting.IPLocalEndPointV4.ToString(), socket.RemoteEndPoint.ToString());

                    while (retransmissionTries < Settings.SerialPort.MaximumNumberOfRetransmissions)
                    {

                        retransmissionTries++;

                        try
                        {

                            if (!ListenerIsAlive)
                            {
                                socket.LingerState = new LingerOption(true, 25);
                                socket.Receive(SentAndReceivedBytes);
                                recibedData += Encoding.ASCII.GetString(SentAndReceivedBytes);
                            } else
                            {
                                // Continue FollowUp Transaction Immediately
                                recibedData = ControlCharacters.ENQ.ToString();
                            }
                            
                            /*if (DebugMode)
                            {
                                Microsoft.VisualBasic.Interaction.MsgBox("While - Received Data: " + recibedData);
                            }*/

                            if (recibedData == ControlCharacters.ENQ.ToString())
                            {

                                LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(ControlCharacters.ENQ), recibedData);

                                SentAndReceivedBytes = (new ASCIIEncoding()).GetBytes(clearFrame);
                                socket.Send(SentAndReceivedBytes);
                                var port = ((IPEndPoint)socket.RemoteEndPoint).Port;
                                LogNewTransaction(ecrToPos, LogDto.Requests.Sent, Messages.Information.Frame(), clearFrame);

                                socket.Receive(SentAndReceivedBytes);
                                controlCharacter = Convert.ToChar(SentAndReceivedBytes[0]);

                                /*if (DebugMode)
                                {
                                    Microsoft.VisualBasic.Interaction.MsgBox("While - Control Character: " + controlCharacter.ToString());
                                }*/

                                // If transaction is valid
                                if (controlCharacter == ControlCharacters.ACK)
                                {

                                    /*if (DebugMode)
                                    {
                                        Microsoft.VisualBasic.Interaction.MsgBox("Control Character is ACK");
                                    }*/

                                    LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                                    do
                                    {

                                        if (socket.Connected)
                                        {

                                            SentAndReceivedBytes = new byte[1024];
                                            socket.Receive(SentAndReceivedBytes);

                                            controlCharacter = Convert.ToChar(SentAndReceivedBytes[0]);

                                            if (DebugMode)
                                            {
                                                Microsoft.VisualBasic.Interaction.MsgBox("While - Control Character: " + controlCharacter.ToString());
                                            }

                                            if (!controlCharacter.Equals(ControlCharacters.EOT))
                                            {

                                                recibedData = Encoding.ASCII.GetString(SentAndReceivedBytes).Replace("\0", "");

                                                if (frame.Equals(Frames.CardConsultation))
                                                {
                                                    var cardCleanedResponse = Frames.CleanedDataResponse(recibedData);

                                                    LogNewTransaction(posToEcr, LogDto.Requests.Received, Messages.Information.Frame(), recibedData);

                                                    SentAndReceivedBytes = Frames.FrameToBytes(ControlCharacters.ACK.ToString());
                                                    socket.Send(SentAndReceivedBytes);

                                                    //LogNewTransaction(posToEcr, LogDto.Requests.Received, Messages.Information.Frame(), recibedData);

                                                    if (KeepListenerAlive)
                                                    {

                                                        //Keep the connection Open for a Second Transaction
                                                        //Incoming in a few seconds....

                                                    } else
                                                    {
                                                        listener.Close();
                                                    }

                                                    return cardCleanedResponse.Split(ControlCharacters.FS);

                                                }

                                                if (frame.Contains(Frames.ClosingBatch.Substring(0, 5)))
                                                    closures.Add(recibedData);

                                                LogNewTransaction(posToEcr, LogDto.Requests.Received, Messages.Information.Frame(), recibedData);

                                            }
                                            else
                                            {

                                                /*if (DebugMode)
                                                {
                                                    Microsoft.VisualBasic.Interaction.MsgBox("Received EOT");
                                                }*/

                                                LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(controlCharacter), controlCharacter.ToString());
                                            }

                                            SentAndReceivedBytes = Frames.FrameToBytes(ControlCharacters.ACK.ToString());
                                            socket.Send(SentAndReceivedBytes);
                                            LogNewTransaction(ecrToPos, LogDto.Requests.Sent, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                                        }

                                    } while (!controlCharacter.Equals(ControlCharacters.EOT));

                                    if (recibedData.StartsWith(ControlCharacters.TransactionNotProgress) 
                                    || recibedData.StartsWith(ControlCharacters.CommunicationError))
                                    {

                                        if (DebugMode)
                                        {
                                            Microsoft.VisualBasic.Interaction.MsgBox("Received Data = TransactionNotProgress (" +
                                            recibedData.StartsWith(ControlCharacters.TransactionNotProgress).ToString() + ") OR CommunicationError (" +
                                            recibedData.StartsWith(ControlCharacters.CommunicationError).ToString() + ")");
                                        }

                                        if (recibedData.StartsWith(ControlCharacters.TransactionNotProgress))
                                            throw new Exception(Messages.Warnings.TransactionDidNotProgress);
                                        else
                                            throw new Exception(Messages.Warnings.NegativeAcknowledgement());

                                    }

                                }

                            }

                        }
                        catch (TimeoutException TE)
                        {

                            if (DebugMode)
                            {
                                Microsoft.VisualBasic.Interaction.MsgBox("Timeout Exception: " + TE.Message + "\n" +
                                TE.Source + "\n" + TE.HelpLink + "\n" + TE.HResult.ToString() + "\n\n" + TE.StackTrace);
                            }

                            throw new TimeoutException(Messages.Warnings.TimeoutPOS(_currentSetting.TimeOut));

                        }

                        //if (KeepListenerAlive)
                        //{

                            //Keep the connection Open for a Second Transaction
                            //Incoming in a few seconds....

                        //}
                        //else
                        //{
                            listener.Close();
                        //}

                        if (frame.Contains(Frames.ClosingBatch.Substring(0, 5)))
                            return closures.ToArray();

                        var cleanedResponse = Frames.CleanedDataResponse(recibedData);

                        return cleanedResponse.Split(ControlCharacters.FS);

                    }

                }
                catch (Exception e)
                {

                    listener.Close();

                    if (DebugMode)
                    {
                        Microsoft.VisualBasic.Interaction.MsgBox("General Exception: " + e.Message + "\n" +
                        e.Source + "\n" + e.HelpLink + "\n" + e.HResult.ToString() + "\n\n" + e.StackTrace);
                    }

                    Console.WriteLine(e.ToString());

                }

            }
            else
            {

                if (DebugMode)
                {
                    Microsoft.VisualBasic.Interaction.MsgBox("ReadOnly True");
                }

                SentAndReceivedBytes = (new ASCIIEncoding()).GetBytes(clearFrame);
                socket.Send(SentAndReceivedBytes);
                LogNewTransaction(ecrToPos, LogDto.Requests.Sent, Messages.Information.Frame(), clearFrame);

                SentAndReceivedBytes = new byte[1024];
                socket.Receive(SentAndReceivedBytes);
                controlCharacter = Convert.ToChar(SentAndReceivedBytes[0]);
                LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(controlCharacter), ControlCharacters.ACK.ToString());

                SentAndReceivedBytes = new byte[1024];
                socket.Receive(SentAndReceivedBytes);
                recibedData = Encoding.ASCII.GetString(SentAndReceivedBytes).Replace("\0", "");
                LogNewTransaction(posToEcr, LogDto.Requests.Received, Messages.Information.Frame(), recibedData);

                SentAndReceivedBytes = Frames.FrameToBytes(ControlCharacters.ACK.ToString());
                socket.Send(SentAndReceivedBytes);
                LogNewTransaction(ecrToPos, LogDto.Requests.Sent, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                SentAndReceivedBytes = new byte[1024];
                socket.Receive(SentAndReceivedBytes);
                controlCharacter = Convert.ToChar(SentAndReceivedBytes[0]);
                LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(controlCharacter), controlCharacter.ToString());

                SentAndReceivedBytes = Frames.FrameToBytes(ControlCharacters.ACK.ToString());
                socket.Send(SentAndReceivedBytes);
                LogNewTransaction(ecrToPos, LogDto.Requests.Sent, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                listener.Close();

                var cleanedResponse = Frames.CleanedDataResponse(recibedData);

                if (DebugMode)
                {
                    Microsoft.VisualBasic.Interaction.MsgBox("CleanedResponse " + cleanedResponse);
                }

                return cleanedResponse.Split(ControlCharacters.FS);

            }

            if (KeepListenerAlive)
            {

                //Keep the connection Open for a Second Transaction
                //Incoming in a few seconds....

            }
            else
            {
                listener.Close();
            }

            /*if (DebugMode)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Finished WriteReadPOS With a NACK.");
            }*/

            // Negative Acknowledgment
            throw new Exception(Messages.Warnings.NegativeAcknowledgement());

        }

        private void ConnectToPOS(out byte[] buffer)
        {
            using (var remoteEndPoint = new Socket(_currentSetting.IPLocalEndPointV4.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
            {
                remoteEndPoint.ReceiveTimeout = _currentSetting.TimeOut;
                remoteEndPoint.SendTimeout = _currentSetting.TimeOut;
                remoteEndPoint.Connect(_currentSetting.IPRemoteEndPointV4);

                /*if (DebugMode)
                {
                    Microsoft.VisualBasic.Interaction.MsgBox("Connected: " + remoteEndPoint.Connected.ToString());
                }*/

                buffer = Frames.FrameToBytes(ControlCharacters.SYN.ToString());
                remoteEndPoint.Send(buffer);

                /*if (DebugMode)
                {
                    Microsoft.VisualBasic.Interaction.MsgBox("Buffer Sent: " + buffer.ToString());
                }*/

                LogNewTransaction(Messages.Information.SentToPOS(_currentSetting.IPLocalEndPointV4.ToString(), _currentSetting.IPLocalEndPointV4.ToString()), LogDto.Requests.Sent, GetRequestType(ControlCharacters.SYN), ControlCharacters.SYN.ToString());
            }
        }

        private void ValidateConnection()
        {

            using (var remoteEndPoint = new Socket(_currentSetting.IPLocalEndPointV4.AddressFamily, SocketType.Stream, ProtocolType.Tcp))
            {
                remoteEndPoint.ReceiveTimeout = _currentSetting.TimeOut;
                remoteEndPoint.SendTimeout = _currentSetting.TimeOut;
                remoteEndPoint.Connect(_currentSetting.IPRemoteEndPointV4);

                /*if (DebugMode)
                {
                    Microsoft.VisualBasic.Interaction.MsgBox("Connected: " + remoteEndPoint.Connected.ToString());
                }*/

                var buffer = Frames.FrameToBytes(ControlCharacters.ENQ.ToString());
                remoteEndPoint.Send(buffer);

                /*if (DebugMode)
                {
                    Microsoft.VisualBasic.Interaction.MsgBox("Buffer Sent: " + buffer.ToString());
                }*/

                LogNewTransaction(Messages.Information.SentToPOS(_currentSetting.IPLocalEndPointV4.ToString(), _currentSetting.IPLocalEndPointV4.ToString()), LogDto.Requests.Sent, GetRequestType(ControlCharacters.ENQ), ControlCharacters.ENQ.ToString());

                remoteEndPoint.Receive(buffer);

                /*if (DebugMode)
                {
                    Microsoft.VisualBasic.Interaction.MsgBox("Buffer Received: " + buffer.ToString());
                }*/

                var recibedData = Encoding.ASCII.GetString(buffer);
                LogNewTransaction(Messages.Information.SentToPOS(_currentSetting.IPLocalEndPointV4.ToString(), _currentSetting.IPLocalEndPointV4.ToString()), LogDto.Requests.Received, GetRequestType(recibedData[0]), recibedData);

                /*if (DebugMode)
                {
                    Microsoft.VisualBasic.Interaction.MsgBox("Received Data: " + recibedData);
                }*/

            }

        }

        public string[] Test_WriteReadPOS_DoubleTransaction(
        Decimal TransactionTotalAmount, Decimal TransactionITBIS, Int32 InvoiceNumber)
        {

            bool readOnly = false;
            bool ListenerIsAlive = false;
            bool KeepListenerAlive = true;

            String frame1 = Frames.CardConsultation;
            
            //Suscribe to observe the comunication with the POS and log information
            LogMonitor observer = new LogMonitor();
            observer.Subscribe(this);

            //Variables for read and  write data from POS
            byte[] SentAndReceivedBytes = new byte[1024];
            char controlCharacter = '\0';
            string recibedData = null;
            string clearFrame = Frames.ClearFrame(frame1);
            int retransmissionTries = 0;
            List<string> closures = new List<string>();

            if (!readOnly)
            {

                if (!ListenerIsAlive)
                {
                    ValidateConnection();
                    ConnectToPOS(out SentAndReceivedBytes);
                }

                try
                {
                    if (!ListenerIsAlive)
                    {
                        // Program is suspended while waiting for an incoming connection.
                        listener.Bind(_currentSetting.IPLocalEndPointV4);
                        listener.Listen(5);
                        listener.ReceiveTimeout = _currentSetting.TimeOut;
                        listener.SendTimeout = _currentSetting.TimeOut;
                        socket = listener.Accept();

                    }

                    ecrToPos = Messages.Information.SentToPOS(_currentSetting.IPLocalEndPointV4.ToString(), socket.RemoteEndPoint.ToString());
                    posToEcr = Messages.Information.ReceivedFromPOS(_currentSetting.IPLocalEndPointV4.ToString(), socket.RemoteEndPoint.ToString());

                    while (retransmissionTries < Settings.SerialPort.MaximumNumberOfRetransmissions)
                    {

                        retransmissionTries++;

                        try
                        {

                            socket.LingerState = new LingerOption(true, 25);
                            socket.Receive(SentAndReceivedBytes);
                            recibedData += Encoding.ASCII.GetString(SentAndReceivedBytes);

                            /*if (DebugMode)
                            {
                                Microsoft.VisualBasic.Interaction.MsgBox("While - Received Data: " + recibedData);
                            }*/

                            if (recibedData == ControlCharacters.ENQ.ToString())
                            {

                                LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(ControlCharacters.ENQ), recibedData);

                                SentAndReceivedBytes = (new ASCIIEncoding()).GetBytes(clearFrame);
                                socket.Send(SentAndReceivedBytes);
                                var port = ((IPEndPoint)socket.RemoteEndPoint).Port;
                                LogNewTransaction(ecrToPos, LogDto.Requests.Sent, Messages.Information.Frame(), clearFrame);

                                socket.Receive(SentAndReceivedBytes);
                                controlCharacter = Convert.ToChar(SentAndReceivedBytes[0]);

                                /*if (DebugMode)
                                {
                                    Microsoft.VisualBasic.Interaction.MsgBox("While - Control Character: " + controlCharacter.ToString());
                                }*/

                                // If transaction is valid
                                if (controlCharacter == ControlCharacters.ACK)
                                {

                                    /*if (DebugMode)
                                    {
                                        Microsoft.VisualBasic.Interaction.MsgBox("Control Character is ACK");
                                    }*/

                                    LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                                    do
                                    {

                                        if (socket.Connected)
                                        {

                                            SentAndReceivedBytes = new byte[1024];
                                            socket.Receive(SentAndReceivedBytes);

                                            controlCharacter = Convert.ToChar(SentAndReceivedBytes[0]);

                                            if (DebugMode)
                                            {
                                                Microsoft.VisualBasic.Interaction.MsgBox("While - Control Character: " + controlCharacter.ToString());
                                            }

                                            if (!controlCharacter.Equals(ControlCharacters.EOT))
                                            {

                                                recibedData = Encoding.ASCII.GetString(SentAndReceivedBytes).Replace("\0", "");

                                                if (frame1.Equals(Frames.CardConsultation))
                                                {
                                                    var cardCleanedResponse = Frames.CleanedDataResponse(recibedData);

                                                    LogNewTransaction(posToEcr, LogDto.Requests.Received, Messages.Information.Frame(), recibedData);

                                                    SentAndReceivedBytes = Frames.FrameToBytes(ControlCharacters.ACK.ToString());
                                                    socket.Send(SentAndReceivedBytes);

                                                    //LogNewTransaction(posToEcr, LogDto.Requests.Received, Messages.Information.Frame(), recibedData);

                                                    if (KeepListenerAlive)
                                                    {

                                                        string[] tmpCardInfo = cardCleanedResponse.Split(ControlCharacters.FS);

                                                        // Read Card Info

                                                        if (tmpCardInfo.Length != 6)
                                                        {
                                                            throw new FormatException();
                                                        }

                                                        if (!Enum.TryParse(tmpCardInfo[0], ignoreCase: true, out CardDto.Hosts result))
                                                        {
                                                            throw new FormatException();
                                                        }

                                                        if (!Enum.TryParse(tmpCardInfo[1], ignoreCase: true, out CardDto.CardProducts result2))
                                                        {
                                                            throw new FormatException();
                                                        }

                                                        if (!short.TryParse(tmpCardInfo[4], out short result3))
                                                        {
                                                            throw new FormatException();
                                                        }

                                                        CardDto tmpCardDto = new CardDto
                                                        {
                                                            Host = result,
                                                            CardProduct = result2,
                                                            TransactionMode = tmpCardInfo[2],
                                                            AccountNumber = tmpCardInfo[3],
                                                            ServiceCode = result3,
                                                            CardHolderName = tmpCardInfo[5]
                                                        };

                                                        // At this point we have CardBin (AccountNumber.Left(6))

                                                        /*
                                                         * 
                                                        TransactionDto testSale = new TransactionDto()
                                                        {
                                                            MultiMerchantID = 0,
                                                            MultiAcquirerID = 0,
                                                            Folio = 1,
                                                            CheckInDate = DateTime.Now,
                                                            CheckOutDate = DateTime.Now,
                                                            TransactionAmount = 1M,
                                                            TransactionITBIS = 0.18M,
                                                            TransactionOtherTaxes = 0,
                                                            InvoiceNumber = 150
                                                        };

                                                        String frame2 = Frames.BuildFrameToAutorizeTransaction(TransactionDto.TransactionTypes.NormalSale, testSale);

                                                        // Try to process second request (frame2) with Listener / Socket Open...

                                                        //Variables for read and  write data from POS
                                                        SentAndReceivedBytes = new byte[1024];
                                                        controlCharacter = '\0';
                                                        recibedData = null;
                                                        clearFrame = Frames.ClearFrame(frame2);
                                                        retransmissionTries = 0;
                                                        closures = new List<string>();

                                                        while (retransmissionTries < Settings.SerialPort.MaximumNumberOfRetransmissions)
                                                        {

                                                            retransmissionTries++;

                                                            try
                                                            {

                                                                socket.LingerState = new LingerOption(true, 10);
                                                                socket.Receive(SentAndReceivedBytes);
                                                                recibedData += Encoding.ASCII.GetString(SentAndReceivedBytes);

                                                                if (recibedData == ControlCharacters.ENQ.ToString())
                                                                {

                                                                    LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(ControlCharacters.ENQ), recibedData);

                                                                    SentAndReceivedBytes = (new ASCIIEncoding()).GetBytes(clearFrame);
                                                                    socket.Send(SentAndReceivedBytes);
                                                                    port = ((IPEndPoint)socket.RemoteEndPoint).Port;
                                                                    LogNewTransaction(ecrToPos, LogDto.Requests.Sent, Messages.Information.Frame(), clearFrame);

                                                                    socket.Receive(SentAndReceivedBytes);
                                                                    controlCharacter = Convert.ToChar(SentAndReceivedBytes[0]);

                                                                    // If transaction is valid
                                                                    if (controlCharacter == ControlCharacters.ACK)
                                                                    {

                                                                        
                                                                        LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                                                                        do
                                                                        {

                                                                            if (socket.Connected)
                                                                            {

                                                                                SentAndReceivedBytes = new byte[1024];
                                                                                socket.Receive(SentAndReceivedBytes);

                                                                                controlCharacter = Convert.ToChar(SentAndReceivedBytes[0]);

                                                                                if (DebugMode)
                                                                                {
                                                                                    Microsoft.VisualBasic.Interaction.MsgBox("While - Control Character: " + controlCharacter.ToString());
                                                                                }

                                                                                if (!controlCharacter.Equals(ControlCharacters.EOT))
                                                                                {

                                                                                    recibedData = Encoding.ASCII.GetString(SentAndReceivedBytes).Replace("\0", "");

                                                                                    if (frame1.Contains(Frames.ClosingBatch.Substring(0, 5)))
                                                                                        closures.Add(recibedData);

                                                                                    LogNewTransaction(posToEcr, LogDto.Requests.Received, Messages.Information.Frame(), recibedData);

                                                                                    LogNewTransaction(posToEcr, LogDto.Requests.Received, Messages.Information.Frame(), recibedData);

                                                                                }
                                                                                else
                                                                                {

                                                                                    LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(controlCharacter), controlCharacter.ToString());
                                                                                }

                                                                                SentAndReceivedBytes = Frames.FrameToBytes(ControlCharacters.ACK.ToString());
                                                                                socket.Send(SentAndReceivedBytes);
                                                                                LogNewTransaction(ecrToPos, LogDto.Requests.Sent, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                                                                            }

                                                                        } while (!controlCharacter.Equals(ControlCharacters.EOT));

                                                                        if (recibedData.StartsWith(ControlCharacters.TransactionNotProgress) || recibedData.StartsWith(ControlCharacters.CommunicationError))
                                                                        {

                                                                            if (DebugMode)
                                                                            {
                                                                                Microsoft.VisualBasic.Interaction.MsgBox("Received Data = TransactionNotProgress (" +
                                                                                recibedData.StartsWith(ControlCharacters.TransactionNotProgress).ToString() + ") OR CommunicationError (" +
                                                                                recibedData.StartsWith(ControlCharacters.CommunicationError).ToString() + ")");
                                                                            }

                                                                            if (recibedData.StartsWith(ControlCharacters.TransactionNotProgress))
                                                                                throw new Exception(Messages.Warnings.TransactionDidNotProgress);
                                                                            else
                                                                                throw new Exception(Messages.Warnings.NegativeAcknowledgement());

                                                                        }

                                                                    }

                                                                }

                                                            }
                                                            catch (TimeoutException TE2)
                                                            {

                                                                if (DebugMode)
                                                                {
                                                                    Microsoft.VisualBasic.Interaction.MsgBox("Timeout Exception: " + TE2.Message + "\n" +
                                                                    TE2.Source + "\n" + TE2.HelpLink + "\n" + TE2.HResult.ToString() + "\n\n" + TE2.StackTrace);
                                                                }

                                                                throw new TimeoutException(Messages.Warnings.TimeoutPOS(_currentSetting.TimeOut));

                                                            }

                                                            listener.Close();

                                                            var cleanedResponse2 = Frames.CleanedDataResponse(recibedData);

                                                            string[] tmpSaleResp = cleanedResponse2.Split(ControlCharacters.FS);

                                                            Microsoft.VisualBasic.Interaction.MsgBox(tmpSaleResp.Length);

                                                            String RespStr = String.Empty;

                                                            for (int i = 0; i < tmpSaleResp.Length; i++)
                                                            {
                                                                if (tmpSaleResp[i] != null)
                                                                    RespStr += tmpSaleResp[i] + "\n";
                                                                else
                                                                    RespStr += "null" + "\n";
                                                            }

                                                            Microsoft.VisualBasic.Interaction.MsgBox(RespStr);

                                                            return tmpSaleResp;

                                                        }
                                                        */

                                                        // Simulate some delay while recalculating promotion ....

                                                        //System.Threading.Thread.Sleep(5000);

                                                        return Test_WriteReadPOS_Step2(TransactionTotalAmount, TransactionITBIS, InvoiceNumber,
                                                        ref SentAndReceivedBytes, ref controlCharacter, ref recibedData, ref clearFrame,
                                                        ref retransmissionTries, ref port);

                                                        // End second request and leave.
                                                        
                                                    }
                                                    else
                                                    {
                                                        listener.Close();
                                                    }

                                                    return cardCleanedResponse.Split(ControlCharacters.FS);

                                                }

                                                if (frame1.Contains(Frames.ClosingBatch.Substring(0, 5)))
                                                    closures.Add(recibedData);

                                                LogNewTransaction(posToEcr, LogDto.Requests.Received, Messages.Information.Frame(), recibedData);

                                            }
                                            else
                                            {

                                                /*if (DebugMode)
                                                {
                                                    Microsoft.VisualBasic.Interaction.MsgBox("Received EOT");
                                                }*/

                                                LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(controlCharacter), controlCharacter.ToString());
                                            }

                                            SentAndReceivedBytes = Frames.FrameToBytes(ControlCharacters.ACK.ToString());
                                            socket.Send(SentAndReceivedBytes);
                                            LogNewTransaction(ecrToPos, LogDto.Requests.Sent, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                                        }

                                    } while (!controlCharacter.Equals(ControlCharacters.EOT));

                                    if (recibedData.StartsWith(ControlCharacters.TransactionNotProgress) || recibedData.StartsWith(ControlCharacters.CommunicationError))
                                    {

                                        if (DebugMode)
                                        {
                                            Microsoft.VisualBasic.Interaction.MsgBox("Received Data = TransactionNotProgress (" +
                                            recibedData.StartsWith(ControlCharacters.TransactionNotProgress).ToString() + ") OR CommunicationError (" +
                                            recibedData.StartsWith(ControlCharacters.CommunicationError).ToString() + ")");
                                        }

                                        if (recibedData.StartsWith(ControlCharacters.TransactionNotProgress))
                                            throw new Exception(Messages.Warnings.TransactionDidNotProgress);
                                        else
                                            throw new Exception(Messages.Warnings.NegativeAcknowledgement());

                                    }

                                }

                            }

                        }
                        catch (TimeoutException TE)
                        {

                            if (DebugMode)
                            {
                                Microsoft.VisualBasic.Interaction.MsgBox("Timeout Exception: " + TE.Message + "\n" +
                                TE.Source + "\n" + TE.HelpLink + "\n" + TE.HResult.ToString() + "\n\n" + TE.StackTrace);
                            }

                            throw new TimeoutException(Messages.Warnings.TimeoutPOS(_currentSetting.TimeOut));

                        }

                        listener.Close();

                        if (frame1.Contains(Frames.ClosingBatch.Substring(0, 5)))
                            return closures.ToArray();

                        var cleanedResponse = Frames.CleanedDataResponse(recibedData);

                        return cleanedResponse.Split(ControlCharacters.FS);

                    }

                }
                catch (Exception e)
                {

                    listener.Close();

                    if (DebugMode)
                    {
                        Microsoft.VisualBasic.Interaction.MsgBox("General Exception: " + e.Message + "\n" +
                        e.Source + "\n" + e.HelpLink + "\n" + e.HResult.ToString() + "\n\n" + e.StackTrace);
                    }

                    Console.WriteLine(e.ToString());

                }

            }
            else
            {

                if (DebugMode)
                {
                    Microsoft.VisualBasic.Interaction.MsgBox("ReadOnly True");
                }

                SentAndReceivedBytes = (new ASCIIEncoding()).GetBytes(clearFrame);
                socket.Send(SentAndReceivedBytes);
                LogNewTransaction(ecrToPos, LogDto.Requests.Sent, Messages.Information.Frame(), clearFrame);

                SentAndReceivedBytes = new byte[1024];
                socket.Receive(SentAndReceivedBytes);
                controlCharacter = Convert.ToChar(SentAndReceivedBytes[0]);
                LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(controlCharacter), ControlCharacters.ACK.ToString());

                SentAndReceivedBytes = new byte[1024];
                socket.Receive(SentAndReceivedBytes);
                recibedData = Encoding.ASCII.GetString(SentAndReceivedBytes).Replace("\0", "");
                LogNewTransaction(posToEcr, LogDto.Requests.Received, Messages.Information.Frame(), recibedData);

                SentAndReceivedBytes = Frames.FrameToBytes(ControlCharacters.ACK.ToString());
                socket.Send(SentAndReceivedBytes);
                LogNewTransaction(ecrToPos, LogDto.Requests.Sent, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                SentAndReceivedBytes = new byte[1024];
                socket.Receive(SentAndReceivedBytes);
                controlCharacter = Convert.ToChar(SentAndReceivedBytes[0]);
                LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(controlCharacter), controlCharacter.ToString());

                SentAndReceivedBytes = Frames.FrameToBytes(ControlCharacters.ACK.ToString());
                socket.Send(SentAndReceivedBytes);
                LogNewTransaction(ecrToPos, LogDto.Requests.Sent, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                listener.Close();

                var cleanedResponse = Frames.CleanedDataResponse(recibedData);

                if (DebugMode)
                {
                    Microsoft.VisualBasic.Interaction.MsgBox("CleanedResponse " + cleanedResponse);
                }

                return cleanedResponse.Split(ControlCharacters.FS);

            }

            listener.Close();

            /*if (DebugMode)
            {
                Microsoft.VisualBasic.Interaction.MsgBox("Finished WriteReadPOS With a NACK.");
            }*/

            // Negative Acknowledgment
            throw new Exception(Messages.Warnings.NegativeAcknowledgement());

        }

        private string[] Test_WriteReadPOS_Step2(
        Decimal TransactionTotalAmount, Decimal TransactionITBIS, int InvoiceNumber, 
        ref byte[] SentAndReceivedBytes, ref char controlCharacter, 
        ref String recibedData, ref String clearFrame, ref int retransmissionTries, 
        ref int port)
        {

            TransactionDto testSale = new TransactionDto()
            {
                MultiMerchantID = 0,
                MultiAcquirerID = 0,
                Folio = 1,
                CheckInDate = DateTime.Now,
                CheckOutDate = DateTime.Now,
                TransactionAmount = TransactionTotalAmount,
                TransactionITBIS = TransactionITBIS,
                TransactionOtherTaxes = 0,
                InvoiceNumber = InvoiceNumber
            };

            String frame2 = Frames.BuildFrameToAutorizeTransaction(TransactionDto.TransactionTypes.NormalSale, testSale);

            // Try to process second request (frame2) with Listener / Socket Open...

            //Variables for read and  write data from POS
            SentAndReceivedBytes = new byte[1024];
            controlCharacter = '\0';
            recibedData = null;
            clearFrame = Frames.ClearFrame(frame2);
            retransmissionTries = 0;

            while (retransmissionTries < Settings.SerialPort.MaximumNumberOfRetransmissions)
            {

                retransmissionTries++;

                try
                {

                    socket.LingerState = new LingerOption(true, 10);
                    socket.Receive(SentAndReceivedBytes);
                    recibedData += Encoding.ASCII.GetString(SentAndReceivedBytes);

                    /*if (DebugMode)
                    {
                        Microsoft.VisualBasic.Interaction.MsgBox("While - Received Data: " + recibedData);
                    }*/

                    if (recibedData == ControlCharacters.ENQ.ToString())
                    {

                        LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(ControlCharacters.ENQ), recibedData);

                        SentAndReceivedBytes = (new ASCIIEncoding()).GetBytes(clearFrame);
                        socket.Send(SentAndReceivedBytes);
                        port = ((IPEndPoint)socket.RemoteEndPoint).Port;
                        LogNewTransaction(ecrToPos, LogDto.Requests.Sent, Messages.Information.Frame(), clearFrame);

                        socket.Receive(SentAndReceivedBytes);
                        controlCharacter = Convert.ToChar(SentAndReceivedBytes[0]);

                        /*if (DebugMode)
                        {
                            Microsoft.VisualBasic.Interaction.MsgBox("While - Control Character: " + controlCharacter.ToString());
                        }*/

                        // If transaction is valid
                        if (controlCharacter == ControlCharacters.ACK)
                        {

                            /*if (DebugMode)
                            {
                                Microsoft.VisualBasic.Interaction.MsgBox("Control Character is ACK");
                            }*/

                            LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                            do
                            {

                                if (socket.Connected)
                                {

                                    SentAndReceivedBytes = new byte[1024];
                                    socket.Receive(SentAndReceivedBytes);

                                    controlCharacter = Convert.ToChar(SentAndReceivedBytes[0]);

                                    if (DebugMode)
                                    {
                                        Microsoft.VisualBasic.Interaction.MsgBox("While - Control Character: " + controlCharacter.ToString());
                                    }

                                    if (!controlCharacter.Equals(ControlCharacters.EOT))
                                    {

                                        recibedData = Encoding.ASCII.GetString(SentAndReceivedBytes).Replace("\0", "");

                                        LogNewTransaction(posToEcr, LogDto.Requests.Received, Messages.Information.Frame(), recibedData);

                                    }
                                    else
                                    {

                                        /*if (DebugMode)
                                        {
                                            Microsoft.VisualBasic.Interaction.MsgBox("Received EOT");
                                        }*/

                                        LogNewTransaction(posToEcr, LogDto.Requests.Received, GetRequestType(controlCharacter), controlCharacter.ToString());
                                    }

                                    SentAndReceivedBytes = Frames.FrameToBytes(ControlCharacters.ACK.ToString());
                                    socket.Send(SentAndReceivedBytes);
                                    LogNewTransaction(ecrToPos, LogDto.Requests.Sent, GetRequestType(ControlCharacters.ACK), ControlCharacters.ACK.ToString());

                                }

                            } while (!controlCharacter.Equals(ControlCharacters.EOT));

                            if (recibedData.StartsWith(ControlCharacters.TransactionNotProgress) || recibedData.StartsWith(ControlCharacters.CommunicationError))
                            {

                                if (DebugMode)
                                {
                                    Microsoft.VisualBasic.Interaction.MsgBox("Received Data = TransactionNotProgress (" +
                                    recibedData.StartsWith(ControlCharacters.TransactionNotProgress).ToString() + ") OR CommunicationError (" +
                                    recibedData.StartsWith(ControlCharacters.CommunicationError).ToString() + ")");
                                }

                                if (recibedData.StartsWith(ControlCharacters.TransactionNotProgress))
                                    throw new Exception(Messages.Warnings.TransactionDidNotProgress);
                                else
                                    throw new Exception(Messages.Warnings.NegativeAcknowledgement());

                            }

                        }

                    }

                }
                catch (TimeoutException TE2)
                {

                    if (DebugMode)
                    {
                        Microsoft.VisualBasic.Interaction.MsgBox("Timeout Exception: " + TE2.Message + "\n" +
                        TE2.Source + "\n" + TE2.HelpLink + "\n" + TE2.HResult.ToString() + "\n\n" + TE2.StackTrace);
                    }

                    throw new TimeoutException(Messages.Warnings.TimeoutPOS(_currentSetting.TimeOut));

                }

                listener.Close();

                var cleanedResponse2 = Frames.CleanedDataResponse(recibedData);

                string[] tmpSaleResp = cleanedResponse2.Split(ControlCharacters.FS);

                Microsoft.VisualBasic.Interaction.MsgBox(tmpSaleResp.Length);

                String RespStr = String.Empty;

                for (int i = 0; i < tmpSaleResp.Length; i++)
                {
                    if (tmpSaleResp[i] != null)
                        RespStr += tmpSaleResp[i] + "\n";
                    else
                        RespStr += "null" + "\n";
                }

                Microsoft.VisualBasic.Interaction.MsgBox(RespStr);

                return tmpSaleResp;

            }

            return null;

        }

    }

}