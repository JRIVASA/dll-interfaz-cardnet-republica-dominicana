﻿using ECRti.Dtos;
using System.Collections.Generic;

namespace ECRti
{
    internal interface ICommunication
    {
        CardDto RequestCardInformation();
        CardDto RequestCardInformation_KeepListenerAlive();

        TransactionDto AuthorizeTransaction(TransactionDto.TransactionTypes transactionType, TransactionDto transactionDto);
        TransactionDto AuthorizeTransaction(TransactionDto.TransactionTypes transactionType, 
        TransactionDto transactionDto, bool ListenerIsAlive = false);

        string CancelTransaction(TransactionDto.TransactionTypes transactionType, TransactionDto transactionDto);

        List<ClosureDto> ExecuteClosure(bool isMultiMerchantEnabled, byte? multiMerchantID = null);

        string AdjustTipAmount(TransactionDto transactionDto);

        void AuthorizeService(byte MultiAcquirerID);
    }
}