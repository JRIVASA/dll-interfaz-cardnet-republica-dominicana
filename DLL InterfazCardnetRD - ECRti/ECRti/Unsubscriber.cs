﻿using System;
using System.Collections.Generic;

namespace ECRti
{
    internal class Unsubscriber<Log> : IDisposable
    {
        private List<IObserver<Log>> _observers;
        private IObserver<Log> _observer;

        internal Unsubscriber(List<IObserver<Log>> observers, IObserver<Log> observer)
        {
            this._observers = observers;
            this._observer = observer;
        }

        public void Dispose()
        {
            if (_observers.Contains(_observer))
                _observers.Remove(_observer);
        }
    }
}