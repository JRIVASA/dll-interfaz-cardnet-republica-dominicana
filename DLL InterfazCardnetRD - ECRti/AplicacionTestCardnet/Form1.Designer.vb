﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> _
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()> _
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()> _
    Private Sub InitializeComponent()
        Me.Button1 = New System.Windows.Forms.Button()
        Me.btn_Anulacion = New System.Windows.Forms.Button()
        Me.Button2 = New System.Windows.Forms.Button()
        Me.txt_Monto = New System.Windows.Forms.TextBox()
        Me.txt_Cuotas = New System.Windows.Forms.TextBox()
        Me.txt_MontoImp = New System.Windows.Forms.TextBox()
        Me.txt_IPPosTerminal = New System.Windows.Forms.TextBox()
        Me.txt_PortCajaPC = New System.Windows.Forms.TextBox()
        Me.txt_IPCajaPC = New System.Windows.Forms.TextBox()
        Me.txt_MultiAcquirerID = New System.Windows.Forms.TextBox()
        Me.txt_MultiMerchantID = New System.Windows.Forms.TextBox()
        Me.txt_PortPosTerminal = New System.Windows.Forms.TextBox()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.Label2 = New System.Windows.Forms.Label()
        Me.Label3 = New System.Windows.Forms.Label()
        Me.Label4 = New System.Windows.Forms.Label()
        Me.Label5 = New System.Windows.Forms.Label()
        Me.Label6 = New System.Windows.Forms.Label()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.txt_OperatorID = New System.Windows.Forms.TextBox()
        Me.Label12 = New System.Windows.Forms.Label()
        Me.btn_Guardar = New System.Windows.Forms.Button()
        Me.btn_Limpiar = New System.Windows.Forms.Button()
        Me.Label7 = New System.Windows.Forms.Label()
        Me.Label8 = New System.Windows.Forms.Label()
        Me.Label9 = New System.Windows.Forms.Label()
        Me.btn_Cierre = New System.Windows.Forms.Button()
        Me.cmb_TipoTrans = New System.Windows.Forms.ComboBox()
        Me.Label10 = New System.Windows.Forms.Label()
        Me.Label11 = New System.Windows.Forms.Label()
        Me.txt_NumAut = New System.Windows.Forms.TextBox()
        Me.BtnPreCie = New System.Windows.Forms.Button()
        Me.OptTrue1 = New System.Windows.Forms.RadioButton()
        Me.OptFalse1 = New System.Windows.Forms.RadioButton()
        Me.lblDebugMode = New System.Windows.Forms.Label()
        Me.lblTimeout = New System.Windows.Forms.Label()
        Me.txtTimeout = New System.Windows.Forms.TextBox()
        Me.btnTest = New System.Windows.Forms.Button()
        Me.Button3 = New System.Windows.Forms.Button()
        Me.Label13 = New System.Windows.Forms.Label()
        Me.txtRef = New System.Windows.Forms.TextBox()
        Me.txtCardHost = New System.Windows.Forms.TextBox()
        Me.Label14 = New System.Windows.Forms.Label()
        Me.GroupBox1.SuspendLayout()
        Me.SuspendLayout()
        '
        'Button1
        '
        Me.Button1.Location = New System.Drawing.Point(209, 24)
        Me.Button1.Margin = New System.Windows.Forms.Padding(2)
        Me.Button1.Name = "Button1"
        Me.Button1.Size = New System.Drawing.Size(86, 20)
        Me.Button1.TabIndex = 0
        Me.Button1.Text = "Venta"
        Me.Button1.UseVisualStyleBackColor = True
        '
        'btn_Anulacion
        '
        Me.btn_Anulacion.Location = New System.Drawing.Point(209, 49)
        Me.btn_Anulacion.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_Anulacion.Name = "btn_Anulacion"
        Me.btn_Anulacion.Size = New System.Drawing.Size(86, 19)
        Me.btn_Anulacion.TabIndex = 1
        Me.btn_Anulacion.Text = "Anulacion"
        Me.btn_Anulacion.UseVisualStyleBackColor = True
        '
        'Button2
        '
        Me.Button2.Location = New System.Drawing.Point(209, 206)
        Me.Button2.Margin = New System.Windows.Forms.Padding(2)
        Me.Button2.Name = "Button2"
        Me.Button2.Size = New System.Drawing.Size(86, 41)
        Me.Button2.TabIndex = 2
        Me.Button2.Text = "Buscar Ultima Transaccion"
        Me.Button2.UseVisualStyleBackColor = True
        Me.Button2.Visible = False
        '
        'txt_Monto
        '
        Me.txt_Monto.Location = New System.Drawing.Point(113, 26)
        Me.txt_Monto.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Monto.Name = "txt_Monto"
        Me.txt_Monto.Size = New System.Drawing.Size(76, 20)
        Me.txt_Monto.TabIndex = 3
        Me.txt_Monto.Text = "1.00"
        '
        'txt_Cuotas
        '
        Me.txt_Cuotas.Location = New System.Drawing.Point(113, 49)
        Me.txt_Cuotas.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_Cuotas.Name = "txt_Cuotas"
        Me.txt_Cuotas.Size = New System.Drawing.Size(76, 20)
        Me.txt_Cuotas.TabIndex = 4
        Me.txt_Cuotas.Text = "0"
        Me.txt_Cuotas.Visible = False
        '
        'txt_MontoImp
        '
        Me.txt_MontoImp.Location = New System.Drawing.Point(113, 72)
        Me.txt_MontoImp.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_MontoImp.Name = "txt_MontoImp"
        Me.txt_MontoImp.Size = New System.Drawing.Size(76, 20)
        Me.txt_MontoImp.TabIndex = 5
        Me.txt_MontoImp.Text = "0.18"
        '
        'txt_IPPosTerminal
        '
        Me.txt_IPPosTerminal.Location = New System.Drawing.Point(146, 72)
        Me.txt_IPPosTerminal.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_IPPosTerminal.Name = "txt_IPPosTerminal"
        Me.txt_IPPosTerminal.Size = New System.Drawing.Size(198, 20)
        Me.txt_IPPosTerminal.TabIndex = 8
        Me.txt_IPPosTerminal.Text = "10.12.3.80"
        '
        'txt_PortCajaPC
        '
        Me.txt_PortCajaPC.Location = New System.Drawing.Point(146, 49)
        Me.txt_PortCajaPC.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_PortCajaPC.Name = "txt_PortCajaPC"
        Me.txt_PortCajaPC.Size = New System.Drawing.Size(198, 20)
        Me.txt_PortCajaPC.TabIndex = 7
        Me.txt_PortCajaPC.Text = "2018"
        '
        'txt_IPCajaPC
        '
        Me.txt_IPCajaPC.Location = New System.Drawing.Point(146, 26)
        Me.txt_IPCajaPC.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_IPCajaPC.Name = "txt_IPCajaPC"
        Me.txt_IPCajaPC.Size = New System.Drawing.Size(198, 20)
        Me.txt_IPCajaPC.TabIndex = 6
        Me.txt_IPCajaPC.Text = "10.12.2.46"
        '
        'txt_MultiAcquirerID
        '
        Me.txt_MultiAcquirerID.Location = New System.Drawing.Point(146, 140)
        Me.txt_MultiAcquirerID.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_MultiAcquirerID.Name = "txt_MultiAcquirerID"
        Me.txt_MultiAcquirerID.Size = New System.Drawing.Size(198, 20)
        Me.txt_MultiAcquirerID.TabIndex = 11
        Me.txt_MultiAcquirerID.Text = "0"
        '
        'txt_MultiMerchantID
        '
        Me.txt_MultiMerchantID.Location = New System.Drawing.Point(146, 117)
        Me.txt_MultiMerchantID.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_MultiMerchantID.Name = "txt_MultiMerchantID"
        Me.txt_MultiMerchantID.Size = New System.Drawing.Size(198, 20)
        Me.txt_MultiMerchantID.TabIndex = 10
        Me.txt_MultiMerchantID.Text = "0"
        '
        'txt_PortPosTerminal
        '
        Me.txt_PortPosTerminal.Location = New System.Drawing.Point(146, 94)
        Me.txt_PortPosTerminal.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_PortPosTerminal.Name = "txt_PortPosTerminal"
        Me.txt_PortPosTerminal.Size = New System.Drawing.Size(199, 20)
        Me.txt_PortPosTerminal.TabIndex = 9
        Me.txt_PortPosTerminal.Text = "7060"
        '
        'Label1
        '
        Me.Label1.AutoSize = True
        Me.Label1.Location = New System.Drawing.Point(23, 28)
        Me.Label1.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label1.Name = "Label1"
        Me.Label1.Size = New System.Drawing.Size(72, 13)
        Me.Label1.TabIndex = 12
        Me.Label1.Text = "IP CAJA - PC:"
        '
        'Label2
        '
        Me.Label2.AutoSize = True
        Me.Label2.Location = New System.Drawing.Point(23, 51)
        Me.Label2.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label2.Name = "Label2"
        Me.Label2.Size = New System.Drawing.Size(81, 13)
        Me.Label2.TabIndex = 13
        Me.Label2.Text = "Port CAJA - PC:"
        '
        'Label3
        '
        Me.Label3.AutoSize = True
        Me.Label3.Location = New System.Drawing.Point(23, 74)
        Me.Label3.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label3.Name = "Label3"
        Me.Label3.Size = New System.Drawing.Size(103, 13)
        Me.Label3.TabIndex = 14
        Me.Label3.Text = "IP POS TERMINAL:"
        '
        'Label4
        '
        Me.Label4.AutoSize = True
        Me.Label4.Location = New System.Drawing.Point(23, 97)
        Me.Label4.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label4.Name = "Label4"
        Me.Label4.Size = New System.Drawing.Size(123, 13)
        Me.Label4.TabIndex = 15
        Me.Label4.Text = "PORT POS TERMINAL:"
        '
        'Label5
        '
        Me.Label5.AutoSize = True
        Me.Label5.Location = New System.Drawing.Point(23, 142)
        Me.Label5.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label5.Name = "Label5"
        Me.Label5.Size = New System.Drawing.Size(116, 13)
        Me.Label5.TabIndex = 16
        Me.Label5.Text = "MULTI ACQUIRER ID:"
        '
        'Label6
        '
        Me.Label6.AutoSize = True
        Me.Label6.Location = New System.Drawing.Point(23, 119)
        Me.Label6.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label6.Name = "Label6"
        Me.Label6.Size = New System.Drawing.Size(121, 13)
        Me.Label6.TabIndex = 17
        Me.Label6.Text = "MULTI MERCHANT ID:"
        '
        'GroupBox1
        '
        Me.GroupBox1.Controls.Add(Me.txt_OperatorID)
        Me.GroupBox1.Controls.Add(Me.Label12)
        Me.GroupBox1.Controls.Add(Me.btn_Guardar)
        Me.GroupBox1.Controls.Add(Me.btn_Limpiar)
        Me.GroupBox1.Controls.Add(Me.txt_IPCajaPC)
        Me.GroupBox1.Controls.Add(Me.Label6)
        Me.GroupBox1.Controls.Add(Me.txt_PortCajaPC)
        Me.GroupBox1.Controls.Add(Me.Label5)
        Me.GroupBox1.Controls.Add(Me.Label1)
        Me.GroupBox1.Controls.Add(Me.txt_MultiAcquirerID)
        Me.GroupBox1.Controls.Add(Me.Label4)
        Me.GroupBox1.Controls.Add(Me.txt_MultiMerchantID)
        Me.GroupBox1.Controls.Add(Me.Label2)
        Me.GroupBox1.Controls.Add(Me.Label3)
        Me.GroupBox1.Controls.Add(Me.txt_IPPosTerminal)
        Me.GroupBox1.Controls.Add(Me.txt_PortPosTerminal)
        Me.GroupBox1.Location = New System.Drawing.Point(9, 289)
        Me.GroupBox1.Margin = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(2)
        Me.GroupBox1.Size = New System.Drawing.Size(359, 256)
        Me.GroupBox1.TabIndex = 18
        Me.GroupBox1.TabStop = False
        Me.GroupBox1.Text = "Datos Configuracion"
        '
        'txt_OperatorID
        '
        Me.txt_OperatorID.Location = New System.Drawing.Point(146, 164)
        Me.txt_OperatorID.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_OperatorID.Name = "txt_OperatorID"
        Me.txt_OperatorID.Size = New System.Drawing.Size(198, 20)
        Me.txt_OperatorID.TabIndex = 20
        Me.txt_OperatorID.Text = "TEST"
        '
        'Label12
        '
        Me.Label12.AutoSize = True
        Me.Label12.Location = New System.Drawing.Point(23, 166)
        Me.Label12.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label12.Name = "Label12"
        Me.Label12.Size = New System.Drawing.Size(84, 13)
        Me.Label12.TabIndex = 21
        Me.Label12.Text = "OPERATOR ID:"
        Me.Label12.Visible = False
        '
        'btn_Guardar
        '
        Me.btn_Guardar.Location = New System.Drawing.Point(200, 226)
        Me.btn_Guardar.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_Guardar.Name = "btn_Guardar"
        Me.btn_Guardar.Size = New System.Drawing.Size(76, 25)
        Me.btn_Guardar.TabIndex = 19
        Me.btn_Guardar.Text = "Guardar"
        Me.btn_Guardar.UseVisualStyleBackColor = True
        Me.btn_Guardar.Visible = False
        '
        'btn_Limpiar
        '
        Me.btn_Limpiar.Location = New System.Drawing.Point(110, 227)
        Me.btn_Limpiar.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_Limpiar.Name = "btn_Limpiar"
        Me.btn_Limpiar.Size = New System.Drawing.Size(76, 25)
        Me.btn_Limpiar.TabIndex = 18
        Me.btn_Limpiar.Text = "Limpiar"
        Me.btn_Limpiar.UseVisualStyleBackColor = True
        '
        'Label7
        '
        Me.Label7.AutoSize = True
        Me.Label7.Location = New System.Drawing.Point(25, 30)
        Me.Label7.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label7.Name = "Label7"
        Me.Label7.Size = New System.Drawing.Size(40, 13)
        Me.Label7.TabIndex = 19
        Me.Label7.Text = "Monto:"
        '
        'Label8
        '
        Me.Label8.AutoSize = True
        Me.Label8.Location = New System.Drawing.Point(25, 51)
        Me.Label8.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label8.Name = "Label8"
        Me.Label8.Size = New System.Drawing.Size(40, 13)
        Me.Label8.TabIndex = 20
        Me.Label8.Text = "Cuotas"
        Me.Label8.Visible = False
        '
        'Label9
        '
        Me.Label9.AutoSize = True
        Me.Label9.Location = New System.Drawing.Point(25, 74)
        Me.Label9.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label9.Name = "Label9"
        Me.Label9.Size = New System.Drawing.Size(86, 13)
        Me.Label9.TabIndex = 22
        Me.Label9.Text = "Monto Impuesto:"
        '
        'btn_Cierre
        '
        Me.btn_Cierre.Location = New System.Drawing.Point(209, 161)
        Me.btn_Cierre.Margin = New System.Windows.Forms.Padding(2)
        Me.btn_Cierre.Name = "btn_Cierre"
        Me.btn_Cierre.Size = New System.Drawing.Size(86, 34)
        Me.btn_Cierre.TabIndex = 23
        Me.btn_Cierre.Text = "Cierre"
        Me.btn_Cierre.UseVisualStyleBackColor = True
        Me.btn_Cierre.Visible = False
        '
        'cmb_TipoTrans
        '
        Me.cmb_TipoTrans.FormattingEnabled = True
        Me.cmb_TipoTrans.Items.AddRange(New Object() {"Sale", "Refund"})
        Me.cmb_TipoTrans.Location = New System.Drawing.Point(120, 218)
        Me.cmb_TipoTrans.Margin = New System.Windows.Forms.Padding(2)
        Me.cmb_TipoTrans.Name = "cmb_TipoTrans"
        Me.cmb_TipoTrans.Size = New System.Drawing.Size(76, 21)
        Me.cmb_TipoTrans.TabIndex = 24
        Me.cmb_TipoTrans.Visible = False
        '
        'Label10
        '
        Me.Label10.AutoSize = True
        Me.Label10.Location = New System.Drawing.Point(26, 220)
        Me.Label10.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label10.Name = "Label10"
        Me.Label10.Size = New System.Drawing.Size(93, 13)
        Me.Label10.TabIndex = 25
        Me.Label10.Text = "Tipo Transaccion:"
        Me.Label10.Visible = False
        '
        'Label11
        '
        Me.Label11.AutoSize = True
        Me.Label11.Location = New System.Drawing.Point(26, 97)
        Me.Label11.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label11.Name = "Label11"
        Me.Label11.Size = New System.Drawing.Size(90, 13)
        Me.Label11.TabIndex = 26
        Me.Label11.Text = "Num Autorizacion"
        '
        'txt_NumAut
        '
        Me.txt_NumAut.Location = New System.Drawing.Point(113, 94)
        Me.txt_NumAut.Margin = New System.Windows.Forms.Padding(2)
        Me.txt_NumAut.Name = "txt_NumAut"
        Me.txt_NumAut.Size = New System.Drawing.Size(76, 20)
        Me.txt_NumAut.TabIndex = 27
        '
        'BtnPreCie
        '
        Me.BtnPreCie.Location = New System.Drawing.Point(35, 162)
        Me.BtnPreCie.Margin = New System.Windows.Forms.Padding(2)
        Me.BtnPreCie.Name = "BtnPreCie"
        Me.BtnPreCie.Size = New System.Drawing.Size(86, 34)
        Me.BtnPreCie.TabIndex = 28
        Me.BtnPreCie.Text = "PreCierre"
        Me.BtnPreCie.UseVisualStyleBackColor = True
        Me.BtnPreCie.Visible = False
        '
        'OptTrue1
        '
        Me.OptTrue1.AutoSize = True
        Me.OptTrue1.Location = New System.Drawing.Point(155, 247)
        Me.OptTrue1.Name = "OptTrue1"
        Me.OptTrue1.Size = New System.Drawing.Size(41, 17)
        Me.OptTrue1.TabIndex = 29
        Me.OptTrue1.Text = "ON"
        Me.OptTrue1.UseVisualStyleBackColor = True
        '
        'OptFalse1
        '
        Me.OptFalse1.AutoSize = True
        Me.OptFalse1.Checked = True
        Me.OptFalse1.Location = New System.Drawing.Point(240, 247)
        Me.OptFalse1.Name = "OptFalse1"
        Me.OptFalse1.Size = New System.Drawing.Size(45, 17)
        Me.OptFalse1.TabIndex = 30
        Me.OptFalse1.TabStop = True
        Me.OptFalse1.Text = "OFF"
        Me.OptFalse1.UseVisualStyleBackColor = True
        '
        'lblDebugMode
        '
        Me.lblDebugMode.AutoSize = True
        Me.lblDebugMode.Location = New System.Drawing.Point(28, 249)
        Me.lblDebugMode.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblDebugMode.Name = "lblDebugMode"
        Me.lblDebugMode.Size = New System.Drawing.Size(69, 13)
        Me.lblDebugMode.TabIndex = 31
        Me.lblDebugMode.Text = "DebugMode:"
        '
        'lblTimeout
        '
        Me.lblTimeout.AutoSize = True
        Me.lblTimeout.Location = New System.Drawing.Point(326, 128)
        Me.lblTimeout.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.lblTimeout.Name = "lblTimeout"
        Me.lblTimeout.Size = New System.Drawing.Size(67, 13)
        Me.lblTimeout.TabIndex = 35
        Me.lblTimeout.Text = "Timeout MS:"
        '
        'txtTimeout
        '
        Me.txtTimeout.Location = New System.Drawing.Point(414, 124)
        Me.txtTimeout.Margin = New System.Windows.Forms.Padding(2)
        Me.txtTimeout.Name = "txtTimeout"
        Me.txtTimeout.Size = New System.Drawing.Size(76, 20)
        Me.txtTimeout.TabIndex = 34
        Me.txtTimeout.Text = "75000"
        '
        'btnTest
        '
        Me.btnTest.Location = New System.Drawing.Point(307, 25)
        Me.btnTest.Margin = New System.Windows.Forms.Padding(2)
        Me.btnTest.Name = "btnTest"
        Me.btnTest.Size = New System.Drawing.Size(86, 20)
        Me.btnTest.TabIndex = 36
        Me.btnTest.Text = "** Test **"
        Me.btnTest.UseVisualStyleBackColor = True
        '
        'Button3
        '
        Me.Button3.Location = New System.Drawing.Point(307, 49)
        Me.Button3.Margin = New System.Windows.Forms.Padding(2)
        Me.Button3.Name = "Button3"
        Me.Button3.Size = New System.Drawing.Size(86, 20)
        Me.Button3.TabIndex = 37
        Me.Button3.Text = "Leer Tarjeta"
        Me.Button3.UseVisualStyleBackColor = True
        '
        'Label13
        '
        Me.Label13.AutoSize = True
        Me.Label13.Location = New System.Drawing.Point(29, 127)
        Me.Label13.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label13.Name = "Label13"
        Me.Label13.Size = New System.Drawing.Size(59, 13)
        Me.Label13.TabIndex = 38
        Me.Label13.Text = "Referencia"
        '
        'txtRef
        '
        Me.txtRef.Location = New System.Drawing.Point(113, 128)
        Me.txtRef.Margin = New System.Windows.Forms.Padding(2)
        Me.txtRef.Name = "txtRef"
        Me.txtRef.Size = New System.Drawing.Size(76, 20)
        Me.txtRef.TabIndex = 39
        '
        'txtCardHost
        '
        Me.txtCardHost.Location = New System.Drawing.Point(263, 94)
        Me.txtCardHost.Margin = New System.Windows.Forms.Padding(2)
        Me.txtCardHost.Name = "txtCardHost"
        Me.txtCardHost.Size = New System.Drawing.Size(76, 20)
        Me.txtCardHost.TabIndex = 41
        '
        'Label14
        '
        Me.Label14.AutoSize = True
        Me.Label14.Location = New System.Drawing.Point(205, 97)
        Me.Label14.Margin = New System.Windows.Forms.Padding(2, 0, 2, 0)
        Me.Label14.Name = "Label14"
        Me.Label14.Size = New System.Drawing.Size(54, 13)
        Me.Label14.TabIndex = 40
        Me.Label14.Text = "Card Host"
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6.0!, 13.0!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.ClientSize = New System.Drawing.Size(534, 581)
        Me.Controls.Add(Me.txtCardHost)
        Me.Controls.Add(Me.Label14)
        Me.Controls.Add(Me.txtRef)
        Me.Controls.Add(Me.Label13)
        Me.Controls.Add(Me.Button3)
        Me.Controls.Add(Me.btnTest)
        Me.Controls.Add(Me.lblTimeout)
        Me.Controls.Add(Me.txtTimeout)
        Me.Controls.Add(Me.lblDebugMode)
        Me.Controls.Add(Me.OptFalse1)
        Me.Controls.Add(Me.OptTrue1)
        Me.Controls.Add(Me.BtnPreCie)
        Me.Controls.Add(Me.txt_NumAut)
        Me.Controls.Add(Me.Label11)
        Me.Controls.Add(Me.Label10)
        Me.Controls.Add(Me.cmb_TipoTrans)
        Me.Controls.Add(Me.btn_Cierre)
        Me.Controls.Add(Me.Label9)
        Me.Controls.Add(Me.Label8)
        Me.Controls.Add(Me.Label7)
        Me.Controls.Add(Me.GroupBox1)
        Me.Controls.Add(Me.txt_MontoImp)
        Me.Controls.Add(Me.txt_Cuotas)
        Me.Controls.Add(Me.txt_Monto)
        Me.Controls.Add(Me.Button2)
        Me.Controls.Add(Me.btn_Anulacion)
        Me.Controls.Add(Me.Button1)
        Me.Margin = New System.Windows.Forms.Padding(2)
        Me.Name = "Form1"
        Me.Text = "Form1"
        Me.GroupBox1.ResumeLayout(False)
        Me.GroupBox1.PerformLayout()
        Me.ResumeLayout(False)
        Me.PerformLayout()

    End Sub
    Friend WithEvents Button1 As System.Windows.Forms.Button
    Friend WithEvents btn_Anulacion As System.Windows.Forms.Button
    Friend WithEvents Button2 As System.Windows.Forms.Button
    Friend WithEvents txt_Monto As System.Windows.Forms.TextBox
    Friend WithEvents txt_Cuotas As System.Windows.Forms.TextBox
    Friend WithEvents txt_MontoImp As System.Windows.Forms.TextBox
    Friend WithEvents txt_IPPosTerminal As System.Windows.Forms.TextBox
    Friend WithEvents txt_PortCajaPC As System.Windows.Forms.TextBox
    Friend WithEvents txt_IPCajaPC As System.Windows.Forms.TextBox
    Friend WithEvents txt_MultiAcquirerID As System.Windows.Forms.TextBox
    Friend WithEvents txt_MultiMerchantID As System.Windows.Forms.TextBox
    Friend WithEvents txt_PortPosTerminal As System.Windows.Forms.TextBox
    Friend WithEvents Label1 As System.Windows.Forms.Label
    Friend WithEvents Label2 As System.Windows.Forms.Label
    Friend WithEvents Label3 As System.Windows.Forms.Label
    Friend WithEvents Label4 As System.Windows.Forms.Label
    Friend WithEvents Label5 As System.Windows.Forms.Label
    Friend WithEvents Label6 As System.Windows.Forms.Label
    Friend WithEvents GroupBox1 As System.Windows.Forms.GroupBox
    Friend WithEvents btn_Guardar As System.Windows.Forms.Button
    Friend WithEvents btn_Limpiar As System.Windows.Forms.Button
    Friend WithEvents Label7 As System.Windows.Forms.Label
    Friend WithEvents Label8 As System.Windows.Forms.Label
    Friend WithEvents Label9 As System.Windows.Forms.Label
    Friend WithEvents btn_Cierre As System.Windows.Forms.Button
    Friend WithEvents cmb_TipoTrans As System.Windows.Forms.ComboBox
    Friend WithEvents Label10 As System.Windows.Forms.Label
    Friend WithEvents Label11 As System.Windows.Forms.Label
    Friend WithEvents txt_NumAut As System.Windows.Forms.TextBox
    Friend WithEvents BtnPreCie As Button
    Friend WithEvents OptTrue1 As RadioButton
    Friend WithEvents OptFalse1 As RadioButton
    Friend WithEvents lblDebugMode As Label
    Friend WithEvents lblTimeout As Label
    Friend WithEvents txtTimeout As TextBox
    Friend WithEvents txt_OperatorID As TextBox
    Friend WithEvents Label12 As Label
    Friend WithEvents btnTest As Button
    Friend WithEvents Button3 As Button
    Friend WithEvents Label13 As Label
    Friend WithEvents txtRef As TextBox
    Friend WithEvents txtCardHost As TextBox
    Friend WithEvents Label14 As Label
End Class
