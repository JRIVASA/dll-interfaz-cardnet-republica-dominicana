﻿Public Class VB6Aux

    Public Sub ShowBigDebugMessage(ByVal pMsg As String)
        MsgBox(pMsg)
    End Sub

    Private Function SafeWriteTextFile(ByVal pFilePath As String, ByVal pText As String, ByVal pAppend As Boolean) As Boolean
        Try
            My.Computer.FileSystem.WriteAllText(pFilePath, pText, pAppend)
            SafeWriteTextFile = True
        Catch ex As Exception
            Return False
        End Try
    End Function

    Public Function ResolverRutaDinamica(ByVal pRuta As String) As String

        Try

            pRuta = Replace(pRuta, "$(AppPath)", System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().Location), , , CompareMethod.Text)
            pRuta = Replace(pRuta, "$(CallerAppPath)", System.IO.Path.GetDirectoryName(
                System.Reflection.Assembly.GetExecutingAssembly().Location), , , CompareMethod.Text)
            pRuta = Replace(pRuta, "$(CurDrive)", Left(Application.ExecutablePath, 2), , , CompareMethod.Text)

            ResolverRutaDinamica = pRuta

        Catch ex As Exception

            Return pRuta

        End Try

    End Function

    Public Function SaveQuickLogFile(ByVal pText As String, ByVal pRuta As String, ByVal pArchivo As String) As Boolean

        Try

            pRuta = ResolverRutaDinamica(pRuta)

            Return SafeWriteTextFile(pRuta & "\" & pArchivo, pText, False)

        Catch ex As Exception

            Return False

        End Try

    End Function

    Public Function Azul_GrabarLogBD(ByRef pDatosLog As Object,
    Optional ByVal pFin As Boolean = False) As Boolean
        Return False
    End Function

End Class
