﻿Imports System.Net
Imports ECRti
Imports ECRti.Dtos
Imports Microsoft.VisualBasic

Public Class Form1

    Dim RespB As Boolean

    Private Cardnet_Response_Info As Object
    Public Property ResponseInfo() As Object
        Get
            Return Cardnet_Response_Info
        End Get
        Set(ByVal value As Object)
            Cardnet_Response_Info = value
        End Set
    End Property

    Private Const CorrelativoTestFactura = 150
    Private ProximaSecuenciaFactura As Integer

    Public DebugMode As Boolean

    Private LocalData As Dictionary(Of Object, Object)

    Private NetConn As EthernetCommunication
    Private SerialConn As SerialPortCommunication

    Private LANSettings As ECRti.Settings.Ethernet
    Private SerialPortSettings As ECRti.Settings.SerialPort

    Private Sub ValidateIPAddressV4(ByVal IPAddrV4 As String)

        Const InvalidIPAddress = "La dirección IP debe de ser versión 4. Ejemplo \"" + 143.24.20.36\"""
        Const RegexIPv4 = "\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}\b"

        Dim Match As System.Text.RegularExpressions.Match = System.Text.RegularExpressions.Regex.Match(
            IPAddrV4, RegexIPv4, System.Text.RegularExpressions.RegexOptions.IgnoreCase)

        If (Not Match.Success) Then
            Throw New InvalidOperationException(InvalidIPAddress)
        End If

    End Sub

    Private Sub Button1_Click(sender As Object, e As EventArgs) Handles Button1.Click

        Try


            If IsNothing(LocalData) Then LocalData = New Dictionary(Of Object, Object) ' Uso Interno de Esta DLL .NET - Exclusivo

            ResponseInfo = New Microsoft.VisualBasic.Collection

            NetConn = Nothing

            '

            ValidateIPAddressV4(txt_IPCajaPC.Text)
            ValidateIPAddressV4(txt_IPPosTerminal.Text)

            'Dim TCELocalIPNumber As Long, TCERemoteIPNumber As Long
            Dim TCELocalIPAddress As New IPAddress(1), TCERemoteIPAddress As New IPAddress(1)

            IPAddress.TryParse(txt_IPCajaPC.Text, TCELocalIPAddress)
            IPAddress.TryParse(txt_IPPosTerminal.Text, TCERemoteIPAddress)

            Dim TCELocalIP As Net.IPEndPoint = New Net.IPEndPoint(TCELocalIPAddress, txt_PortCajaPC.Text)
            Dim TCERemoteIP As Net.IPEndPoint = New Net.IPEndPoint(TCERemoteIPAddress, txt_PortPosTerminal.Text)
            Dim TCETimeout As Int64 = CType(txtTimeout.Text, Int64)

            DebugMode = OptTrue1.Checked

            If DebugMode Then
                MsgBox("IP Local: " + TCELocalIP.Address.ToString + " - " + TCELocalIP.Port.ToString + vbNewLine +
                        "IP Remota: " + TCERemoteIP.Address.ToString + " - " + TCERemoteIP.Port.ToString + vbNewLine +
                        "ReadTimeout: " + TCETimeout.ToString)
            End If

            LANSettings = New Settings.Ethernet(TCELocalIP, TCERemoteIP, TCETimeout)

            NetConn = New EthernetCommunication(LANSettings)
            NetConn.Prop_DebugMode = DebugMode

            Dim TransData As TransactionDto

            If ProximaSecuenciaFactura = 0 Then
                ProximaSecuenciaFactura = CorrelativoTestFactura - 1
            End If

            ProximaSecuenciaFactura = ProximaSecuenciaFactura + 1

            Dim Input_InvoiceNumber As String = "000" + ProximaSecuenciaFactura.ToString() 'New Random().Next(100000, 999999).ToString()

            Dim TransDataRequest As TransactionDto = New TransactionDto() With {
                .MultiMerchantID = CByte(IIf(String.IsNullOrEmpty(txt_MultiMerchantID.Text), 0, txt_MultiMerchantID.Text)),
                .MultiAcquirerID = CByte(IIf(String.IsNullOrEmpty(txt_MultiAcquirerID.Text), 0, txt_MultiAcquirerID.Text)),
                .Folio = 1, .CheckInDate = Now, .CheckOutDate = Now,
                .TransactionAmount = CDec(txt_Monto.Text),
                .TransactionITBIS = CDec(txt_MontoImp.Text),
                .TransactionOtherTaxes = CDec(0),
                .InvoiceNumber = Microsoft.VisualBasic.Strings.Right(ProximaSecuenciaFactura, 6)
            }

            TransData = NetConn.AuthorizeTransaction(
            TransactionDto.TransactionTypes.NormalSale, TransDataRequest)

            Console.WriteLine(TransData)

            'Console.WriteLine(TransData.SaleResultTypeIndicator)
            'Console.WriteLine(TransData.DCCAccepted)
            'Console.WriteLine(TransData.DCCCurrencyIdentifier)
            'Console.WriteLine(TransData.DCCDisplayRate)
            'Console.WriteLine(TransData.DCCMarginPercentage)
            'Console.WriteLine(TransData.DCCTransactionAmount)

            MsgBox(TransData.AuthorizationNumber)

            txt_NumAut.Text = TransData.AuthorizationNumber
            txtRef.Text = TransData.ReferenceNumber
            txtCardHost.Text = Convert.ToInt32(TransData.Host).ToString()

        Catch ex As Exception

            Dim CodigoStr As String = Err.Number
            Dim ErrorStr As String = Err.Description

            MsgBox(ErrorStr)

        End Try

    End Sub

    Private Sub btn_Anulacion_Click(sender As Object, e As EventArgs) Handles btn_Anulacion.Click

        Try


            If IsNothing(LocalData) Then LocalData = New Dictionary(Of Object, Object) ' Uso Interno de Esta DLL .NET - Exclusivo

            ResponseInfo = New Microsoft.VisualBasic.Collection

            NetConn = Nothing


            '

            ValidateIPAddressV4(txt_IPCajaPC.Text)
            ValidateIPAddressV4(txt_IPPosTerminal.Text)

            'Dim TCELocalIPNumber As Long, TCERemoteIPNumber As Long
            Dim TCELocalIPAddress As New IPAddress(1), TCERemoteIPAddress As New IPAddress(1)

            IPAddress.TryParse(txt_IPCajaPC.Text, TCELocalIPAddress)
            IPAddress.TryParse(txt_IPPosTerminal.Text, TCERemoteIPAddress)

            Dim TCELocalIP As Net.IPEndPoint = New Net.IPEndPoint(TCELocalIPAddress, txt_PortCajaPC.Text)
            Dim TCERemoteIP As Net.IPEndPoint = New Net.IPEndPoint(TCERemoteIPAddress, txt_PortPosTerminal.Text)
            Dim TCETimeout As Int64 = CType(txtTimeout.Text, Int64)

            DebugMode = OptTrue1.Checked

            If DebugMode Then
                MsgBox("IP Local: " + TCELocalIP.Address.ToString + " - " + TCELocalIP.Port.ToString + vbNewLine +
                        "IP Remota: " + TCERemoteIP.Address.ToString + " - " + TCERemoteIP.Port.ToString + vbNewLine +
                        "ReadTimeout: " + TCETimeout.ToString)
            End If

            LANSettings = New Settings.Ethernet(TCELocalIP, TCERemoteIP, TCETimeout)

            NetConn = New EthernetCommunication(LANSettings)
            NetConn.Prop_DebugMode = DebugMode

            Dim TransData As TransactionDto

            If ProximaSecuenciaFactura = 0 Then
                ProximaSecuenciaFactura = CorrelativoTestFactura - 1
            End If

            ProximaSecuenciaFactura = ProximaSecuenciaFactura + 1

            Dim Input_InvoiceNumber As String = "000" + ProximaSecuenciaFactura.ToString() 'New Random().Next(100000, 999999).ToString()

            Dim TransDataRequest As TransactionDto = New TransactionDto() With {
                .MultiMerchantID = CByte(IIf(String.IsNullOrEmpty(txt_MultiMerchantID.Text), 0, txt_MultiMerchantID.Text)),
                .MultiAcquirerID = CByte(IIf(String.IsNullOrEmpty(txt_MultiAcquirerID.Text), 0, txt_MultiAcquirerID.Text)),
                .Folio = 1, .CheckInDate = Now, .CheckOutDate = Now,
                .TransactionAmount = CDec(txt_Monto.Text),
                .TransactionITBIS = CDec(txt_MontoImp.Text),
                .TransactionOtherTaxes = CDec(0),
                .Host = CType(txtCardHost.Text, CardDto.Hosts),
                .AuthorizationNumber = txt_NumAut.Text,
                .ReferenceNumber = CShort(txtRef.Text)
            }

            TransData = NetConn.AuthorizeTransaction(
            TransactionDto.TransactionTypes.CancellationNormalSale, TransDataRequest)

            Console.WriteLine(TransData)

            MsgBox(TransData.AuthorizationNumber)

            txt_NumAut.Text = vbEmpty
            txtRef.Text = vbEmpty
            txtCardHost.Text = vbEmpty

        Catch ex As Exception

            Dim CodigoStr As String = Err.Number
            Dim ErrorStr As String = Err.Description

            MsgBox(ErrorStr)

        End Try

    End Sub

    Private Sub Button2_Click(sender As Object, e As EventArgs) Handles Button2.Click

    End Sub

    Private Sub Form1_Load(sender As Object, e As EventArgs) Handles MyBase.Load

        'Try
        '    ServicePointManager.SecurityProtocol = (SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12 Or SecurityProtocolType.Ssl3)
        'Catch
        'End Try

        'txt_IPCajaPC.Text = "https://pruebas.azul.com.do"
        'txt_PortCajaPC.Text = "https://pruebas.azul.com.do"
        'txt_IPPosTerminal.Text = "merit"
        'txt_PortPosTerminal.Text = "merit"
        'txt_MultiAcquirerID.Text = "39036630010"
        'txt_MultiMerchantID.Text = "01290010"

    End Sub

    Private Sub btn_Limpiar_Click(sender As Object, e As EventArgs) Handles btn_Limpiar.Click

        txt_IPCajaPC.Text = ""
        txt_PortCajaPC.Text = ""
        txt_IPPosTerminal.Text = ""
        txt_PortPosTerminal.Text = ""
        txt_MultiAcquirerID.Text = ""
        txt_MultiMerchantID.Text = ""
        txt_OperatorID.Text = ""

    End Sub

    Private Sub btn_Guardar_Click(sender As Object, e As EventArgs) Handles btn_Guardar.Click

    End Sub

    Private Sub btn_Cierre_Click(sender As Object, e As EventArgs) Handles btn_Cierre.Click

    End Sub

    Private Sub BtnPreCie_Click(sender As Object, e As EventArgs) Handles BtnPreCie.Click

    End Sub

    Private Sub btnTest_Click(sender As Object, e As EventArgs) Handles btnTest.Click

        Try


            If IsNothing(LocalData) Then LocalData = New Dictionary(Of Object, Object) ' Uso Interno de Esta DLL .NET - Exclusivo

            ResponseInfo = New Microsoft.VisualBasic.Collection

            NetConn = Nothing

            '

            ValidateIPAddressV4(txt_IPCajaPC.Text)
            ValidateIPAddressV4(txt_IPPosTerminal.Text)

            'Dim TCELocalIPNumber As Long, TCERemoteIPNumber As Long
            Dim TCELocalIPAddress As New IPAddress(1), TCERemoteIPAddress As New IPAddress(1)

            IPAddress.TryParse(txt_IPCajaPC.Text, TCELocalIPAddress)
            IPAddress.TryParse(txt_IPPosTerminal.Text, TCERemoteIPAddress)

            Dim TCELocalIP As Net.IPEndPoint = New Net.IPEndPoint(TCELocalIPAddress, txt_PortCajaPC.Text)
            Dim TCERemoteIP As Net.IPEndPoint = New Net.IPEndPoint(TCERemoteIPAddress, txt_PortPosTerminal.Text)
            Dim TCETimeout As Int64 = CType(txtTimeout.Text, Int64)

            DebugMode = OptTrue1.Checked

            If DebugMode Then
                MsgBox("IP Local: " + TCELocalIP.Address.ToString + " - " + TCELocalIP.Port.ToString + vbNewLine +
                        "IP Remota: " + TCERemoteIP.Address.ToString + " - " + TCERemoteIP.Port.ToString + vbNewLine +
                        "ReadTimeout: " + TCETimeout.ToString)
            End If

            LANSettings = New Settings.Ethernet(TCELocalIP, TCERemoteIP, TCETimeout)

            NetConn = New EthernetCommunication(LANSettings)
            NetConn.Prop_DebugMode = DebugMode

            If ProximaSecuenciaFactura = 0 Then
                ProximaSecuenciaFactura = CorrelativoTestFactura - 1
            End If

            ProximaSecuenciaFactura = ProximaSecuenciaFactura + 1

            Dim Input_InvoiceNumber As String = "000" + ProximaSecuenciaFactura.ToString() 'New Random().Next(100000, 999999).ToString()

            Dim Result As String()

            Result = NetConn.Test_WriteReadPOS_DoubleTransaction(
            Convert.ToDecimal(txt_Monto.Text), Convert.ToDecimal(txt_MontoImp.Text),
            ProximaSecuenciaFactura)

            Console.WriteLine(Result)

            If (Not Result Is Nothing) Then
                MsgBox(Result.Length.ToString() + Environment.NewLine + Result.ToString())
            Else
                MsgBox("Trx Fail")
            End If

        Catch ex As Exception

            Dim CodigoStr As String = Err.Number
            Dim ErrorStr As String = Err.Description

            MsgBox(ErrorStr)

        End Try

    End Sub

    Private Sub Button3_Click(sender As Object, e As EventArgs) Handles Button3.Click

        Try


            If IsNothing(LocalData) Then LocalData = New Dictionary(Of Object, Object) ' Uso Interno de Esta DLL .NET - Exclusivo

            ResponseInfo = New Microsoft.VisualBasic.Collection

            NetConn = Nothing

            '

            ValidateIPAddressV4(txt_IPCajaPC.Text)
            ValidateIPAddressV4(txt_IPPosTerminal.Text)

            'Dim TCELocalIPNumber As Long, TCERemoteIPNumber As Long
            Dim TCELocalIPAddress As New IPAddress(1), TCERemoteIPAddress As New IPAddress(1)

            IPAddress.TryParse(txt_IPCajaPC.Text, TCELocalIPAddress)
            IPAddress.TryParse(txt_IPPosTerminal.Text, TCERemoteIPAddress)

            Dim TCELocalIP As Net.IPEndPoint = New Net.IPEndPoint(TCELocalIPAddress, txt_PortCajaPC.Text)
            Dim TCERemoteIP As Net.IPEndPoint = New Net.IPEndPoint(TCERemoteIPAddress, txt_PortPosTerminal.Text)
            Dim TCETimeout As Int64 = CType(txtTimeout.Text, Int64)

            DebugMode = OptTrue1.Checked

            If DebugMode Then
                MsgBox("IP Local: " + TCELocalIP.Address.ToString + " - " + TCELocalIP.Port.ToString + vbNewLine +
                        "IP Remota: " + TCERemoteIP.Address.ToString + " - " + TCERemoteIP.Port.ToString + vbNewLine +
                        "ReadTimeout: " + TCETimeout.ToString)
            End If

            LANSettings = New Settings.Ethernet(TCELocalIP, TCERemoteIP, TCETimeout)

            NetConn = New EthernetCommunication(LANSettings)
            NetConn.Prop_DebugMode = DebugMode

            Dim CardData As CardDto

            CardData = NetConn.RequestCardInformation()

            If Not CardData Is Nothing Then
                Console.WriteLine(CardData)
                MsgBox(CardData.AccountNumber)
                txtCardHost.Text = CardData.AccountNumber
            Else
                MsgBox("Fail")
            End If

        Catch ex As Exception

            Dim CodigoStr As String = Err.Number
            Dim ErrorStr As String = Err.Description

            MsgBox(ErrorStr)

        End Try

    End Sub
End Class
