﻿Imports System.IO
Imports System.Net
Imports ECRti.Dtos
Imports ECRti
Imports Microsoft.VisualBasic
Imports System.Xml
Imports System.Xml.XPath

Public Class ClsInterfazCardnetRD

    Private DCI As Object, DRI As Object

    Private Cardnet_Request_Info As Object
    Public Property RequestInfo() As Object
        Get
            Return Cardnet_Request_Info
        End Get
        Set(ByVal value As Object)
            Cardnet_Request_Info = value
        End Set
    End Property

    Private Cardnet_Response_Info As Object
    Public Property ResponseInfo() As Object
        Get
            Return Cardnet_Response_Info
        End Get
        Set(ByVal value As Object)
            Cardnet_Response_Info = value
        End Set
    End Property

    Private VB6Aux As Object
    Public Property VB6AuxObj() As Object
        Get
            Return VB6Aux
        End Get
        Set(ByVal value As Object)
            VB6Aux = value
        End Set
    End Property

    Private DebugMode As Boolean

    Public Property Prop_DebugMode() As Boolean
        Get
            Return DebugMode
        End Get
        Set(ByVal value As Boolean)
            DebugMode = value
        End Set
    End Property

    Private LocalData As Dictionary(Of Object, Object)

    Public NetConn As EthernetCommunication
    Public SerialConn As SerialPortCommunication

    Public LANSettings As ECRti.Settings.Ethernet
    Public SerialPortSettings As ECRti.Settings.SerialPort

    Private Sub ValidateIPAddressV4(ByVal IPAddrV4 As String)
        
        Const InvalidIPAddress = "La dirección IP debe de ser versión 4. Ejemplo \"" + 143.24.20.36\"""
        Const RegexIPv4 = "\b((25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)(\.|$)){4}\b"

        Dim Match As System.Text.RegularExpressions.Match = System.Text.RegularExpressions.Regex.Match(IPAddrV4, RegexIPv4, Text.RegularExpressions.RegexOptions.IgnoreCase)
        
        If (Not Match.Success) Then
            Throw New InvalidOperationException(InvalidIPAddress)
        End If

    End Sub

    Public Sub CloseListener()

        Try

            If Not NetConn Is Nothing Then

                If DebugMode Then MsgBox("Attempting Listener.Close")

                NetConn.listener.Close()

            End If

            'NetConn = Nothing

            If DebugMode Then MsgBox("Listener.Close Successfully")

        Catch ex As Exception

            If DebugMode Then
                MsgBox("Listener.Close Error. " + Environment.NewLine +
                ex.Message + Environment.NewLine + ex.StackTrace)
            End If

        End Try

    End Sub

    Public Sub BeginProcess()

        If IsNothing(LocalData) Then LocalData = New Dictionary(Of Object, Object) ' Uso Interno de Esta DLL .NET - Exclusivo

        'Dim ResponseInfo As Microsoft.VisualBasic.Collection
        ResponseInfo = New Microsoft.VisualBasic.Collection

        DCI = Cardnet_Request_Info

        If Not DCI.Exists("ListenerIsAlive") Then
            DCI("ListenerIsAlive") = False
        End If

        If DebugMode Then
            MsgBox("ListenerIsAlive: " + DCI("ListenerIsAlive").ToString())
        End If

        If DCI("ListenerIsAlive") Then
            'NetConn Already Set and has a live connection and Listener active. 
            'Need to keep using that one.
        Else
            NetConn = Nothing
            SerialConn = Nothing
        End If

        If DCI("TrainingMode") Then

            ' No hay que inicializar nada.

        Else

            If DCI("TipoConexion") = 1 Then

                If DCI("ListenerIsAlive") Then

                    'NetConn Already Set and has a live connection and Listener. 
                    'Need to keep using that one.

                Else

                    ValidateIPAddressV4(DCI("TCE_POS_IP"))
                    ValidateIPAddressV4(DCI("TCE_POSTerminal_IP"))

                    'Dim TCELocalIPNumber As Long, TCERemoteIPNumber As Long
                    Dim TCELocalIPAddress As New IPAddress(1), TCERemoteIPAddress As New IPAddress(1)

                    IPAddress.TryParse(DCI("TCE_POS_IP"), TCELocalIPAddress)
                    IPAddress.TryParse(DCI("TCE_POSTerminal_IP"), TCERemoteIPAddress)

                    Dim TCELocalIP As Net.IPEndPoint = New Net.IPEndPoint(TCELocalIPAddress, DCI("TCE_POS_Puerto"))
                    Dim TCERemoteIP As Net.IPEndPoint = New Net.IPEndPoint(TCERemoteIPAddress, DCI("TCE_POSTerminal_Puerto"))
                    Dim TCETimeout As Int64 = CType(DCI("TCE_ReadTimeout"), Int64)

                    If DebugMode Then
                        MsgBox("IP Local: " + TCELocalIP.Address.ToString + " - " + TCELocalIP.Port.ToString + vbNewLine +
                        "IP Remota: " + TCERemoteIP.Address.ToString + " - " + TCERemoteIP.Port.ToString + vbNewLine +
                        "ReadTimeout: " + TCETimeout.ToString)
                    End If

                    LANSettings = New Settings.Ethernet(TCELocalIP, TCERemoteIP, TCETimeout)

                    NetConn = New EthernetCommunication(LANSettings)

                    'If DebugMode Then
                    'MsgBox("NetConn Enabled: " + NetConn.ControlCharacters.ACK.ToString)
                    'End If

                End If

                NetConn.Prop_DebugMode = DebugMode

            ElseIf DCI("TipoConexion") = 2 Then

                If DCI("ListenerIsAlive") Then

                    'NetConn Already Set and has a live connection and Listener. 
                    'Need to keep using that one.

                Else

                    Dim TCSPort As String = DCI("TCS_Puerto")
                    Dim TCSBaud As Settings.SerialPort.Baud = CType(DCI("TCS_Baudios"), Settings.SerialPort.Baud)
                    Dim TCSDataBits As Settings.SerialPort.DataBitsLength = CType(DCI("TCS_BitsDatos"), Settings.SerialPort.DataBitsLength)
                    Dim TCSParity As Ports.Parity = CType(DCI("TCS_Paridad"), Ports.Parity)
                    Dim TCSStopBits As Ports.StopBits = CType(DCI("TCS_BitsParada"), Ports.StopBits)
                    Dim TCSReadTimeout As Int32 = CType(DCI("TCS_ReadTimeout"), Int32)
                    Dim TCSWriteTimeout As Int32 = CType(DCI("TCS_WriteTimeout"), Int32)

                    If DebugMode Then
                        MsgBox("Port: " + TCSPort + vbNewLine +
                        "Baud: " + TCSBaud.ToString + vbNewLine +
                        "DataBits: " + TCSDataBits.ToString + vbNewLine +
                        "Parity: " + TCSParity.ToString + vbNewLine +
                        "StopBits: " + TCSStopBits.ToString + vbNewLine +
                        "ReadTimeout: " + TCSReadTimeout.ToString + vbNewLine +
                        "WriteTimeout: " + TCSWriteTimeout.ToString + vbNewLine
                        )
                    End If

                    SerialPortSettings = New Settings.SerialPort(TCSPort, TCSBaud, TCSDataBits, TCSParity, TCSStopBits, TCSReadTimeout, TCSWriteTimeout)

                    SerialConn = New SerialPortCommunication(SerialPortSettings)

                    'If DebugMode Then
                    'MsgBox("SerialConn Enabled: " + SerialConn.ControlCharacters.ACK.ToString)
                    'End If

                End If

            End If

        End If

        Select Case UCase(DCI("TipoOperacion"))

            Case UCase("LeerTarjeta")

                EMVGetCardInfo()

            Case UCase("LeerTarjetaPromoBIN")

                EMVGetCardInfo_KeepListenerAlive()

            Case UCase("Venta")

                EMVSale()

            Case UCase("Anulacion")

                EMVVoid()

            Case UCase("Devolucion")

                EMVReturn()

            Case UCase("ObtenerFirma")

                'GetSignature()

            Case UCase("Precierre")

                'EMVBatchSummary()

            Case UCase("CierreLote")

                BatchClose()

        End Select

    End Sub

    Private Sub LimpiarVariables()

        Try


        Catch ex As Exception
            MsgBox(Err.Description, , "Limpiar Variables")
        End Try

    End Sub

    Private Function MainTest()

        ''Dim TS As New ECRti.Settings.Ethernet()
        'Dim TS As ECRti.Settings.Ethernet

        ''TS.IPLocalEndPointV4 = New Net.IPEndPoint(New )

        'Dim TS2 As New ECRti.EthernetCommunication(TS)

        'Dim DTO As New ECRti.Dtos.TransactionDto() With {
        '.AccountNumber = "3489439843984"
        '}

        'Dim DTO1 As New ECRti.Dtos.TransactionDto() With {
        '    .AccountNumber = 111,
        '}

        'DTO = TS2.AuthorizeTransaction(ECRti.Dtos.TransactionDto.TransactionTypes.NormalSale, DTO)

        ''TS2.AuthorizeTransaction(ECRti.Dtos.TransactionDto.TransactionTypes.NormalSale, New ECRti.Dtos.TransactionDto() With {})
        ''TS2.

        Return True

    End Function

    Public Function GetIP(IP As String) As System.Net.IPAddress
        GetIP = System.Net.IPAddress.Parse(IP)
    End Function

    Public Sub New()
        Try
            ServicePointManager.SecurityProtocol = (SecurityProtocolType.Tls Or SecurityProtocolType.Tls11 Or SecurityProtocolType.Tls12 Or SecurityProtocolType.Ssl3)
        Catch
        End Try
        InitializePort(String.Empty)
    End Sub

    Public Function InitializePort(ByVal pPort As String)

        'If String.IsNullOrEmpty(pPort) Then

        'ServicePort = "7060" ' Default

        'Else

        'If DebugMode Then MsgBox(pPort, MsgBoxStyle.Information, "DebugMode - Port")

        'End If

        'ServiceLink = "http://localhost:" + ServicePort + "/vpos/metodo"

        Return True

    End Function

    Public Function GetHostDescription(ByVal HostID As CardDto.Hosts) As String
        Select Case HostID
            Case CardDto.Hosts.Amex
                GetHostDescription = "AMEX"
            Case CardDto.Hosts.Credit
                GetHostDescription = "CREDITO"
            Case CardDto.Hosts.Debit
                GetHostDescription = "DEBITO"
            Case CardDto.Hosts.Other
                GetHostDescription = "OTROS"
            Case CardDto.Hosts.Peaje
                GetHostDescription = "PEAJE"
            Case CardDto.Hosts.Privada
                GetHostDescription = "PRIVADO"
            Case CardDto.Hosts.Tpago
                GetHostDescription = "TPAGO"
            Case CardDto.Hosts.Undefined
                GetHostDescription = "N/A"
            Case Else
                GetHostDescription = "N/A"
        End Select
    End Function

    Public Function GetCardProduct(ByVal CardProductID As CardDto.CardProducts) As String
        Select Case CardProductID
            Case CardDto.CardProducts.Visa
                GetCardProduct = "VISA"
            Case CardDto.CardProducts.MC
                GetCardProduct = "MC"
            Case CardDto.CardProducts.ATH
                GetCardProduct = "ATH"
            Case CardDto.CardProducts.AMEX
                GetCardProduct = "AMEX"
            Case Else
                GetCardProduct = "N/A"
        End Select
    End Function

    Public Function GetTransactionMode(ByVal TransactionModeID As String) As String
        Select Case TransactionModeID
            Case "D@5"
                GetTransactionMode = "Venta con presencia de banda magnetica"
            Case "D@6"
                GetTransactionMode = "Anulacion de venta con presencia de banda magnetica"
            Case "C@5"
                GetTransactionMode = "Venta con chip (EMV)"
            Case "C@6"
                GetTransactionMode = "Anulacion de venta con chip (EMV)"
            Case "F@5"
                GetTransactionMode = "Venta Fallback"
            Case "F@6"
                GetTransactionMode = "Anulacion Fallback"
            Case "A@5"
                GetTransactionMode = "Venta con proximidad (Contactless)"
            Case "A@6"
                GetTransactionMode = "Anulacion de venta con proximidad (Contactless)"
            Case "B@5"
                GetTransactionMode = "Venta con proximidad (Fallback)"
            Case "B@6"
                GetTransactionMode = "Anulacion de venta con proximidad (Fallback)"
            Case "T@5"
                GetTransactionMode = "Entrada manual sin presencia del plastico"
            Case "T@6"
                GetTransactionMode = "Anulacion de venta sin presencia del plastico"
            Case Else
                GetTransactionMode = "N/A"
        End Select
    End Function

    Public Function CardResult() As CardDto

        CardResult = New CardDto

        If DCI("TrainingMode") Then

            Dim Lucky As Int32

            Lucky = RandomInt32(100)

            Select Case Lucky

                Case 1 To 10

                    Return Nothing

                Case 11 To 30

                    Return New CardDto() With
                    {
                        .Host = 99,
                        .AccountNumber = String.Empty,
                        .CardHolderName = String.Empty,
                        .CardProduct = CardDto.CardProducts.Visa,
                        .ServiceCode = 200,
                        .TransactionMode = "C@5"
                    }

                Case Else '31 To 100

                    Dim TmpResp As New CardDto() With
                    {
                        .Host = IIf(Lucky > 90, CardDto.Hosts.Amex,
                        IIf(Lucky > 70, CardDto.Hosts.Credit,
                        IIf(Lucky > 40, CardDto.Hosts.Debit,
                        CardDto.Hosts.Other))),
                        .AccountNumber = IIf(RandomInt32(100) > 50, "154689******4578", "123456******3456"),
                        .CardHolderName = IIf(RandomInt32(100) > 50, "CARDHOLDER 1", "CARDHOLDER 2"),
                        .CardProduct = IIf(RandomInt32(100) > 50, CardDto.CardProducts.MC, CardDto.CardProducts.Visa),
                        .ServiceCode = RandomInt32(125, 100),
                        .TransactionMode = IIf(RandomInt32(100) > 50, "D@5", "C@5")
                    }

                    Return TmpResp

            End Select

        Else

            If DCI("TipoConexion") = 1 Then

                If DebugMode Then
                    MsgBox("NetConn.RequestCardInformation()" + vbNewLine + vbNewLine +
                    "**Internal Use** - ListenerIsAlive: " + Convert.ToString(DCI("ListenerIsAlive"))
                    )
                End If

                CardResult = NetConn.RequestCardInformation()

            ElseIf DCI("TipoConexion") = 2 Then
                CardResult = SerialConn.RequestCardInformation()
            End If

            If Not IsNothing(CardResult) Then

                If IsNothing(CardResult.Host) Then CardResult.Host = CardDto.Hosts.Undefined
                If IsNothing(CardResult.AccountNumber) Then CardResult.AccountNumber = String.Empty
                If IsNothing(CardResult.CardHolderName) Then CardResult.CardHolderName = String.Empty
                CardResult.CardHolderName = System.Text.RegularExpressions.Regex.Replace(CardResult.CardHolderName, "[^a-zA-Z0-9_ .\-]", String.Empty)
                If IsNothing(CardResult.CardProduct) Then CardResult.CardProduct = CardDto.CardProducts.Visa
                If IsNothing(CardResult.ServiceCode) Then CardResult.ServiceCode = 0
                If IsNothing(CardResult.TransactionMode) Then CardResult.TransactionMode = String.Empty

            End If

        End If

    End Function

    Public Function CardResult_KeepListenerAlive() As CardDto

        CardResult_KeepListenerAlive = New CardDto

        If DCI("TrainingMode") Then

            Dim Lucky As Int32

            Lucky = RandomInt32(100)

            Select Case Lucky

                Case 1 To 10

                    Return Nothing

                Case 11 To 30

                    Return New CardDto() With
                    {
                        .Host = 99,
                        .AccountNumber = String.Empty,
                        .CardHolderName = String.Empty,
                        .CardProduct = CardDto.CardProducts.Visa,
                        .ServiceCode = 200,
                        .TransactionMode = "C@5"
                    }

                Case Else '31 To 100

                    Dim TmpResp As New CardDto() With
                    {
                        .Host = IIf(Lucky > 90, CardDto.Hosts.Amex,
                        IIf(Lucky > 70, CardDto.Hosts.Credit,
                        IIf(Lucky > 40, CardDto.Hosts.Debit,
                        CardDto.Hosts.Other))),
                        .AccountNumber = IIf(RandomInt32(100) > 50, "154689******4578", "123456******3456"),
                        .CardHolderName = IIf(RandomInt32(100) > 50, "CARDHOLDER 1", "CARDHOLDER 2"),
                        .CardProduct = IIf(RandomInt32(100) > 50, CardDto.CardProducts.MC, CardDto.CardProducts.Visa),
                        .ServiceCode = RandomInt32(125, 100),
                        .TransactionMode = IIf(RandomInt32(100) > 50, "D@5", "C@5")
                    }

                    Return TmpResp

            End Select

        Else

            If DCI("TipoConexion") = 1 Then

                If DebugMode Then
                    MsgBox("NetConn.RequestCardInformation_KeepListenerAlive()" + vbNewLine + vbNewLine +
                    "**Internal Use** - ListenerIsAlive: " + Convert.ToString(DCI("ListenerIsAlive"))
                    )
                End If

                'CardResult_KeepListenerAlive = NetConn.RequestCardInformation()
                CardResult_KeepListenerAlive = NetConn.RequestCardInformation_KeepListenerAlive()

            ElseIf DCI("TipoConexion") = 2 Then
                CardResult_KeepListenerAlive = SerialConn.RequestCardInformation()
            End If

            If Not IsNothing(CardResult_KeepListenerAlive) Then

                If IsNothing(CardResult_KeepListenerAlive.Host) Then CardResult_KeepListenerAlive.Host = CardDto.Hosts.Undefined
                If IsNothing(CardResult_KeepListenerAlive.AccountNumber) Then CardResult_KeepListenerAlive.AccountNumber = String.Empty
                If IsNothing(CardResult_KeepListenerAlive.CardHolderName) Then CardResult_KeepListenerAlive.CardHolderName = String.Empty
                CardResult_KeepListenerAlive.CardHolderName = System.Text.RegularExpressions.Regex.Replace(CardResult_KeepListenerAlive.CardHolderName, "[^a-zA-Z0-9_ .\-]", String.Empty)
                If IsNothing(CardResult_KeepListenerAlive.CardProduct) Then CardResult_KeepListenerAlive.CardProduct = CardDto.CardProducts.Visa
                If IsNothing(CardResult_KeepListenerAlive.ServiceCode) Then CardResult_KeepListenerAlive.ServiceCode = 0
                If IsNothing(CardResult_KeepListenerAlive.TransactionMode) Then CardResult_KeepListenerAlive.TransactionMode = String.Empty

            End If

        End If

    End Function

    Public Sub SecureTransactionResult(ByRef TransResult As TransactionDto)

        If IsNothing(TransResult.Host) Then TransResult.Host = CardDto.Hosts.Undefined
        If IsNothing(TransResult.CardProduct) Then TransResult.CardProduct = CardDto.CardProducts.Visa
        If IsNothing(TransResult.TransactionMode) Then TransResult.TransactionMode = String.Empty
        If IsNothing(TransResult.AccountNumber) Then TransResult.AccountNumber = String.Empty
        If IsNothing(TransResult.BatchNumber) Then TransResult.BatchNumber = 0
        If IsNothing(TransResult.ReferenceNumber) Then TransResult.ReferenceNumber = 0
        If IsNothing(TransResult.TransactionDateTime) Then TransResult.TransactionDateTime = Now
        If IsNothing(TransResult.CardHolderName) Then TransResult.CardHolderName = String.Empty
        TransResult.CardHolderName = System.Text.RegularExpressions.Regex.Replace(TransResult.CardHolderName, "[^a-zA-Z0-9_ .\-]", String.Empty)
        If IsNothing(TransResult.AuthorizationNumber) Then TransResult.AuthorizationNumber = "0"
        If IsNothing(TransResult.TerminalID) Then TransResult.TerminalID = "0"
        If IsNothing(TransResult.RetrievalReferenceNumber) Then TransResult.RetrievalReferenceNumber = 0
        If IsNothing(TransResult.MerchantID) Then TransResult.MerchantID = 0
        If IsNothing(TransResult.LoyaltyDeferredNumber) Then TransResult.LoyaltyDeferredNumber = String.Empty
        If IsNothing(TransResult.TransactionID) Then TransResult.TransactionID = String.Empty
        If IsNothing(TransResult.ApplicationIdentifier) Then TransResult.ApplicationIdentifier = String.Empty
        TransResult.ApplicationIdentifier = System.Text.RegularExpressions.Regex.Replace(TransResult.ApplicationIdentifier, "[^a-zA-Z0-9_ .\-]", String.Empty)
        If IsNothing(TransResult.Reserved) Then TransResult.Reserved = String.Empty
        If IsNothing(TransResult.Signature) Then TransResult.Signature = New Byte() {}
        If IsNothing(TransResult.ApplicationVersion) Then TransResult.ApplicationVersion = String.Empty
        TransResult.ApplicationVersion = System.Text.RegularExpressions.Regex.Replace(TransResult.ApplicationVersion, "[^a-zA-Z0-9_ .\-]", String.Empty)
        If IsNothing(TransResult.SaleResultTypeIndicator) Then TransResult.SaleResultTypeIndicator = String.Empty
        If IsNothing(TransResult.DCCAccepted) Then TransResult.DCCAccepted = "N"
        If IsNothing(TransResult.DCCMarginPercentage) Then TransResult.DCCMarginPercentage = 0
        If IsNothing(TransResult.DCCTransactionAmount) Then TransResult.DCCTransactionAmount = 0
        If IsNothing(TransResult.DCCDisplayRate) Then TransResult.DCCDisplayRate = 1
        If IsNothing(TransResult.DCCCurrencyIdentifier) Then TransResult.DCCCurrencyIdentifier = String.Empty
        If IsNothing(TransResult.PromonetProgramID) Then TransResult.PromonetProgramID = String.Empty
        If IsNothing(TransResult.PromonetProgramName) Then TransResult.PromonetProgramName = String.Empty
        If IsNothing(TransResult.PromonetMessage) Then TransResult.PromonetMessage = String.Empty
        If IsNothing(TransResult.PromonetQRCode) Then TransResult.PromonetQRCode = String.Empty
        If IsNothing(TransResult.DigitalSignaturePerformed) Then TransResult.DigitalSignaturePerformed = "N"
        If IsNothing(TransResult.ResponseMessage) Then TransResult.ResponseMessage = "N/A"
        TransResult.ResponseMessage = System.Text.RegularExpressions.Regex.Replace(TransResult.ResponseMessage, "[^a-zA-Z0-9_/ .\-]", String.Empty)
        If IsNothing(TransResult.SignatureModeIndicator) Then TransResult.SignatureModeIndicator = "0"
        If IsNothing(TransResult.FormattedExpirationDate) Then TransResult.FormattedExpirationDate = "00/00"
        If IsNothing(TransResult.VoucherMode) Then TransResult.VoucherMode = "4"
        If IsNothing(TransResult.EMVApplicationLevel) Then TransResult.EMVApplicationLevel = "CARDNET"
        TransResult.EMVApplicationLevel = System.Text.RegularExpressions.Regex.Replace(TransResult.EMVApplicationLevel, "[^a-zA-Z0-9_ .\-]", String.Empty)
        If IsNothing(TransResult.QuickPaymentIndicator) Then TransResult.QuickPaymentIndicator = "0"
        If IsNothing(TransResult.PINModeIndicator) Then TransResult.PINModeIndicator = "0"
        If IsNothing(TransResult.MultiAcquirerID) Then TransResult.MultiAcquirerID = 0
        If IsNothing(TransResult.MultiMerchantID) Then TransResult.MultiMerchantID = 0
        If IsNothing(TransResult.InvoiceNumber) Then TransResult.InvoiceNumber = 0
        If IsNothing(TransResult.Folio) Then TransResult.Folio = 1

    End Sub

    Public Function TransactionResult(ByVal TransData As TransactionDto, ByVal TransType As TransactionDto.TransactionTypes) As TransactionDto

        DCI("TmpTransactionResultRepresentation") = String.Empty

        Dim TransResult As New TransactionDto

        If DCI("TrainingMode") Then

            Dim Lucky As Int32

            Lucky = RandomInt32(100)

            Select Case Lucky

                Case 1 To 10

                    Return Nothing

                Case 11 To 30

                    Throw New Exception("Rechazada / Fallo de Comunicacion / Falta de fondos / Cancelada por el usuario / etc.")

                Case Else '31 To 100

                    Dim TmpResp As New TransactionDto() With {
                        .Host = IIf(RandomInt32(100) > 50, CardDto.Hosts.Credit, CardDto.Hosts.Debit),
                        .CardProduct = IIf(RandomInt32(100) > 50, CardDto.CardProducts.MC, CardDto.CardProducts.Visa),
                        .TransactionMode = IIf(RandomInt32(100) > 50, "D@5", "C@5"),
                        .AccountNumber = IIf(RandomInt32(100) > 50, "154689******4578", "123456******3456"),
                        .BatchNumber = IIf(RandomInt32(100) > 50, 101, 102),
                        .ReferenceNumber = IIf(RandomInt32(100) > 50, 123, 234),
                        .TransactionDateTime = DateTime.Now,
                        .CardHolderName = IIf(RandomInt32(100) > 50, "CARDHOLDER 1", "CARDHOLDER 2"),
                        .AuthorizationNumber = RandomInt32(400000, 300000).ToString(),
                        .TerminalID = "80001234",
                        .RetrievalReferenceNumber = RandomInt64(999999999999, 101010101010),
                        .MerchantID = "10203040",
                        .LoyaltyDeferredNumber = IIf(TransType = TransactionDto.TransactionTypes.SaleInQuotas,
                         "CU" + FormatNumber(TransData.NumberOfShares, "00"),
                         IIf(RandomInt32(100) > 50, "Q00", "")),
                        .TransactionID = RandomInt64(99999, 10000),
                        .ApplicationIdentifier = "154541531521131",
                        .Reserved = String.Empty,
                        .ApplicationVersion = "1.0.1",
                        .ResponseMessage = "APROBADA",
                        .SignatureModeIndicator = IIf(Lucky > 90, "1", "2"),
                        .DigitalSignaturePerformed = IIf(Lucky > 90, "Y", "N"),
                        .FormattedExpirationDate = "00/00",
                        .VoucherMode = IIf(TransType = TransactionDto.TransactionTypes.TPagoSale, "4", "6"),
                        .EMVApplicationLevel = "CARDNET",
                        .QuickPaymentIndicator = IIf(TransData.TransactionAmount <= 2000, "1", "0"),
                        .PINModeIndicator = "2",
                        .InvoiceNumber = TransData.InvoiceNumber,
                        .Folio = TransData.Folio,
                        .MultiMerchantID = TransData.MultiMerchantID,
                        .MultiAcquirerID = TransData.MultiAcquirerID,
                        .TransactionOtherTaxes = TransData.TransactionOtherTaxes
                    }

                    Lucky = RandomInt32(100)

                    Select Case Lucky
                        Case 1 To 10
                            With TmpResp
                                .SaleResultTypeIndicator = "DC"
                                .DCCAccepted = "Y"
                                .DCCMarginPercentage = 4.0
                                .DCCTransactionAmount = (TransData.TransactionAmount / 58.45)
                                .DCCDisplayRate = 58.45
                                .DCCCurrencyIdentifier = "USD"
                            End With
                        Case 11 To 20
                            With TmpResp
                                .SaleResultTypeIndicator = "BE"
                            End With
                        Case 21 To 30
                            With TmpResp
                                .SaleResultTypeIndicator = "PR"
                                .PromonetProgramID = "ABCD"
                                .PromonetProgramName = "NOMBRE PROGRAMA"
                                .PromonetMessage = "USTED HA GANADO..."
                                .PromonetQRCode = "... PROMONET QR ..."
                            End With
                        Case Else
                            With TmpResp
                                .SaleResultTypeIndicator = "00"
                            End With
                    End Select

                    Try

                        Dim Dumper As System.IO.StringWriter
                        Dumper = New System.IO.StringWriter()

                        ObjectDumper.Dumper.Dump(TmpResp, "Transaction Result", Dumper)

                        DCI("TmpTransactionResultRepresentation") = Dumper.ToString()

                    Catch ex As Exception

                        If DebugMode Then

                            VB6Aux.ShowBigDebugMessage("Error Parsing TransResult " + vbNewLine + ex.Message + vbNewLine +
                            ex.Source + vbNewLine + ex.HResult.ToString + vbNewLine + ex.HelpLink + vbNewLine + vbNewLine + ex.StackTrace)

                        End If

                        DCI("TmpTransactionResultRepresentation") = String.Empty

                    End Try

                    ' Intentar almacenar siempre un log de la transaccion, independientemente de si el debug mode esta activado.
                    If (Not String.IsNullOrEmpty(DCI("TmpTransactionResultRepresentation"))) Then

                        VB6Aux.SaveQuickLogFile(DCI("TmpTransactionResultRepresentation"), "$(CurDrive)\CardnetRDLogBackup\",
                        Microsoft.VisualBasic.Strings.Format(DateTime.Now, "YYYY_MM_DD_HH_NN_SS") & "_" &
                        DCI("TipoOperacion") & "_" + DCI("InvoiceNo") & "_" & (DCI("InternalAppSequenceNo") + 1).ToString & ".log")

                        If DebugMode Then

                            'VB6Aux.ShowBigDebugMessage("Transaction Returned: " + TransResult.ToString() + "\n\n" + DCI("TmpTransactionResultRepresentation"))

                            VB6Aux.SaveQuickLogFile(DCI("TmpTransactionResultRepresentation"), "$(AppPath)", "InterfazCardnetRD.log")

                        End If

                    End If

                    SecureTransactionResult(TmpResp)

                    Return TmpResp

            End Select

        Else

            If DCI("TipoConexion") = 1 Then

                If DebugMode Then
                    MsgBox("NetConn.AuthorizeTransaction(TransType, TransData)" + vbNewLine + vbNewLine +
                    "TransType: " + TransType.ToString + vbNewLine +
                    "ITBIS: " + TransData.TransactionITBIS.ToString + vbNewLine +
                    "TransactionAmount: " + TransData.TransactionAmount.ToString + vbNewLine +
                    "MerchantID: " + TransData.MerchantID.ToString + vbNewLine +
                    "MultiMerchantID: " + TransData.MultiMerchantID.ToString + vbNewLine +
                    "InvoiceNumber: " + TransData.InvoiceNumber.ToString + vbNewLine + vbNewLine +
                    "**Internal Use** - ListenerIsAlive: " + Convert.ToString(DCI("ListenerIsAlive"))
                    )
                End If

                'TransResult = NetConn.AuthorizeTransaction(TransType, TransData)
                TransResult = NetConn.AuthorizeTransaction(TransType, TransData,
                Convert.ToBoolean(DCI("ListenerIsAlive")))

            ElseIf DCI("TipoConexion") = 2 Then

                If DebugMode Then
                    MsgBox("SerialConn.AuthorizeTransaction(TransType, TransData)" + vbNewLine + vbNewLine +
                    "TransType: " + TransType.ToString + vbNewLine +
                    "ITBIS: " + TransData.TransactionITBIS.ToString + vbNewLine +
                    "TransactionAmount: " + TransData.TransactionAmount.ToString + vbNewLine +
                    "MerchantID: " + TransData.MerchantID.ToString + vbNewLine +
                    "MultiMerchantID: " + TransData.MultiMerchantID.ToString + vbNewLine +
                    "InvoiceNumber: " + TransData.InvoiceNumber.ToString + vbNewLine
                    )
                End If

                TransResult = SerialConn.AuthorizeTransaction(TransType, TransData)

            End If

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage("TransResult Received ? " + (Not IsNothing(TransResult)).ToString)
            End If

            Try

                Dim Dumper As System.IO.StringWriter
                Dumper = New System.IO.StringWriter()

                ObjectDumper.Dumper.Dump(TransResult, "Transaction Result", Dumper)

                DCI("TmpTransactionResultRepresentation") = Dumper.ToString()

            Catch ex As Exception

                If DebugMode Then

                    VB6Aux.ShowBigDebugMessage("Error Parsing TransResult " + vbNewLine + ex.Message + vbNewLine +
                    ex.Source + vbNewLine + ex.HResult.ToString + vbNewLine + ex.HelpLink + vbNewLine + vbNewLine + ex.StackTrace)

                End If

                DCI("TmpTransactionResultRepresentation") = String.Empty

            End Try

            ' Intentar almacenar siempre un log de la transaccion, independientemente de si el debug mode esta activado.

            If (Not String.IsNullOrEmpty(DCI("TmpTransactionResultRepresentation"))) Then

                VB6Aux.SaveQuickLogFile(DCI("TmpTransactionResultRepresentation"), "$(CurDrive)\CardnetRDLogBackup\",
                Microsoft.VisualBasic.Strings.Format(DateTime.Now, "YYYY_MM_DD_HH_NN_SS") & "_" &
                DCI("TipoOperacion") & "_" + DCI("InvoiceNo") & "_" & (DCI("InternalAppSequenceNo") + 1).ToString & ".log")

                If DebugMode Then

                    'VB6Aux.ShowBigDebugMessage("Transaction Returned: " + TransResult.ToString() + "\n\n" + DCI("TmpTransactionResultRepresentation"))

                    VB6Aux.SaveQuickLogFile(DCI("TmpTransactionResultRepresentation"), "$(AppPath)", "InterfazCardnetRD.log")

                End If

            End If

        End If

        SecureTransactionResult(TransResult)

        TransactionResult = TransResult

        Return TransactionResult

    End Function

    Private Sub EMVGetCardInfo()

        'If Not DCI.Exists("RespData") Then
        DRI = VB6Aux.GetMeADictionary
        DCI("RespData") = DRI
        'Else
        'DRI = DCI("RespData")
        'End If

        DRI("TransactionResultError") = False

        Try

            Dim CardData As CardDto

            CardData = CardResult()

            If IsNothing(CardData) Then
                Throw New Exception("Transacción sin respuesta.")
            End If

            'If TransData.ReferenceNumber = 0 Then
            'DRI("TransactionSuccess") = False
            'Else
            DRI("TransactionSuccess") = (CType(CardData.Host, Int32) <> 99)
            'End If

            DRI("HostID") = CardData.Host
            DRI("HostDescription") = GetHostDescription(CardData.Host)

            DRI("CardProduct") = GetCardProduct(CardData.CardProduct)
            DRI("CardProductID") = Convert.ToInt32(CardData.CardProduct).ToString

            DRI("TransactionModeID") = CardData.TransactionMode
            DRI("TransactionMode") = GetTransactionMode(DRI("TransactionModeID").ToString)

            DRI("AccountNumber") = CardData.AccountNumber
            DRI("ServiceCode") = CardData.ServiceCode
            DRI("CardHolderName") = CardData.CardHolderName

            DRI("ListenerIsAlive") = False

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<Transaction>"

            TXML += "<Request>"

            TXML += "<ConsortiumID>10</ConsortiumID>"
            TXML += "<ConsortiumName>CardNET RD</ConsortiumName>"

            TXML += "<OperationType>" + DCI("TipoOperacion").ToString + "</OperationType>"
            TXML += "<TranCode>" + DCI("TranCode").ToString + "</TranCode>"

            TXML += "</Request>"

            TXML += "<Response>"

            TXML += "<HostID>" + DRI("HostID").ToString + "</HostID>"
            TXML += "<HostDescription>" + DRI("HostDescription").ToString + "</HostDescription>"
            TXML += "<CardProductID>" + DRI("CardProductID").ToString + "</CardProductID>"
            TXML += "<CardProduct>" + DRI("CardProduct").ToString + "</CardProduct>"
            TXML += "<TransactionModeID>" + DRI("TransactionModeID").ToString + "</TransactionModeID>"
            TXML += "<TransactionMode>" + DRI("TransactionMode").ToString + "</TransactionMode>"
            TXML += "<AccountNumber>" + DRI("AccountNumber").ToString + "</AccountNumber>"
            TXML += "<ServiceCode>" + DRI("ServiceCode").ToString + "</ServiceCode>"
            TXML += "<CardHolderName>" + DRI("CardHolderName").ToString + "</CardHolderName>"

            TXML += "</Response>"

            TXML += "</Transaction>"

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            Dim RespXML As String = TXML
            Dim FormattedRespXml = GetFormattedXml(RespXML)

            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXml

        Catch Any As Exception

            DRI("TransactionSuccess") = False

            DRI("TransactionResultError") = True
            DRI("TransactionResultError_Code") = Any.HResult
            DRI("TransactionResultError_Desc") = Any.Message
            DRI("TransactionResultError_Source") = Any.Source
            DRI("TransactionResultError_StackTrace") = Any.StackTrace

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<Transaction>"

            TXML += "<Error>" + "True" + "</Error>"
            TXML += "<Code>" + DRI("TransactionResultError_Code").ToString + "</Code>"
            TXML += "<Desc><![CDATA[" + DRI("TransactionResultError_Desc").ToString + "]]></Desc>"
            TXML += "<Source><![CDATA[" + DRI("TransactionResultError_Source").ToString + "]]></Source>"
            TXML += "<StackTrace><![CDATA[" + DRI("TransactionResultError_StackTrace").ToString + "]]></StackTrace>"

            TXML += "</Transaction>"

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            Dim RespXML As String = TXML
            Dim FormattedRespXml = GetFormattedXml(RespXML)

            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXml

            DRI("ListenerIsAlive") = False

        End Try

    End Sub

    Private Sub EMVGetCardInfo_KeepListenerAlive()

        'If Not DCI.Exists("RespData") Then
        DRI = VB6Aux.GetMeADictionary
        DCI("RespData") = DRI
        'Else
        'DRI = DCI("RespData")
        'End If

        DRI("TransactionResultError") = False

        Try

            Dim CardData As CardDto

            CardData = CardResult_KeepListenerAlive()

            If IsNothing(CardData) Then
                CloseListener()
                Throw New Exception("Transacción sin respuesta.")
            End If

            'If TransData.ReferenceNumber = 0 Then
            'DRI("TransactionSuccess") = False
            'Else
            DRI("TransactionSuccess") = (CType(CardData.Host, Int32) <> 99)
            'End If

            DRI("HostID") = CardData.Host
            DRI("HostDescription") = GetHostDescription(CardData.Host)

            DRI("CardProduct") = GetCardProduct(CardData.CardProduct)
            DRI("CardProductID") = Convert.ToInt32(CardData.CardProduct).ToString

            DRI("TransactionModeID") = CardData.TransactionMode
            DRI("TransactionMode") = GetTransactionMode(DRI("TransactionModeID").ToString)

            DRI("AccountNumber") = CardData.AccountNumber
            DRI("ServiceCode") = CardData.ServiceCode
            DRI("CardHolderName") = CardData.CardHolderName

            If (DRI("AccountNumber").ToString().Trim() <> String.Empty) Then
                DRI("ListenerIsAlive") = True
            End If

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<Transaction>"

            TXML += "<Request>"

            TXML += "<ConsortiumID>10</ConsortiumID>"
            TXML += "<ConsortiumName>CardNET RD</ConsortiumName>"

            TXML += "<OperationType>" + DCI("TipoOperacion").ToString + "</OperationType>"
            TXML += "<TranCode>" + DCI("TranCode").ToString + "</TranCode>"

            TXML += "</Request>"

            TXML += "<Response>"

            TXML += "<HostID>" + DRI("HostID").ToString + "</HostID>"
            TXML += "<HostDescription>" + DRI("HostDescription").ToString + "</HostDescription>"
            TXML += "<CardProductID>" + DRI("CardProductID").ToString + "</CardProductID>"
            TXML += "<CardProduct>" + DRI("CardProduct").ToString + "</CardProduct>"
            TXML += "<TransactionModeID>" + DRI("TransactionModeID").ToString + "</TransactionModeID>"
            TXML += "<TransactionMode>" + DRI("TransactionMode").ToString + "</TransactionMode>"
            TXML += "<AccountNumber>" + DRI("AccountNumber").ToString + "</AccountNumber>"
            TXML += "<ServiceCode>" + DRI("ServiceCode").ToString + "</ServiceCode>"
            TXML += "<CardHolderName>" + DRI("CardHolderName").ToString + "</CardHolderName>"

            TXML += "</Response>"

            TXML += "</Transaction>"

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            Dim RespXML As String = TXML
            Dim FormattedRespXml = GetFormattedXml(RespXML)

            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXml

        Catch Any As Exception

            DRI("TransactionSuccess") = False

            DRI("TransactionResultError") = True
            DRI("TransactionResultError_Code") = Any.HResult
            DRI("TransactionResultError_Desc") = Any.Message
            DRI("TransactionResultError_Source") = Any.Source
            DRI("TransactionResultError_StackTrace") = Any.StackTrace

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<Transaction>"

            TXML += "<Error>" + "True" + "</Error>"
            TXML += "<Code>" + DRI("TransactionResultError_Code").ToString + "</Code>"
            TXML += "<Desc><![CDATA[" + DRI("TransactionResultError_Desc").ToString + "]]></Desc>"
            TXML += "<Source><![CDATA[" + DRI("TransactionResultError_Source").ToString + "]]></Source>"
            TXML += "<StackTrace><![CDATA[" + DRI("TransactionResultError_StackTrace").ToString + "]]></StackTrace>"

            TXML += "</Transaction>"

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            Dim RespXML As String = TXML
            Dim FormattedRespXml = GetFormattedXml(RespXML)

            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXml

            CloseListener()
            DRI("ListenerIsAlive") = False

        End Try

    End Sub

    Private Sub EMVSale()

        'If DebugMode Then
        'MsgBox("EMVSale()")
        'End If

        'If Not DCI.Exists("RespData") Then
        DRI = VB6Aux.GetMeADictionary
        DCI("RespData") = DRI
        'Else
        'DRI = DCI("RespData")
        'End If

        DRI("TransactionResultError") = False

        Try

            Dim TranType As TransactionDto.TransactionTypes = TransactionDto.TransactionTypes.NormalSale

            Dim TransData As TransactionDto

            Dim TransDataRequest As TransactionDto = New TransactionDto() With {
                .MultiMerchantID = CByte(IIf(String.IsNullOrEmpty(DCI("MultiMerchantID").ToString), 0, DCI("MultiMerchantID").ToString)),
                .MultiAcquirerID = CByte(IIf(String.IsNullOrEmpty(DCI("MultiAdquirienteID").ToString), 0, DCI("MultiAdquirienteID").ToString)),
                .Folio = 1, .CheckInDate = Now, .CheckOutDate = Now,
                .TransactionAmount = CDec(DCI("TotalTransactionAmount")),
                .TransactionITBIS = CDec(DCI("PaymentTaxAmount")),
                .TransactionOtherTaxes = CDec(DCI("OtherTaxesAmount")),
                .InvoiceNumber = Right(DCI("InvoiceNo").ToString, 6)
            }

            If (DCI("TranCode") = "CU00") Then
                TranType = TransactionDto.TransactionTypes.SaleInQuotas
                TransDataRequest.NumberOfShares = CType(DCI("NumCuotas"), Decimal)
            End If

            If (DCI("TranCode") = "TP00") Then
                TranType = TransactionDto.TransactionTypes.TPagoSale
            End If

            If (DCI("TranCode") = "PU00") Then
                TranType = TransactionDto.TransactionTypes.Loyalty
            End If

            TransDataRequest.TransactionType = TranType

            TransData = TransactionResult(TransDataRequest, TransDataRequest.TransactionType)

            If IsNothing(TransData) Then
                Throw New Exception("Transacción sin respuesta.")
            End If

            'If DebugMode Then
            '    VB6Aux.ShowBigDebugMessage("TransData Is Live ")
            'End If

            'If TransData.ReferenceNumber = 0 Then
            'DRI("TransactionSuccess") = False
            'Else
            DRI("TransactionSuccess") = True
            'End If

            DRI("HostID") = TransData.Host
            DRI("HostDescription") = GetHostDescription(TransData.Host)

            DRI("CardProduct") = GetCardProduct(TransData.CardProduct)
            DRI("CardProductID") = Convert.ToInt32(TransData.CardProduct).ToString

            DRI("TransactionModeID") = TransData.TransactionMode
            DRI("TransactionMode") = GetTransactionMode(DRI("TransactionModeID").ToString)

            DRI("AccountNumber") = TransData.AccountNumber
            DRI("BatchNumber") = TransData.BatchNumber
            DRI("ReferenceNumber") = TransData.ReferenceNumber
            DRI("TransactionDateTime") = TransData.TransactionDateTime
            DRI("CardHolderName") = TransData.CardHolderName
            DRI("AuthorizationNumber") = TransData.AuthorizationNumber
            DRI("TerminalID") = TransData.TerminalID
            DRI("RetrievalReferenceNumber") = TransData.RetrievalReferenceNumber
            DRI("MerchantID") = TransData.MerchantID
            DRI("LoyaltyDeferredNumber") = TransData.LoyaltyDeferredNumber
            DRI("TransactionID") = TransData.TransactionID
            DRI("ApplicationIdentifier") = TransData.ApplicationIdentifier
            DRI("Reserved") = TransData.Reserved
            DRI("Signature") = TransData.Signature
            DRI("ApplicationVersion") = TransData.ApplicationVersion

            DRI("SaleResultTypeIndicator") = TransData.SaleResultTypeIndicator.ToUpper()

            'If DebugMode Then
            '    VB6Aux.ShowBigDebugMessage("TransData CheckPoint 1 ")
            'End If

            If (DRI("SaleResultTypeIndicator") = ("DC")) Then

                DRI("DCCAccepted") = TransData.DCCAccepted
                DRI("DCCMarginPercentage") = TransData.DCCMarginPercentage
                DRI("DCCTransactionAmount") = TransData.DCCTransactionAmount
                DRI("DCCDisplayRate") = TransData.DCCDisplayRate
                DRI("DCCCurrencyIdentifier") = TransData.DCCCurrencyIdentifier

            ElseIf (DRI("SaleResultTypeIndicator") = ("PR") Or DRI("SaleResultTypeIndicator") = ("PM")) Then

                DRI("PromonetProgramID") = TransData.PromonetProgramID
                DRI("PromonetProgramName") = TransData.PromonetProgramName
                DRI("PromonetMessage") = TransData.PromonetMessage
                DRI("PromonetQRCode") = TransData.PromonetQRCode

            End If

            'If DebugMode Then
            '    VB6Aux.ShowBigDebugMessage("TransData CheckPoint 2 ")
            'End If

            DRI("DigitalSignaturePerformed") = TransData.DigitalSignaturePerformed
            DRI("ResponseMessage") = TransData.ResponseMessage
            DRI("SignatureModeIndicator") = TransData.SignatureModeIndicator
            DRI("FormattedExpirationDate") = TransData.FormattedExpirationDate
            DRI("VoucherMode") = TransData.VoucherMode
            DRI("EMVApplicationLevel") = TransData.EMVApplicationLevel
            DRI("QuickPaymentIndicator") = TransData.QuickPaymentIndicator
            DRI("PINModeIndicator") = TransData.PINModeIndicator

            'If DebugMode Then
            '    VB6Aux.ShowBigDebugMessage("TransData CheckPoint 3 ")
            'End If

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<Transaction>"

            TXML += "<Request>"

            TXML += "<ConsortiumID>10</ConsortiumID>"
            TXML += "<ConsortiumName>CardNET RD</ConsortiumName>"

            TXML += "<OperationType>" + DCI("TipoOperacion").ToString + "</OperationType>"
            TXML += "<TranCode>" + DCI("TranCode").ToString + "</TranCode>"
            TXML += "<TranEnumType>" + Convert.ToInt32(TranType).ToString + "</TranEnumType>"
            TXML += "<MultiMerchantID>" + TransData.MultiMerchantID.ToString + "</MultiMerchantID>"
            TXML += "<MultiAcquirerID>" + TransData.MultiAcquirerID.ToString + "</MultiAcquirerID>"
            TXML += "<InvoiceNumber>" + TransData.InvoiceNumber.ToString + "</InvoiceNumber>"
            TXML += "<Folio>" + TransData.Folio.ToString + "</Folio>"
            TXML += "<PurchaseTaxableAmount>" + DCI("PurchaseTaxableAmount").ToString + "</PurchaseTaxableAmount>"
            TXML += "<PurchaseTaxAmount>" + DCI("PurchaseTaxAmount").ToString + "</PurchaseTaxAmount>"
            TXML += "<PurchaseAmount>" + DCI("PurchaseAmount").ToString + "</PurchaseAmount>"
            TXML += "<PaymentTaxableAmount>" + DCI("PaymentTaxableAmount").ToString + "</PaymentTaxableAmount>"
            TXML += "<PaymentTaxAmount>" + DCI("PaymentTaxAmount").ToString + "</PaymentTaxAmount>"
            TXML += "<PaymentAmount>" + DCI("PaymentAmount").ToString + "</PaymentAmount>"
            TXML += "<CashbackAmount>" + DCI("CashbackAmount").ToString + "</CashbackAmount>"
            TXML += "<OtherTaxesAmount>" + DCI("OtherTaxesAmount").ToString + "</OtherTaxesAmount>"
            TXML += "<TotalTransactionAmount>" + DCI("TotalTransactionAmount").ToString + "</TotalTransactionAmount>"

            'If DebugMode Then
            '    VB6Aux.ShowBigDebugMessage("TXML CheckPoint 1 ")
            'End If

            If DCI("DctoxBIN_Apply") Then
                TXML += "<DctoxBIN_Bin>" + DCI("DctoxBIN_Bin").ToString + "</DctoxBIN_Bin>"
                TXML += "<DctoxBIN_IsPercent>" + IIf(DCI("DctoxBIN_IsPercent"), "1", "0") + "</DctoxBIN_IsPercent>"
                TXML += "<DctoxBIN_Value>" + DCI("DctoxBIN_Value").ToString + "</DctoxBIN_Value>"
                TXML += "<BINDiscountAmount>" + DCI("BINDiscountAmount").ToString + "</BINDiscountAmount>"
            End If

            'If DebugMode Then
            '    VB6Aux.ShowBigDebugMessage("TXML CheckPoint 2 ")
            'End If

            TXML += "</Request>"

            TXML += "<Response>"

            TXML += "<HostID>" + DRI("HostID").ToString + "</HostID>"
            TXML += "<HostDescription>" + DRI("HostDescription").ToString + "</HostDescription>"
            TXML += "<CardProductID>" + DRI("CardProductID").ToString + "</CardProductID>"
            TXML += "<CardProduct>" + DRI("CardProduct").ToString + "</CardProduct>"
            TXML += "<TransactionModeID>" + DRI("TransactionModeID").ToString + "</TransactionModeID>"
            TXML += "<TransactionMode>" + DRI("TransactionMode").ToString + "</TransactionMode>"
            TXML += "<AccountNumber>" + DRI("AccountNumber").ToString + "</AccountNumber>"
            TXML += "<BatchNumber>" + DRI("BatchNumber").ToString + "</BatchNumber>"
            TXML += "<ReferenceNumber>" + DRI("ReferenceNumber").ToString + "</ReferenceNumber>"
            TXML += "<TransactionDateTime>" + EncodeISOFullDate(CType(DRI("TransactionDateTime"), Date)) + "</TransactionDateTime>"
            TXML += "<CardHolderName>" + DRI("CardHolderName").ToString + "</CardHolderName>"
            TXML += "<AuthorizationNumber>" + DRI("AuthorizationNumber").ToString + "</AuthorizationNumber>"
            TXML += "<TerminalID>" + DRI("TerminalID").ToString + "</TerminalID>"
            TXML += "<RetrievalReferenceNumber>" + DRI("RetrievalReferenceNumber").ToString + "</RetrievalReferenceNumber>"
            TXML += "<LoyaltyDeferredNumber>" + DRI("LoyaltyDeferredNumber").ToString + "</LoyaltyDeferredNumber>"
            TXML += "<TransactionID>" + DRI("TransactionID").ToString + "</TransactionID>"
            TXML += "<ApplicationIdentifier>" + DRI("ApplicationIdentifier").ToString + "</ApplicationIdentifier>"
            TXML += "<Reserved><![CDATA[" + DRI("Reserved").ToString + "]]></Reserved>"
            TXML += "<Signature>" + String.Join(" ", TransData.Signature) + "</Signature>"
            TXML += "<ApplicationVersion>" + DRI("ApplicationVersion").ToString + "</ApplicationVersion>"

            TXML += "<SaleResultTypeIndicator>" + DRI("SaleResultTypeIndicator").ToString + "</SaleResultTypeIndicator>"

            'If DebugMode Then
            '    VB6Aux.ShowBigDebugMessage("TXML CheckPoint 3 ")
            'End If

            If (DRI("SaleResultTypeIndicator") = ("DC")) Then

                TXML += "<DCCAccepted>" + DRI("DCCAccepted").ToString + "</DCCAccepted>"
                TXML += "<DCCMarginPercentage>" + DRI("DCCMarginPercentage").ToString + "</DCCMarginPercentage>"
                TXML += "<DCCTransactionAmount>" + DRI("DCCTransactionAmount").ToString + "</DCCTransactionAmount>"
                TXML += "<DCCDisplayRate>" + DRI("DCCDisplayRate").ToString + "</DCCDisplayRate>"
                TXML += "<DCCCurrencyIdentifier>" + DRI("DCCCurrencyIdentifier").ToString + "</DCCCurrencyIdentifier>"

            ElseIf (DRI("SaleResultTypeIndicator") = ("PR") Or DRI("SaleResultTypeIndicator") = ("PM")) Then

                TXML += "<PromonetProgramID><![CDATA[" + DRI("PromonetProgramID").ToString + "]]></PromonetProgramID>"
                TXML += "<PromonetProgramName><![CDATA[" + DRI("PromonetProgramName").ToString + "]]></PromonetProgramName>"
                TXML += "<PromonetMessage><![CDATA[" + DRI("PromonetMessage").ToString + "]]></PromonetMessage>"
                TXML += "<PromonetQRCode><![CDATA[" + DRI("PromonetQRCode").ToString + "]]></PromonetQRCode>"

            End If

            'If DebugMode Then
            '    VB6Aux.ShowBigDebugMessage("TXML CheckPoint 4 ")
            'End If

            TXML += "<DigitalSignaturePerformed>" + DRI("DigitalSignaturePerformed").ToString + "</DigitalSignaturePerformed>"
            TXML += "<ResponseMessage>" + DRI("ResponseMessage").ToString + "</ResponseMessage>"
            TXML += "<SignatureModeIndicator>" + DRI("SignatureModeIndicator").ToString + "</SignatureModeIndicator>"
            TXML += "<FormattedExpirationDate>" + DRI("FormattedExpirationDate").ToString + "</FormattedExpirationDate>"
            TXML += "<VoucherMode>" + DRI("VoucherMode").ToString + "</VoucherMode>"
            TXML += "<EMVApplicationLevel>" + DRI("EMVApplicationLevel").ToString + "</EMVApplicationLevel>"
            TXML += "<QuickPaymentIndicator>" + DRI("QuickPaymentIndicator").ToString + "</QuickPaymentIndicator>"
            TXML += "<PINModeIndicator>" + DRI("PINModeIndicator").ToString + "</PINModeIndicator>"

            TXML += "</Response>"

            TXML += "</Transaction>"

            If DebugMode Then
                VB6Aux.ShowBigDebugMessage("Creating XML " + vbNewLine + vbNewLine + TXML)
            End If

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            Dim RespXML As String = TXML
            Dim FormattedRespXml = GetFormattedXml(RespXML)

            'If DebugMode Then
            '    VB6Aux.ShowBigDebugMessage("TXML CheckPoint 5 ")
            'End If

            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXml

            DRI("SequenceNo") = "00000001"
            DCI("SequenceNo") = DRI("SequenceNo")

        Catch Any As Exception

            DRI("TransactionSuccess") = False

            DRI("TransactionResultError") = True
            DRI("TransactionResultError_Code") = Any.HResult
            DRI("TransactionResultError_Desc") = Any.Message
            DRI("TransactionResultError_Source") = Any.Source
            DRI("TransactionResultError_StackTrace") = Any.StackTrace

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<Transaction>"

            TXML += "<Error>" + "True" + "</Error>"
            TXML += "<Code>" + DRI("TransactionResultError_Code").ToString + "</Code>"
            TXML += "<Desc><![CDATA[" + DRI("TransactionResultError_Desc").ToString + "]]></Desc>"
            TXML += "<Source><![CDATA[" + DRI("TransactionResultError_Source").ToString + "]]></Source>"
            TXML += "<StackTrace><![CDATA[" + DRI("TransactionResultError_StackTrace").ToString + "]]></StackTrace>"

            TXML += "</Transaction>"

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            Dim RespXML As String = TXML
            Dim FormattedRespXml = GetFormattedXml(RespXML)

            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXml

            'If DebugMode Then
            'VB6Aux.ShowBigDebugMessage("CheckPoint Err ")
            'End If

        End Try

    End Sub

    Public Function VoidResult(ByVal TransData As TransactionDto, ByVal TransType As TransactionDto.TransactionTypes) As String

        VoidResult = String.Empty

        If DCI("TrainingMode") Then

            Dim Lucky As Int32

            Lucky = RandomInt32(100)

            Select Case Lucky

                Case 1 To 20

                    Return "-1" ' Fallo

                Case Else '21 To 100

                    Return "00" ' Exito

            End Select

        Else

            If DCI("TipoConexion") = 1 Then
                VoidResult = NetConn.CancelTransaction(TransType, TransData)
            ElseIf DCI("TipoConexion") = 2 Then
                VoidResult = SerialConn.CancelTransaction(TransType, TransData)
            End If

            If IsNothing(VoidResult) Then VoidResult = String.Empty

        End If

    End Function

    Private Sub EMVVoid()

        'If Not DCI.Exists("RespData") Then
        DRI = VB6Aux.GetMeADictionary
        DCI("RespData") = DRI
        'Else
        'DRI = DCI("RespData")
        'End If

        DRI("TransactionResultError") = False

        Try

            Dim TranType As TransactionDto.TransactionTypes = CType(DCI("RelatedTransData_TranEnumType"), TransactionDto.TransactionTypes)

            Dim TransData As TransactionDto = New TransactionDto() With {
                .TransactionType = TransactionDto.TransactionTypes.CancellationNormalSale,
                .MultiMerchantID = CByte(IIf(String.IsNullOrEmpty(DCI("MultiMerchantID").ToString), 0, DCI("MultiMerchantID").ToString)),
                .MultiAcquirerID = CByte(IIf(String.IsNullOrEmpty(DCI("MultiAdquirienteID").ToString), 0, DCI("MultiAdquirienteID").ToString)),
                .Folio = 1, .CheckInDate = Now, .CheckOutDate = Now,
                .TransactionAmount = CDec(DCI("RelatedTransData_TotalTransactionAmount")),
                .TransactionITBIS = CDec(DCI("RelatedTransData_PaymentTaxAmount")),
                .TransactionOtherTaxes = CDec(DCI("RelatedTransData_OtherTaxesAmount")),
                .Host = CType(DCI("RelatedTransData_HostID"), CardDto.Hosts),
                .ReferenceNumber = CType(DCI("RelatedTransData_ReferenceNumber"), Short),
                .AuthorizationNumber = DCI("RelatedTransData_AuthorizationNumber").ToString
            }

            Dim ResultData As String = VoidResult(TransData, TransData.TransactionType)

            If ResultData <> "00" Then
                Throw New Exception("La anulación no se pudo realizar.")
            End If

            DRI("TransactionSuccess") = True

            DRI("HostID") = DCI("RelatedTransData_HostID")
            DRI("HostDescription") = DCI("RelatedTransData_HostDescription")

            DRI("CardProduct") = DCI("RelatedTransData_CardProduct")
            DRI("CardProductID") = DCI("RelatedTransData_CardProductID")

            DRI("TransactionModeID") = DCI("RelatedTransData_TransactionModeID")
            DRI("TransactionMode") = DCI("RelatedTransData_TransactionMode")

            DRI("AccountNumber") = DCI("RelatedTransData_AccountNumber")
            DRI("BatchNumber") = DCI("RelatedTransData_BatchNumber")
            DRI("ReferenceNumber") = DCI("RelatedTransData_ReferenceNumber")
            DRI("TransactionDateTime") = DateTime.Now 'DCI("RelatedTransData_TransactionDateTime")
            DRI("CardHolderName") = DCI("RelatedTransData_CardHolderName")
            DRI("AuthorizationNumber") = DCI("RelatedTransData_AuthorizationNumber")
            DRI("TerminalID") = DCI("RelatedTransData_TerminalID")
            DRI("RetrievalReferenceNumber") = DCI("RelatedTransData_RetrievalReferenceNumber")
            DRI("MerchantID") = DCI("RelatedTransData_MerchantID")
            DRI("LoyaltyDeferredNumber") = DCI("RelatedTransData_LoyaltyDeferredNumber")
            DRI("TransactionID") = DCI("RelatedTransData_TransactionID")
            DRI("ApplicationIdentifier") = DCI("RelatedTransData_ApplicationIdentifier")
            DRI("Reserved") = DCI("RelatedTransData_Reserved")
            DRI("Signature") = DCI("RelatedTransData_Signature")
            DRI("ApplicationVersion") = DCI("RelatedTransData_ApplicationVersion")

            DRI("SaleResultTypeIndicator") = DCI("RelatedTransData_SaleResultTypeIndicator")

            If (DRI("SaleResultTypeIndicator") = ("DC")) Then

                DRI("DCCAccepted") = DCI("RelatedTransData_DCCAccepted")
                DRI("DCCMarginPercentage") = DCI("RelatedTransData_DCCMarginPercentage")
                DRI("DCCTransactionAmount") = DCI("RelatedTransData_DCCTransactionAmount")
                DRI("DCCDisplayRate") = DCI("RelatedTransData_DCCDisplayRate")
                DRI("DCCCurrencyIdentifier") = DCI("RelatedTransData_DCCCurrencyIdentifier")

            ElseIf (DRI("SaleResultTypeIndicator") = ("PR") Or DRI("SaleResultTypeIndicator") = ("PM")) Then

                DRI("PromonetProgramID") = DCI("RelatedTransData_PromonetProgramID")
                DRI("PromonetProgramName") = DCI("RelatedTransData_PromonetProgramName")
                DRI("PromonetMessage") = DCI("RelatedTransData_PromonetMessage")
                DRI("PromonetQRCode") = DCI("RelatedTransData_PromonetQRCode")

            End If

            DRI("DigitalSignaturePerformed") = DCI("RelatedTransData_DigitalSignaturePerformed")
            DRI("ResponseMessage") = DCI("RelatedTransData_ResponseMessage")
            DRI("SignatureModeIndicator") = DCI("RelatedTransData_SignatureModeIndicator")
            DRI("FormattedExpirationDate") = DCI("RelatedTransData_FormattedExpirationDate")
            DRI("VoucherMode") = DCI("RelatedTransData_VoucherMode")
            DRI("EMVApplicationLevel") = DCI("RelatedTransData_EMVApplicationLevel")
            DRI("QuickPaymentIndicator") = DCI("RelatedTransData_QuickPaymentIndicator")
            DRI("PINModeIndicator") = DCI("RelatedTransData_PINModeIndicator")

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<Transaction>"

            TXML += "<Request>"

            TXML += "<ConsortiumID>10</ConsortiumID>"
            TXML += "<ConsortiumName>CardNET RD</ConsortiumName>"

            TXML += "<OperationType>" + DCI("TipoOperacion").ToString + "</OperationType>"
            TXML += "<TranCode>" + DCI("TranCode").ToString + "</TranCode>"

            TXML += "<TranEnumType>" + DCI("RelatedTransData_TranEnumType").ToString + "</TranEnumType>"
            TXML += "<MultiMerchantID>" + DCI("RelatedTransData_MultiMerchantID").ToString + "</MultiMerchantID>"
            TXML += "<MultiAcquirerID>" + DCI("RelatedTransData_MultiAcquirerID").ToString + "</MultiAcquirerID>"
            TXML += "<InvoiceNumber>" + DCI("RelatedTransData_InvoiceNumber").ToString + "</InvoiceNumber>"
            TXML += "<Folio>" + DCI("RelatedTransData_Folio").ToString + "</Folio>"
            TXML += "<PurchaseTaxableAmount>" + DCI("RelatedTransData_PurchaseTaxableAmount").ToString + "</PurchaseTaxableAmount>"
            TXML += "<PurchaseTaxAmount>" + DCI("RelatedTransData_PurchaseTaxAmount").ToString + "</PurchaseTaxAmount>"
            TXML += "<PurchaseAmount>" + DCI("RelatedTransData_PurchaseAmount").ToString + "</PurchaseAmount>"
            TXML += "<PaymentTaxableAmount>" + DCI("RelatedTransData_PaymentTaxableAmount").ToString + "</PaymentTaxableAmount>"
            TXML += "<PaymentTaxAmount>" + DCI("RelatedTransData_PaymentTaxAmount").ToString + "</PaymentTaxAmount>"
            TXML += "<PaymentAmount>" + DCI("RelatedTransData_PaymentAmount").ToString + "</PaymentAmount>"
            TXML += "<CashbackAmount>" + DCI("RelatedTransData_CashbackAmount").ToString + "</CashbackAmount>"
            TXML += "<OtherTaxesAmount>" + DCI("RelatedTransData_OtherTaxesAmount").ToString + "</OtherTaxesAmount>"
            TXML += "<TotalTransactionAmount>" + DCI("RelatedTransData_TotalTransactionAmount").ToString + "</TotalTransactionAmount>"

            If DCI("DctoxBIN_Apply") Then
                TXML += "<DctoxBIN_Bin>" + DCI("RelatedTransData_DctoxBIN_Bin").ToString + "</DctoxBIN_Bin>"
                TXML += "<DctoxBIN_IsPercent>" + IIf(DCI("RelatedTransData_DctoxBIN_IsPercent"), "1", "0") + "</DctoxBIN_IsPercent>"
                TXML += "<DctoxBIN_Value>" + DCI("RelatedTransData_DctoxBIN_Value").ToString + "</DctoxBIN_Value>"
                TXML += "<BINDiscountAmount>" + DCI("RelatedTransData_BINDiscountAmount").ToString + "</BINDiscountAmount>"
            End If

            TXML += "</Request>"

            TXML += "<Response>"

            TXML += "<Result>00</Result>"

            TXML += "<HostID>" + DRI("HostID").ToString + "</HostID>"
            TXML += "<HostDescription>" + DRI("HostDescription").ToString + "</HostDescription>"
            TXML += "<CardProductID>" + DRI("CardProductID").ToString + "</CardProductID>"
            TXML += "<CardProduct>" + DRI("CardProduct").ToString + "</CardProduct>"
            TXML += "<TransactionModeID>" + DRI("TransactionModeID").ToString + "</TransactionModeID>"
            TXML += "<TransactionMode>" + DRI("TransactionMode").ToString + "</TransactionMode>"
            TXML += "<AccountNumber>" + DRI("AccountNumber").ToString + "</AccountNumber>"
            TXML += "<BatchNumber>" + DRI("BatchNumber").ToString + "</BatchNumber>"
            TXML += "<ReferenceNumber>" + DRI("ReferenceNumber").ToString + "</ReferenceNumber>"
            TXML += "<TransactionDateTime>" + EncodeISOFullDate(CType(DRI("TransactionDateTime"), Date)) + "</TransactionDateTime>"
            TXML += "<CardHolderName>" + DRI("CardHolderName").ToString + "</CardHolderName>"
            TXML += "<AuthorizationNumber>" + DRI("AuthorizationNumber").ToString + "</AuthorizationNumber>"
            TXML += "<TerminalID>" + DRI("TerminalID").ToString + "</TerminalID>"
            TXML += "<RetrievalReferenceNumber>" + DRI("RetrievalReferenceNumber").ToString + "</RetrievalReferenceNumber>"
            TXML += "<LoyaltyDeferredNumber>" + DRI("LoyaltyDeferredNumber").ToString + "</LoyaltyDeferredNumber>"
            TXML += "<TransactionID>" + DRI("TransactionID").ToString + "</TransactionID>"
            TXML += "<ApplicationIdentifier>" + DRI("ApplicationIdentifier").ToString + "</ApplicationIdentifier>"
            TXML += "<Reserved><![CDATA[" + DRI("Reserved").ToString + "]]></Reserved>"
            TXML += "<Signature>" + String.Join(" ", DRI("Signature")) + "</Signature>"
            TXML += "<ApplicationVersion>" + DRI("ApplicationVersion").ToString + "</ApplicationVersion>"

            TXML += "<SaleResultTypeIndicator>" + DRI("SaleResultTypeIndicator").ToString + "</SaleResultTypeIndicator>"

            If (DRI("SaleResultTypeIndicator") = ("DC")) Then

                TXML += "<DCCAccepted>" + DRI("DCCAccepted").ToString + "</DCCAccepted>"
                TXML += "<DCCMarginPercentage>" + DRI("DCCMarginPercentage").ToString + "</DCCMarginPercentage>"
                TXML += "<DCCTransactionAmount>" + DRI("DCCTransactionAmount").ToString + "</DCCTransactionAmount>"
                TXML += "<DCCDisplayRate>" + DRI("DCCDisplayRate").ToString + "</DCCDisplayRate>"
                TXML += "<DCCCurrencyIdentifier>" + DRI("DCCCurrencyIdentifier").ToString + "</DCCCurrencyIdentifier>"

            ElseIf (DRI("SaleResultTypeIndicator") = ("PR") Or DRI("SaleResultTypeIndicator") = ("PM")) Then

                TXML += "<PromonetProgramID><![CDATA[" + DRI("PromonetProgramID").ToString + "]]></PromonetProgramID>"
                TXML += "<PromonetProgramName><![CDATA[" + DRI("PromonetProgramName").ToString + "]]></PromonetProgramName>"
                TXML += "<PromonetMessage><![CDATA[" + DRI("PromonetMessage").ToString + "]]></PromonetMessage>"
                TXML += "<PromonetQRCode><![CDATA[" + DRI("PromonetQRCode").ToString + "]]></PromonetQRCode>"

            End If

            TXML += "<DigitalSignaturePerformed>" + DRI("DigitalSignaturePerformed").ToString + "</DigitalSignaturePerformed>"
            TXML += "<ResponseMessage>" + DRI("ResponseMessage").ToString + "</ResponseMessage>"
            TXML += "<SignatureModeIndicator>" + DRI("SignatureModeIndicator").ToString + "</SignatureModeIndicator>"
            TXML += "<FormattedExpirationDate>" + DRI("FormattedExpirationDate").ToString + "</FormattedExpirationDate>"
            TXML += "<VoucherMode>" + DRI("VoucherMode").ToString + "</VoucherMode>"
            TXML += "<EMVApplicationLevel>" + DRI("EMVApplicationLevel").ToString + "</EMVApplicationLevel>"
            TXML += "<QuickPaymentIndicator>" + DRI("QuickPaymentIndicator").ToString + "</QuickPaymentIndicator>"
            TXML += "<PINModeIndicator>" + DRI("PINModeIndicator").ToString + "</PINModeIndicator>"

            TXML += "</Response>"

            TXML += "</Transaction>"

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            Dim RespXML As String = TXML
            Dim FormattedRespXml = GetFormattedXml(RespXML)

            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXml

            DRI("SequenceNo") = "00000001"
            DCI("SequenceNo") = DRI("SequenceNo")

        Catch Any As Exception

            DRI("TransactionSuccess") = False

            DRI("TransactionResultError") = True
            DRI("TransactionResultError_Code") = Any.HResult
            DRI("TransactionResultError_Desc") = Any.Message
            DRI("TransactionResultError_Source") = Any.Source
            DRI("TransactionResultError_StackTrace") = Any.StackTrace

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<Transaction>"

            TXML += "<Error>" + "True" + "</Error>"
            TXML += "<Code>" + DRI("TransactionResultError_Code").ToString + "</Code>"
            TXML += "<Desc><![CDATA[" + DRI("TransactionResultError_Desc").ToString + "]]></Desc>"
            TXML += "<Source><![CDATA[" + DRI("TransactionResultError_Source").ToString + "]]></Source>"
            TXML += "<StackTrace><![CDATA[" + DRI("TransactionResultError_StackTrace").ToString + "]]></StackTrace>"

            TXML += "</Transaction>"

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            Dim RespXML As String = TXML
            Dim FormattedRespXml = GetFormattedXml(RespXML)

            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXml

        End Try

    End Sub

    Private Sub EMVReturn()

        'If Not DCI.Exists("RespData") Then
        DRI = VB6Aux.GetMeADictionary
        DCI("RespData") = DRI
        'Else
        'DRI = DCI("RespData")
        'End If

        DRI("TransactionResultError") = False

        Try

            Dim TranType As TransactionDto.TransactionTypes = TransactionDto.TransactionTypes.NormalSaleReturn

            Dim TransData As TransactionDto = New TransactionDto() With {
                .TransactionType = TranType,
                .MultiMerchantID = CByte(IIf(String.IsNullOrEmpty(DCI("MultiMerchantID").ToString), 0, DCI("MultiMerchantID").ToString)),
                .MultiAcquirerID = CByte(IIf(String.IsNullOrEmpty(DCI("MultiAdquirienteID").ToString), 0, DCI("MultiAdquirienteID").ToString)),
                .Folio = 1, .CheckInDate = Now, .CheckOutDate = Now,
                .TransactionAmount = CDec(DCI("RelatedTransData_TotalTransactionAmount")),
                .TransactionITBIS = CDec(DCI("RelatedTransData_PaymentTaxAmount")),
                .TransactionOtherTaxes = CDec(DCI("RelatedTransData_OtherTaxesAmount")),
                .InvoiceNumber = DCI("InvoiceNo").ToString
            }

            TransData = TransactionResult(TransData, TransData.TransactionType)

            If IsNothing(TransData) Then
                Throw New Exception("Transacción sin respuesta.")
            End If

            'If TransData.ReferenceNumber = 0 Then
            'DRI("TransactionSuccess") = False
            'Else
            DRI("TransactionSuccess") = True
            'End If

            DRI("HostID") = TransData.Host
            DRI("HostDescription") = GetHostDescription(TransData.Host)

            DRI("CardProduct") = GetCardProduct(TransData.CardProduct)
            DRI("CardProductID") = Convert.ToInt32(TransData.CardProduct).ToString

            DRI("TransactionModeID") = TransData.TransactionMode
            DRI("TransactionMode") = GetTransactionMode(DRI("TransactionModeID").ToString)

            DRI("AccountNumber") = TransData.AccountNumber
            DRI("BatchNumber") = TransData.BatchNumber
            DRI("ReferenceNumber") = TransData.ReferenceNumber
            DRI("TransactionDateTime") = TransData.TransactionDateTime
            DRI("CardHolderName") = TransData.CardHolderName
            DRI("AuthorizationNumber") = TransData.AuthorizationNumber
            DRI("TerminalID") = TransData.TerminalID
            DRI("RetrievalReferenceNumber") = TransData.RetrievalReferenceNumber
            DRI("MerchantID") = TransData.MerchantID
            DRI("LoyaltyDeferredNumber") = TransData.LoyaltyDeferredNumber
            DRI("TransactionID") = TransData.TransactionID
            DRI("ApplicationIdentifier") = TransData.ApplicationIdentifier
            DRI("Reserved") = TransData.Reserved
            DRI("Signature") = TransData.Signature
            DRI("ApplicationVersion") = TransData.ApplicationVersion

            DRI("PurchaseTaxableAmount") = DCI("RelatedTransData_PurchaseTaxableAmount")
            DRI("PurchaseTaxAmount") = DCI("RelatedTransData_PurchaseTaxAmount")
            DRI("PurchaseAmount") = DCI("RelatedTransData_PurchaseAmount")
            DRI("PaymentTaxableAmount") = DCI("RelatedTransData_PaymentTaxableAmount")
            DRI("PaymentTaxAmount") = DCI("RelatedTransData_PaymentTaxAmount")
            DRI("PaymentAmount") = DCI("RelatedTransData_PaymentAmount")
            DRI("CashbackAmount") = DCI("RelatedTransData_CashbackAmount")
            DRI("OtherTaxesAmount") = DCI("RelatedTransData_OtherTaxesAmount")
            DRI("TotalTransactionAmount") = DCI("RelatedTransData_TotalTransactionAmount")

            DRI("SaleResultTypeIndicator") = TransData.SaleResultTypeIndicator

            If (DRI("SaleResultTypeIndicator") = ("DC")) Then

                DRI("DCCAccepted") = TransData.DCCAccepted
                DRI("DCCMarginPercentage") = TransData.DCCMarginPercentage
                DRI("DCCTransactionAmount") = TransData.DCCTransactionAmount
                DRI("DCCDisplayRate") = TransData.DCCDisplayRate
                DRI("DCCCurrencyIdentifier") = TransData.DCCCurrencyIdentifier

            ElseIf (DRI("SaleResultTypeIndicator") = ("PR") Or DRI("SaleResultTypeIndicator") = ("PM")) Then

                DRI("PromonetProgramID") = TransData.PromonetProgramID
                DRI("PromonetProgramName") = TransData.PromonetProgramName
                DRI("PromonetMessage") = TransData.PromonetMessage
                DRI("PromonetQRCode") = TransData.PromonetQRCode

            End If

            DRI("DigitalSignaturePerformed") = TransData.DigitalSignaturePerformed
            DRI("ResponseMessage") = TransData.ResponseMessage
            DRI("SignatureModeIndicator") = TransData.SignatureModeIndicator
            DRI("FormattedExpirationDate") = TransData.FormattedExpirationDate
            DRI("VoucherMode") = TransData.VoucherMode
            DRI("EMVApplicationLevel") = TransData.EMVApplicationLevel
            DRI("QuickPaymentIndicator") = TransData.QuickPaymentIndicator
            DRI("PINModeIndicator") = TransData.PINModeIndicator

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<Transaction>"

            TXML += "<Request>"

            TXML += "<ConsortiumID>10</ConsortiumID>"
            TXML += "<ConsortiumName>CardNET RD</ConsortiumName>"

            TXML += "<OperationType>" + DCI("TipoOperacion").ToString + "</OperationType>"
            TXML += "<TranCode>" + DCI("TranCode").ToString + "</TranCode>"
            TXML += "<TranEnumType>" + Convert.ToInt32(TranType).ToString + "</TranEnumType>"
            TXML += "<MultiMerchantID>" + TransData.MultiMerchantID.ToString + "</MultiMerchantID>"
            TXML += "<MultiAcquirerID>" + TransData.MultiAcquirerID.ToString + "</MultiAcquirerID>"
            TXML += "<InvoiceNumber>" + DCI("RelatedTransData_InvoiceNumber").ToString + "</InvoiceNumber>"
            TXML += "<Folio>" + TransData.Folio.ToString + "</Folio>"
            TXML += "<PurchaseTaxableAmount>" + DCI("RelatedTransData_PurchaseTaxableAmount").ToString + "</PurchaseTaxableAmount>"
            TXML += "<PurchaseTaxAmount>" + DCI("RelatedTransData_PurchaseTaxAmount").ToString + "</PurchaseTaxAmount>"
            TXML += "<PurchaseAmount>" + DCI("RelatedTransData_PurchaseAmount").ToString + "</PurchaseAmount>"
            TXML += "<PaymentTaxableAmount>" + DCI("RelatedTransData_PaymentTaxableAmount").ToString + "</PaymentTaxableAmount>"
            TXML += "<PaymentTaxAmount>" + DCI("RelatedTransData_PaymentTaxAmount").ToString + "</PaymentTaxAmount>"
            TXML += "<PaymentAmount>" + DCI("RelatedTransData_PaymentAmount").ToString + "</PaymentAmount>"
            TXML += "<CashbackAmount>" + DCI("RelatedTransData_CashbackAmount").ToString + "</CashbackAmount>"
            TXML += "<OtherTaxesAmount>" + DCI("RelatedTransData_OtherTaxesAmount").ToString + "</OtherTaxesAmount>"
            TXML += "<TotalTransactionAmount>" + DCI("RelatedTransData_TotalTransactionAmount").ToString + "</TotalTransactionAmount>"

            If DCI("DctoxBIN_Apply") Then
                TXML += "<DctoxBIN_Bin>" + DCI("RelatedTransData_DctoxBIN_Bin").ToString + "</DctoxBIN_Bin>"
                TXML += "<DctoxBIN_IsPercent>" + IIf(DCI("RelatedTransData_DctoxBIN_IsPercent"), "1", "0") + "</DctoxBIN_IsPercent>"
                TXML += "<DctoxBIN_Value>" + DCI("RelatedTransData_DctoxBIN_Value").ToString + "</DctoxBIN_Value>"
                TXML += "<BINDiscountAmount>" + DCI("RelatedTransData_BINDiscountAmount").ToString + "</BINDiscountAmount>"
            End If

            TXML += "</Request>"

            TXML += "<Response>"

            TXML += "<HostID>" + DRI("HostID").ToString + "</HostID>"
            TXML += "<HostDescription>" + DRI("HostDescription").ToString + "</HostDescription>"
            TXML += "<CardProductID>" + DRI("CardProductID").ToString + "</CardProductID>"
            TXML += "<CardProduct>" + DRI("CardProduct").ToString + "</CardProduct>"
            TXML += "<TransactionModeID>" + DRI("TransactionModeID").ToString + "</TransactionModeID>"
            TXML += "<TransactionMode>" + DRI("TransactionMode").ToString + "</TransactionMode>"
            TXML += "<AccountNumber>" + DRI("AccountNumber").ToString + "</AccountNumber>"
            TXML += "<BatchNumber>" + DRI("BatchNumber").ToString + "</BatchNumber>"
            TXML += "<ReferenceNumber>" + DRI("ReferenceNumber").ToString + "</ReferenceNumber>"
            TXML += "<TransactionDateTime>" + EncodeISOFullDate(CType(DRI("TransactionDateTime"), Date)) + "</TransactionDateTime>"
            TXML += "<CardHolderName>" + DRI("CardHolderName").ToString + "</CardHolderName>"
            TXML += "<AuthorizationNumber>" + DRI("AuthorizationNumber").ToString + "</AuthorizationNumber>"
            TXML += "<TerminalID>" + DRI("TerminalID").ToString + "</TerminalID>"
            TXML += "<RetrievalReferenceNumber>" + DRI("RetrievalReferenceNumber").ToString + "</RetrievalReferenceNumber>"
            TXML += "<LoyaltyDeferredNumber>" + DRI("LoyaltyDeferredNumber").ToString + "</LoyaltyDeferredNumber>"
            TXML += "<TransactionID>" + DRI("TransactionID").ToString + "</TransactionID>"
            TXML += "<ApplicationIdentifier>" + DRI("ApplicationIdentifier").ToString + "</ApplicationIdentifier>"
            TXML += "<Reserved><![CDATA[" + DRI("Reserved").ToString + "]]></Reserved>"
            TXML += "<Signature>" + String.Join(" ", DRI("Signature")) + "</Signature>"
            TXML += "<ApplicationVersion>" + DRI("ApplicationVersion").ToString + "</ApplicationVersion>"

            TXML += "<SaleResultTypeIndicator>" + DRI("SaleResultTypeIndicator").ToString + "</SaleResultTypeIndicator>"

            If (DRI("SaleResultTypeIndicator") = ("DC")) Then

                TXML += "<DCCAccepted>" + DRI("DCCAccepted").ToString + "</DCCAccepted>"
                TXML += "<DCCMarginPercentage>" + DRI("DCCMarginPercentage").ToString + "</DCCMarginPercentage>"
                TXML += "<DCCTransactionAmount>" + DRI("DCCTransactionAmount").ToString + "</DCCTransactionAmount>"
                TXML += "<DCCDisplayRate>" + DRI("DCCDisplayRate").ToString + "</DCCDisplayRate>"
                TXML += "<DCCCurrencyIdentifier>" + DRI("DCCCurrencyIdentifier").ToString + "</DCCCurrencyIdentifier>"

            ElseIf (DRI("SaleResultTypeIndicator") = ("PR") Or DRI("SaleResultTypeIndicator") = ("PM")) Then

                TXML += "<PromonetProgramID><![CDATA[" + DRI("PromonetProgramID").ToString + "]]></PromonetProgramID>"
                TXML += "<PromonetProgramName><![CDATA[" + DRI("PromonetProgramName").ToString + "]]></PromonetProgramName>"
                TXML += "<PromonetMessage><![CDATA[" + DRI("PromonetMessage").ToString + "]]></PromonetMessage>"
                TXML += "<PromonetQRCode><![CDATA[" + DRI("PromonetQRCode").ToString + "]]></PromonetQRCode>"

            End If

            TXML += "<DigitalSignaturePerformed>" + DRI("DigitalSignaturePerformed").ToString + "</DigitalSignaturePerformed>"
            TXML += "<ResponseMessage>" + DRI("ResponseMessage").ToString + "</ResponseMessage>"
            TXML += "<SignatureModeIndicator>" + DRI("SignatureModeIndicator").ToString + "</SignatureModeIndicator>"
            TXML += "<FormattedExpirationDate>" + DRI("FormattedExpirationDate").ToString + "</FormattedExpirationDate>"
            TXML += "<VoucherMode>" + DRI("VoucherMode").ToString + "</VoucherMode>"
            TXML += "<EMVApplicationLevel>" + DRI("EMVApplicationLevel").ToString + "</EMVApplicationLevel>"
            TXML += "<QuickPaymentIndicator>" + DRI("QuickPaymentIndicator").ToString + "</QuickPaymentIndicator>"
            TXML += "<PINModeIndicator>" + DRI("PINModeIndicator").ToString + "</PINModeIndicator>"

            TXML += "</Response>"

            TXML += "</Transaction>"

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            Dim RespXML As String = TXML
            Dim FormattedRespXml = GetFormattedXml(RespXML)

            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXml

            DRI("SequenceNo") = "00000001"
            DCI("SequenceNo") = DRI("SequenceNo")

        Catch Any As Exception

            DRI("TransactionSuccess") = False

            DRI("TransactionResultError") = True
            DRI("TransactionResultError_Code") = Any.HResult
            DRI("TransactionResultError_Desc") = Any.Message
            DRI("TransactionResultError_Source") = Any.Source
            DRI("TransactionResultError_StackTrace") = Any.StackTrace

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<Transaction>"

            TXML += "<Error>" + "True" + "</Error>"
            TXML += "<Code>" + DRI("TransactionResultError_Code").ToString + "</Code>"
            TXML += "<Desc><![CDATA[" + DRI("TransactionResultError_Desc").ToString + "]]></Desc>"
            TXML += "<Source><![CDATA[" + DRI("TransactionResultError_Source").ToString + "]]></Source>"
            TXML += "<StackTrace><![CDATA[" + DRI("TransactionResultError_StackTrace").ToString + "]]></StackTrace>"

            TXML += "</Transaction>"

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            Dim RespXML As String = TXML
            Dim FormattedRespXml = GetFormattedXml(RespXML)

            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXml

        End Try

    End Sub

    Private Function GetFormattedXml(ByVal XmlStr As String, Optional ByVal DeclarationText As String = "") As String
        Return FormatXml(XmlStr, DeclarationText)
    End Function

    Private Function FormatXml(ByVal XmlStr As String, Optional ByVal DeclarationText As String = "") As String

        Try

            Dim TmpDoc As XDocument = XDocument.Parse(XmlStr)

            XmlStr = TmpDoc.ToString()

            If Not String.IsNullOrEmpty(DeclarationText) Then
                XmlStr = DeclarationText + vbNewLine + XmlStr
            End If

            Return XmlStr

        Catch ex As Exception
            Return XmlStr
        End Try

    End Function

    Public Sub SecureClosureResult(ByRef Result As List(Of ClosureDto))

        Dim CloseData As ClosureDto

        If Not IsNothing(Result) Then

            For Each CloseData In Result
                If IsNothing(CloseData.ID) Then CloseData.ID = 0
                If IsNothing(CloseData.Result) Then CloseData.Result = "01"
                If IsNothing(CloseData.Host) Then CloseData.Host = "0"
                If IsNothing(CloseData.MerchantID) Then CloseData.MerchantID = 0
                If IsNothing(CloseData.TerminalID) Then CloseData.TerminalID = 0
                If IsNothing(CloseData.BatchNumber) Then CloseData.BatchNumber = 0
                If IsNothing(CloseData.BatchDateTime) Then CloseData.BatchDateTime = Now
                If IsNothing(CloseData.ReturnsQuantity) Then CloseData.ReturnsQuantity = 0
                If IsNothing(CloseData.ReturnsAmount) Then CloseData.ReturnsAmount = 0
                If IsNothing(CloseData.ReturnsTax) Then CloseData.ReturnsTax = 0
                If IsNothing(CloseData.PurchasesQuantity) Then CloseData.PurchasesQuantity = 0
                If IsNothing(CloseData.PurchasesAmount) Then CloseData.PurchasesAmount = 0
                If IsNothing(CloseData.PurchasesTax) Then CloseData.PurchasesTax = 0
                If IsNothing(CloseData.OtherTax) Then CloseData.OtherTax = 0
            Next

        End If

    End Sub

    Public Function ClosureResult(ByVal MultiMerchantEnabled As Boolean, ByVal MultiMerchantID As Byte) As List(Of ClosureDto)

        ClosureResult = Nothing

        If DCI("TrainingMode") Then

            Dim Lucky As Int32

            Lucky = RandomInt32(100)

            Select Case Lucky

                Case 1 To 20

                    Return Nothing

                Case Else '21 To 100

                    Dim ClosureCollection As New List(Of ClosureDto)

                    Dim Lucky2 As Int32

                    Lucky2 = RandomInt32(100)

                    Dim CloseData As New ClosureDto() With {
                        .ID = RandomInt32(99999, 10000),
                        .Result = IIf(Lucky2 > 30, "00", IIf(Lucky2 > 15, "02", "01")),
                        .Host = IIf(Lucky2 > 65, "DEBITO", "CREDITO"),
                        .MerchantID = "10203040",
                        .TerminalID = "80001234",
                        .BatchNumber = IIf(Lucky2 > 65, 102, 101),
                        .BatchDateTime = DateTime.Now,
                        .ReturnsQuantity = 5,
                        .ReturnsAmount = 2250.89,
                        .ReturnsTax = 405.1602,
                        .PurchasesQuantity = 28,
                        .PurchasesAmount = 53614.8947,
                        .PurchasesTax = 9650.681,
                        .OtherTax = IIf(Lucky2 > 90, 1254.47, 0)
                    }

                    ClosureCollection.Add(CloseData)

                    Return ClosureCollection

            End Select

        Else

            If DCI("TipoConexion") = 1 Then
                ClosureResult = NetConn.ExecuteClosure(MultiMerchantEnabled, MultiMerchantID)
            ElseIf DCI("TipoConexion") = 2 Then
                ClosureResult = SerialConn.ExecuteClosure(MultiMerchantEnabled, MultiMerchantID)
            End If

            SecureClosureResult(ClosureResult)

            Return ClosureResult

        End If

    End Function

    Private Sub BatchClose()

        'If Not DCI.Exists("RespData") Then
        DRI = VB6Aux.GetMeADictionary
        DCI("RespData") = DRI
        'Else
        'DRI = DCI("RespData")
        'End If

        DRI("TransactionResultError") = False

        Try

            Dim CloseData As ClosureDto
            Dim ClosureList As List(Of ClosureDto)

            Dim MultiMerchantID As Double

            MultiMerchantID = Val(DCI("MultiMerchantID"))

            If MultiMerchantID > 99 Then MultiMerchantID = 0

            ClosureList = ClosureResult((MultiMerchantID > 1), Convert.ToByte(MultiMerchantID))

            If IsNothing(ClosureList) Then
                Throw New Exception("Lotes vacíos.")
            End If

            Dim ClosureCollection As Object

            ClosureCollection = VB6Aux.GetMeACollection

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<Transaction>
"
            TXML += "<Request>"

            TXML += "<ConsortiumID>10</ConsortiumID>"
            TXML += "<ConsortiumName>CardNET RD</ConsortiumName>"

            TXML += "<TranCode>" + DCI("TranCode").ToString + "</TranCode>"
            TXML += "<MultiMerchantID>" + DCI("MultiMerchantID").ToString + "</MultiMerchantID>"

            TXML += "</Request>"

            TXML += "<Response>"

            For Each CloseData In ClosureList

                Dim TransferData As Object = VB6Aux.GetMeADictionary

                TransferData("ID") = CloseData.ID
                TransferData("Result") = CloseData.Result
                TransferData("HostID") = CloseData.Host
                TransferData("HostDescription") = CloseData.Host
                TransferData("MerchantID") = CloseData.MerchantID
                TransferData("TerminalID") = CloseData.TerminalID
                TransferData("BatchNumber") = CloseData.BatchNumber
                TransferData("BatchDateTime") = CloseData.BatchDateTime
                TransferData("ReturnsQuantity") = CloseData.ReturnsQuantity
                TransferData("ReturnsAmount") = CloseData.ReturnsAmount
                TransferData("ReturnsTax") = CloseData.ReturnsTax
                TransferData("PurchasesQuantity") = CloseData.PurchasesQuantity
                TransferData("PurchasesAmount") = CloseData.PurchasesAmount
                TransferData("PurchasesTax") = CloseData.PurchasesTax
                TransferData("OtherTax") = CloseData.OtherTax

                ClosureCollection.Add(TransferData)

                TXML += "<ClosureData>"

                TXML += "<ID>" + TransferData("ID").ToString + "</ID>"
                TXML += "<Result>" + TransferData("Result").ToString + "</Result>"
                TXML += "<HostID>" + TransferData("HostID").ToString + "</HostID>"
                TXML += "<HostDescription>" + TransferData("HostDescription").ToString + "</HostDescription>"
                TXML += "<MerchantID>" + TransferData("MerchantID").ToString + "</MerchantID>"
                TXML += "<TerminalID>" + TransferData("TerminalID").ToString + "</TerminalID>"
                TXML += "<BatchNumber>" + TransferData("BatchNumber").ToString + "</BatchNumber>"
                TXML += "<BatchDateTime>" + TransferData("BatchDateTime").ToString + "</BatchDateTime>"
                TXML += "<ReturnsQuantity>" + TransferData("ReturnsQuantity").ToString + "</ReturnsQuantity>"
                TXML += "<ReturnsAmount>" + TransferData("ReturnsAmount").ToString + "</ReturnsAmount>"
                TXML += "<ReturnsTax>" + TransferData("ReturnsTax").ToString + "</ReturnsTax>"
                TXML += "<PurchasesQuantity>" + TransferData("PurchasesQuantity").ToString + "</PurchasesQuantity>"
                TXML += "<PurchasesAmount>" + TransferData("PurchasesAmount").ToString + "</PurchasesAmount>"
                TXML += "<PurchasesTax>" + TransferData("PurchasesTax").ToString + "</PurchasesTax>"
                TXML += "<OtherTax>" + TransferData("OtherTax").ToString + "</OtherTax>"

                TXML += "</ClosureData>"

            Next

            TXML += "</Response>"

            TXML += "</Transaction>"

            DRI("SaleResultTypeIndicator") = String.Empty ' La aplicación siempre espera recibir este parámetro.

            DRI("TransactionSuccess") = True

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            Dim RespXML As String = TXML
            Dim FormattedRespXml = GetFormattedXml(RespXML)

            DRI("ClosureList") = ClosureCollection

            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXml

            DRI("SequenceNo") = "00000001"
            DCI("SequenceNo") = DRI("SequenceNo")

        Catch Any As Exception

            DRI("TransactionSuccess") = False

            DRI("TransactionResultError") = True
            DRI("TransactionResultError_Code") = Any.HResult
            DRI("TransactionResultError_Desc") = Any.Message
            DRI("TransactionResultError_Source") = Any.Source
            DRI("TransactionResultError_StackTrace") = Any.StackTrace

            Dim TmpXml1 As XmlDocument = New XmlDocument()

            Dim TXML As String = String.Empty

            TXML += "<Transaction>"

            TXML += "<Error>" + "True" + "</Error>"
            TXML += "<Code>" + DRI("TransactionResultError_Code").ToString + "</Code>"
            TXML += "<Desc><![CDATA[" + DRI("TransactionResultError_Desc").ToString + "]]></Desc>"
            TXML += "<Source><![CDATA[" + DRI("TransactionResultError_Source").ToString + "]]></Source>"
            TXML += "<StackTrace><![CDATA[" + DRI("TransactionResultError_StackTrace").ToString + "]]></StackTrace>"

            TXML += "</Transaction>"

            FormatXml(TXML, TmpXml1.CreateXmlDeclaration("1.0", String.Empty, String.Empty).OuterXml)

            Dim RespXML As String = TXML
            Dim FormattedRespXml = GetFormattedXml(RespXML)

            DRI("OperationResponseXML") = RespXML
            DRI("OperationResponseFormattedXML") = FormattedRespXml

        End Try

    End Sub

    Public Function EncodeISOFullDate(ByVal pDate As Date) As String

        EncodeISOFullDate = String.Empty

        EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("yyyy", pDate), "0000")
        EncodeISOFullDate = EncodeISOFullDate & "-"
        EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("m", pDate), "00")
        EncodeISOFullDate = EncodeISOFullDate & "-"
        EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("d", pDate), "00")

        EncodeISOFullDate = EncodeISOFullDate & "T"

        EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("h", pDate), "00")
        EncodeISOFullDate = EncodeISOFullDate & ":"
        EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("n", pDate), "00")
        EncodeISOFullDate = EncodeISOFullDate & ":"
        EncodeISOFullDate = EncodeISOFullDate & Format(DatePart("s", pDate), "00")

    End Function

    Public Function DecodeISOFullDate(ByVal pISOString As String) As Date

        DecodeISOFullDate = CDate(CStr(
        DateSerial(CInt(Mid(pISOString, 1, 4)), CInt(Mid(pISOString, 6, 2)), CInt(Mid(pISOString, 9, 2))) & " " &
        TimeSerial(CInt(Mid(pISOString, 12, 2)), CInt(Mid(pISOString, 15, 2)), CInt(Mid(pISOString, 18, 2)))))

    End Function

    Private Function RandomInt64(ByVal pEndRange As Int64, Optional ByVal pInitialRange As Int64 = 1) As Int64
        Randomize()
        RandomInt64 = Convert.ToInt64(Math.Ceiling(Rnd() * pEndRange)) + (pInitialRange - 1)
    End Function

    Private Function RandomInt32(ByVal pEndRange As Int32, Optional ByVal pInitialRange As Int32 = 1) As Int32
        Randomize()
        RandomInt32 = Convert.ToInt32(Math.Ceiling(Rnd() * pEndRange)) + (pInitialRange - 1)
    End Function

End Class